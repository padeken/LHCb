###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

import os
from DDDB.CheckDD4Hep import UseDD4Hep

# If no DD4hep, just skip the test
# this is what the rc=77 indicates
if UseDD4Hep:
    exit(77)

# Prepare detector description
##############################

from Configurables import CondDB, DDDBConf, GitEntityResolver
GitEntityResolver(
    'GitDDDB',
    PathToRepository=os.path.join(os.environ.get('TEST_DBS_ROOT'), 'TESTCOND'))
DDDBConf(DataType='2016', DbRoot='git:/lhcb.xml', EnableRunStampCheck=False)
CondDB(Tags={'DDDB': ''})


@appendPostConfigAction
def reduce_resolver():
    '''override some settings from DDDBConf'''
    from Configurables import XmlParserSvc
    resolvers = XmlParserSvc().EntityResolver.EntityResolvers
    resolvers[:] = [
        r for r in resolvers if r.name()[8:15] in ('GitDDDB', 'GitOver')
    ]


# Configure fake event time
###########################
from Configurables import EventClockSvc, FakeEventTime
ecs = EventClockSvc()
ecs.addTool(FakeEventTime, 'EventTimeDecoder')
# tuned from the content of condition in TESTCOND
ecs.EventTimeDecoder.StartTime = 1442403000000000000
ecs.EventTimeDecoder.TimeStep = 18399600000000000

# Configure algorithms
######################
from Configurables import DetCond__Examples__Functional__CondAccessExample as CondAlg
from Configurables import DetCond__Examples__Functional__CondAccessExampleWithDerivation as CondAlgDerived

app = ApplicationMgr(EvtSel="NONE", EvtMax=3, OutputLevel=INFO)
app.TopAlg += [CondAlg('CondAlg'), CondAlgDerived('CondAlgDerived')]
