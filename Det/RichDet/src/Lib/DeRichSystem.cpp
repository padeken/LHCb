/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//=============================================================================
/** @file DeRichSystem.cpp
 *
 * Implementation file for class : DeRichSystem
 *
 * @author Antonis Papanestis a.papanestis@rl.ac.uk
 * @date   2006-01-27
 */
//=============================================================================

// Gaudi
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

// RichUtils
#include "RichUtils/RichPDIdentifier.h"

// LHCbKernel
#include "Kernel/RichSmartID32.h"

// DetDesc
#include "DetDesc/Condition.h"

// local
#include "RichDet/DeRich.h"
#include "RichDet/DeRichSystem.h"

// boost
#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

// STL
#include <algorithm>

//=============================================================================

const CLID CLID_DERichSystem = 12005; // User defined

// Retrieve Pointer to class defininition structure
const CLID& DeRichSystem::classID() { return CLID_DERichSystem; }

//=========================================================================
//  initialize
//=========================================================================
StatusCode DeRichSystem::initialize() {

  setMyName( "DeRichSystem" );

  _ri_debug << "Initialize " << name() << endmsg;

  // photon detector config
  setupPhotDetConf();

  // Load the RICH detectors
  for ( const auto rich : Rich::detectors() ) {
    SmartDataPtr<DeRich> deR( dataSvc(), DeRichLocations::location( rich ) );
    m_deRich[rich] = deR;
  }

  // locations for detector numbering conditions
  const auto detNumCondLocs = DeRichLocations::detectorNumberings( m_photDetConf );
  if ( detNumCondLocs.empty() ) { return Error( "Unknown detector configuration" ); }

  // register readout conditions
  for ( const auto& loc : detNumCondLocs ) {
    const auto c = condition( loc ).path();
    updMgrSvc()->registerCondition( this, c, &DeRichSystem::buildPDMappings );
    _ri_debug << "Registered: " << c << endmsg;
  }

  // for version 1 there are separate conditions for inactive PDs
  if ( systemVersion() == 1 ) {
    for ( const auto& loc : DeRichLocations::inactivePDs() ) {
      const auto c = condition( loc ).path();
      updMgrSvc()->registerCondition( this, c, &DeRichSystem::buildPDMappings );
      _ri_debug << "Registered: " << c << endmsg;
    }
  }

  // Run first update
  const auto sc = updMgrSvc()->update( this );
  if ( sc.isFailure() ) error() << "Failed to update mappings" << endmsg;

  _ri_debug << "DeRichSystem initialized " << endmsg;

  return sc;
}

//=========================================================================
// setup photon detector configuration
//=========================================================================
void DeRichSystem::setupPhotDetConf() noexcept {
  // assume HPD by default
  m_photDetConf = Rich::HPDConfig;
  // get condition names for detector numbers
  const std::string str_PhotoDetConfig      = "RichPhotoDetectorConfiguration";
  const std::string str_PhotoDetConfigValue = "DetectorConfiguration";
  if ( hasCondition( str_PhotoDetConfig ) ) {
    const auto deRC = condition( str_PhotoDetConfig );
    if ( deRC->exists( str_PhotoDetConfigValue ) ) {
      m_photDetConf = ( Rich::RichPhDetConfigType )( deRC->param<int>( str_PhotoDetConfigValue ) );
    }
  }
}

//=========================================================================
// build PD Mappings
//=========================================================================
StatusCode DeRichSystem::buildPDMappings() {

  _ri_debug << "Update triggered for PD numbering maps" << endmsg;

  // clear maps and containers
  m_soft2hard.clear();
  m_hard2soft.clear();
  m_activePDSmartIDs.clear();
  m_inactivePDSmartIDs.clear();
  m_allPDSmartIDs.clear();
  m_activePDHardIDs.clear();
  m_inactivePDHardIDs.clear();
  m_allPDHardIDs.clear();
  m_smartid2copyNumber.clear();
  m_copyNumber2smartid.clear();

  // photon detector config
  setupPhotDetConf();

  // Fill the maps for each RICH
  for ( const auto rich : Rich::detectors() ) {
    const auto sc = fillMaps( rich );
    if ( !sc ) { return sc; }
  }

  return StatusCode::SUCCESS;
}

//=========================================================================
//  fillMaps
//=========================================================================
StatusCode DeRichSystem::fillMaps( const Rich::DetectorType rich ) {

  _ri_debug << "Building Mappings for " << rich << endmsg;

  std::string str_NumberOfPDs              = "";
  std::string str_PDSmartIDs               = "";
  std::string str_PDHardwareIDs            = "";
  std::string str_PDCopyNumbers            = "";
  std::string str_InactivePDListInSmartIDs = "";
  std::string str_InactivePDs              = "";

  if ( Rich::PMTConfig == m_photDetConf ) {
    str_NumberOfPDs              = "NumberOfPMTs";
    str_PDSmartIDs               = "PMTSmartIDs";
    str_PDHardwareIDs            = "PMTHardwareIDs";
    str_PDCopyNumbers            = "PMTCopyNumbers";
    str_InactivePDListInSmartIDs = "InactivePMTListInSmartIDs";
    str_InactivePDs              = "InactivePMTs";
  } else if ( Rich::HPDConfig == m_photDetConf ) {
    str_NumberOfPDs              = "NumberOfHPDs";
    str_PDSmartIDs               = "HPDSmartIDs";
    str_PDHardwareIDs            = "HPDHardwareIDs";
    str_PDCopyNumbers            = "HPDCopyNumbers";
    str_InactivePDListInSmartIDs = "InactiveHPDListInSmartIDs";
    str_InactivePDs              = "InactiveHPDs";
  } else {
    return Error( "Unknown detector configuration" );
  }

  // load conditions
  const auto detNumCondLocs = DeRichLocations::detectorNumberings( m_photDetConf );
  if ( detNumCondLocs.empty() ) { return Error( "Unknown detector configuration" ); }
  _ri_debug << "Loading Conditions from " << detNumCondLocs[rich] << endmsg;
  const auto numbers = condition( detNumCondLocs[rich] );
  _ri_debug << detNumCondLocs[rich] << " since:" << numbers->validSince().format( true )
            << " till:" << numbers->validTill().format( true ) << endmsg;
  SmartRef<Condition> inactives;
  if ( systemVersion() == 1 ) {
    const auto inactiveLocs = DeRichLocations::inactivePDs();
    if ( inactiveLocs.empty() ) { return Error( "No inactive PD conditions found" ); }
    inactives = condition( inactiveLocs[rich] );
    _ri_debug << "Inactive list since:" << inactives->validSince().format( true )
              << " till:" << inactives->validTill().format( true ) << endmsg;
  }

  // local type for vector from Conditions
  using CondData = std::vector<LHCb::RichSmartID::KeyType>;

  // number of PDs
  const unsigned int nPDs = numbers->param<int>( str_NumberOfPDs );
  _ri_verbo << "Condition " << str_NumberOfPDs << " = " << nPDs << endmsg;

  // vector of PD RichSmartIDs
  const auto& softIDs = numbers->paramVect<int>( str_PDSmartIDs );
  _ri_verbo << "Condition " << str_PDSmartIDs << " = " << softIDs << endmsg;

  // vector of PD hardware IDs
  const auto& hardIDs = numbers->paramVect<int>( str_PDHardwareIDs );
  _ri_verbo << "Condition " << str_PDHardwareIDs << " = " << hardIDs << endmsg;

  // vector of PD Copy numbers
  const auto& copyNs = numbers->paramVect<int>( str_PDCopyNumbers );
  _ri_verbo << "Condition " << str_PDCopyNumbers << " = " << copyNs << endmsg;

  // inactive PDs
  CondData   inacts;
  const bool inactivePDListInSmartIDs( numbers->exists( str_InactivePDListInSmartIDs ) || systemVersion() == 1 );
  _ri_verbo << "Condition " << str_InactivePDListInSmartIDs << " exists = " << inactivePDListInSmartIDs << endmsg;
  if ( inactivePDListInSmartIDs ) {
    // smartIDs
    _ri_debug << "Inactive PDs are taken from the smartID list" << endmsg;
    const auto& inactsHuman = ( systemVersion() == 1 ? inactives->paramVect<int>( str_InactivePDListInSmartIDs )
                                                     : numbers->paramVect<int>( str_InactivePDListInSmartIDs ) );

    inacts.reserve( inactsHuman.size() );
    for ( const auto inpd : inactsHuman ) {
      const LHCb::RichSmartID ID( Rich::DAQ::PDIdentifier( inpd ).smartID() );
      _ri_debug << "Inactive SmartID " << inpd << " : " << ID << endmsg;
      if ( ID.isValid() ) {
        inacts.push_back( ID.key() );
        if ( !std::any_of( softIDs.begin(), softIDs.end(),
                           [&ID]( const auto& sID ) { return ID == LHCb::RichSmartID( sID ); } ) ) {
          warning() << "Inactive SmartID in list of Active IDs : " << inpd << endmsg;
        }
      } else {
        error() << "Invalid SmartID in the list of inactive PDs : " << inpd << endmsg;
      }
    }
  } else {
    // hardware IDs
    _ri_debug << "Inactive PDs are taken from the hardware list" << endmsg;
    inacts.clear();
    for ( const auto& i : numbers->paramVect<int>( str_InactivePDs ) ) { inacts.push_back( i ); }
  }
  _ri_verbo << "Condition InactiveHPDs = " << inacts << endmsg;

  // check consistency
  if ( nPDs != softIDs.size() || //
       nPDs != hardIDs.size() || //
       nPDs != copyNs.size() ) {
    error() << "Mismatch in " << rich                     //
            << " PD numbering schemes : # PDs = " << nPDs //
            << " # SmartIDs = " << softIDs.size()         //
            << " # HardIDs = " << hardIDs.size()          //
            << " # CopyNumbers = " << copyNs.size()       //
            << endmsg;
    return StatusCode::FAILURE;
  }

  // build cached mappings
  for ( auto iSoft( softIDs.begin() ), //
        iHard( hardIDs.begin() ),      //
        icopyN( copyNs.begin() );
        iSoft != softIDs.end() && //
        iHard != hardIDs.end() && //
        icopyN != copyNs.end();
        ++iSoft, ++iHard, ++icopyN ) {

    // get PD ID
    const LHCb::RichSmartID32 pdID32( *iSoft ); // needed for 32->64 bit support
    LHCb::RichSmartID         pdID( pdID32 );   // handles correct format conversion

    // PMT specific checks
    if ( Rich::PMTConfig == m_photDetConf ) {
      // check large PMT flag
      const auto check_isLarge = isLargePD( pdID );
      if ( pdID.isLargePMT() != check_isLarge ) {
        // should make this a warning/error/fatal when 'classic' PMT support is not required any longer.
        _ri_debug << "IsLarge PMT flag mis-match " << pdID << " " << pdID.isLargePMT() << "->" << check_isLarge
                  << endmsg;
        pdID.setLargePMT( check_isLarge );
      }
      // check if this PD ID has a valid DePD object, if not skip.
      // skip this for HPDs due to circular dep issues with DeRichSystem
      if ( !dePD( pdID ) ) {
        // should make this a warning/error/fatal when 'classic' PMT support is not required any longer.
        _ri_debug << pdID << " has no DePD object !" << endmsg;
        continue;
      }
    }

    // get readout numbers
    const Rich::DAQ::PDHardwareID hardID( *iHard );
    const Rich::DAQ::PDCopyNumber copyN( *icopyN );

    // debug printout
    _ri_verbo << "PD     " << pdID << " PDhardID " << hardID << endmsg;

    // Sanity checks that this PD is not already in the maps
    if ( m_soft2hard.find( pdID ) != m_soft2hard.end() ) {
      error() << "Multiple entries for PD RichSmartID " << pdID;
      return StatusCode::FAILURE;
    }
    if ( m_hard2soft.find( hardID ) != m_hard2soft.end() ) {
      error() << "Multiple entries for PD hardware ID " << (std::string)hardID << " " << pdID << endmsg;
      return StatusCode::FAILURE;
    }
    if ( m_copyNumber2smartid.find( copyN ) != m_copyNumber2smartid.end() ) {
      error() << "Multiple entries for PD copy number " << (std::string)copyN << pdID << endmsg;
      return StatusCode::FAILURE;
    }

    // set up mappings etc.

    const auto myID = ( inactivePDListInSmartIDs ? pdID.key() : LHCb::RichSmartID::KeyType( *iHard ) );
    if ( std::find( inacts.begin(), inacts.end(), myID ) == inacts.end() ) {
      m_activePDSmartIDs.push_back( pdID );
      m_activePDHardIDs.push_back( hardID );
      _ri_debug << "PD " << pdID << " hardID " << hardID << " is ACTIVE" << endmsg;
    } else {
      if ( !pdIsActive( hardID ) ) {
        error() << "PD " << pdID << " hardID " << hardID << " listed twice in INACTIVE PD list !" << endmsg;
      } else {
        m_inactivePDSmartIDs.push_back( pdID );
        m_inactivePDHardIDs.push_back( hardID );
        _ri_debug << "PD " << pdID << " hardID " << hardID << " is INACTIVE" << endmsg;
      }
    }

    // Fill maps
    m_allPDHardIDs.push_back( hardID );
    m_allPDSmartIDs.push_back( pdID );
    bool OK = true;
    OK &= safeMapFill( pdID, hardID, m_soft2hard );
    OK &= safeMapFill( hardID, pdID, m_hard2soft );
    OK &= safeMapFill( pdID, copyN, m_smartid2copyNumber );
    OK &= safeMapFill( copyN, pdID, m_copyNumber2smartid );
    if ( !OK ) return StatusCode::FAILURE;

  } // end loop over conditions data

  // Sort PD lists
  std::stable_sort( m_activePDHardIDs.begin(), m_activePDHardIDs.end() );
  std::stable_sort( m_activePDSmartIDs.begin(), m_activePDSmartIDs.end() );
  std::stable_sort( m_inactivePDHardIDs.begin(), m_inactivePDHardIDs.end() );
  std::stable_sort( m_inactivePDSmartIDs.begin(), m_inactivePDSmartIDs.end() );
  std::stable_sort( m_allPDHardIDs.begin(), m_allPDHardIDs.end() );
  std::stable_sort( m_allPDSmartIDs.begin(), m_allPDSmartIDs.end() );

  _ri_debug << "Built mappings for " << nPDs << " PDs in " << rich << endmsg;

  return StatusCode::SUCCESS;
}

//=========================================================================
//  hardwareID
//=========================================================================
const Rich::DAQ::PDHardwareID DeRichSystem::hardwareID( const LHCb::RichSmartID& smartID ) const {
  // See if this RichSmartID is known
  const auto id = m_soft2hard.find( smartID.pdID() );
  if ( UNLIKELY( m_soft2hard.end() == id ) ) {
    std::ostringstream mess;
    mess << "Unknown PD RichSmartID " << smartID.pdID();
    throw GaudiException( mess.str(), "DeRichSystem::hardwareID", StatusCode::FAILURE );
  }

  // Found, so return hardware ID
  return ( *id ).second;
}

//=========================================================================
//  richSmartID
//=========================================================================
const LHCb::RichSmartID DeRichSystem::richSmartID( const Rich::DAQ::PDHardwareID& hID ) const {
  // See if this PD hardware ID is known
  const auto id = m_hard2soft.find( hID );
  if ( UNLIKELY( m_hard2soft.end() == id ) ) {
    throw GaudiException( "Unknown PD hardware ID " + (std::string)hID, "DeRichSystem::richSmartID",
                          StatusCode::FAILURE );
  }

  // Found, so return RichSmartID
  return ( *id ).second;
}

//=========================================================================
//  richSmartID from copy number
//=========================================================================
const LHCb::RichSmartID DeRichSystem::richSmartID( const Rich::DAQ::PDCopyNumber& copyNumber ) const {
  // See if this Level0 hardware ID is known
  const auto id = m_copyNumber2smartid.find( copyNumber );
  if ( UNLIKELY( m_copyNumber2smartid.end() == id ) ) {
    throw GaudiException( "Unknown PD Copy Number " + (std::string)copyNumber, "DeRichSystem::richSmartID",
                          StatusCode::FAILURE );
  }

  // Found, so return RichSmartID
  return ( *id ).second;
}

//=========================================================================
// Obtain the Copy Number number for a given RichSmartID
//=========================================================================
const Rich::DAQ::PDCopyNumber DeRichSystem::copyNumber( const LHCb::RichSmartID& smartID ) const {
  // See if this RichSmartID is known
  const auto id = m_smartid2copyNumber.find( smartID.pdID() );
  if ( UNLIKELY( m_smartid2copyNumber.end() == id ) ) {
    std::ostringstream mess;
    mess << "Unknown PD RichSmartID " << smartID.pdID();
    throw GaudiException( mess.str(), "DeRichSystem::copyNumber", StatusCode::FAILURE );
  }

  // Found, so return copy number
  return ( *id ).second;
}

//=========================================================================
// getDePDLocation
//=========================================================================
std::string DeRichSystem::getDePDLocation( const LHCb::RichSmartID& smartID ) const {
  std::string loc;

  if ( smartID.idType() == LHCb::RichSmartID::MaPMTID ) {
    if ( deRich( smartID.rich() )->exists( "PMTPanelDetElemLocations" ) ) {
      const auto& panelLoc = deRich( smartID.rich() )->paramVect<std::string>( "PMTPanelDetElemLocations" );
      loc                  = panelLoc[smartID.panel()];
    } else {
      if ( smartID.rich() == Rich::Rich1 ) {
        loc = ( smartID.panel() == Rich::top ? DeRichLocations::Rich1Panel0 : DeRichLocations::Rich1Panel1 );
      } else {
        loc = ( smartID.panel() == Rich::left ? DeRichLocations::Rich2Panel0 : DeRichLocations::Rich2Panel1 );
      }
    }

    const auto aM     = smartID.pdCol();
    const auto aP     = smartID.pdNumInCol();
    const auto ast_aM = std::to_string( aM );
    const auto ast_aP = std::to_string( aP );
    return ( loc + "/MAPMT_MODULE:" + ast_aM + "/MAPMT:" + ast_aP );
  } else // HPDs
  {

    if ( deRich( smartID.rich() )->exists( "HPDPanelDetElemLocations" ) ) {
      const auto& panelLoc = deRich( smartID.rich() )->paramVect<std::string>( "HPDPanelDetElemLocations" );
      loc                  = panelLoc[smartID.panel()];
    } else {
      if ( smartID.rich() == Rich::Rich1 ) {
        loc = ( smartID.panel() == Rich::top ? DeRichLocations::Rich1Panel0 : DeRichLocations::Rich1Panel1 );
      } else {
        loc = ( smartID.panel() == Rich::left ? DeRichLocations::Rich2Panel0 : DeRichLocations::Rich2Panel1 );
      }
    }

    const auto cNumber = copyNumber( smartID );
    return loc + "/HPD:" + std::string( cNumber );
  }
}

//===========================================================================
