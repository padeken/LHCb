/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Plane3DTypes.h"
#include "Kernel/UTChannelID.h"
#include "UTDet/DeUTBaseElement.h"
#include <string>
#include <vector>

class DeUTSector;
class DeUTStave;

/** @class DeUTLayer DeUTLayer.h UTDet/DeUTLayer.h
 *
 *  UT Layer detector element
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

static const CLID CLID_DeUTLayer = 9303;

class DeUTLayer : public DeUTBaseElement {

public:
  using Sectors = std::vector<const DeUTSector*>;

  /** parent type */
  using parent_type = UTDetTraits<DeUTLayer>::parent;

  /** child type */
  using child_type = UTDetTraits<DeUTLayer>::child;

  /** children */
  using Children = std::vector<child_type*>;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTLayer; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** layer identifier
   *  @return identifier
   */
  unsigned int id() const { return m_id; }

  /** stereo angle
   *  @return identifier
   */
  double angle() const { return m_angle; }

  /** cosine stereo angle
   *  @return identifier
   */
  double cosAngle() const { return m_cosAngle; }

  /** sine stereo angle
   *  @return identifier
   */
  double sinAngle() const { return m_sinAngle; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /**  locate stave based on a channel id
  @return  stave */
  const DeUTStave* findStave( const LHCb::UTChannelID aChannel ) const;

  /** locate stave based on a point
  @return stave */
  const DeUTStave* findStave( const Gaudi::XYZPoint& point ) const;

  /** check whether contains
   *  @param  aChannel channel
   *  @return bool
   **/
  bool contains( const LHCb::UTChannelID aChannel ) const override {
    return ( elementID().station() == aChannel.station() && ( elementID().layer() == aChannel.layer() ) );
  }

  /** flat vector of sectors
   * @return vector of sectors
   **/
  const Sectors& sectors() const { return m_sectors; }

  /** plane corresponding to the sector
   * @return the plane
   **/
  Gaudi::Plane3D plane() const { return m_plane; }

  /**
   * Nickname for the layer
   **/
  const std::string& nickname() const { return m_nickname; }

  /** vector of children **/
  const Children& staves() const { return m_staves; }

  /**
   * fraction active channels
   * @return bool fraction active
   **/
  double fractionActive() const;

  /** ouput operator for class DeUTLayer
   *  @see DeUTLayer
   *  @see MsgStream
   *  @param os     reference to STL output stream
   *  @param aLayer reference to DeUTLayer object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTLayer& aLayer ) { return aLayer.printOut( os ); }

  /** ouput operator for class DeUTLayer
   *  @see DeUTLayer
   *  @see MsgStream
   *  @param os     reference to MsgStream output stream
   *  @param aLayer reference to DeUTLayer object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTLayer& aLayer ) { return aLayer.printOut( os ); }

private:
  StatusCode cachePlane();

  Sectors        m_sectors;
  std::string    m_nickname;
  Gaudi::Plane3D m_plane;

  unsigned int m_id       = 0u;
  double       m_angle    = 0.0;
  double       m_sinAngle = 0.0;
  double       m_cosAngle = 0.0;

  /** make flat list of lowest descendents  and also layers **/
  void flatten();

  Children     m_staves;
  parent_type* m_parent = nullptr;
};

[[deprecated( "please deref first" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTLayer* aLayer ) {
  return os << *aLayer;
}
[[deprecated( "please deref first" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTLayer* aLayer ) {
  return os << *aLayer;
}
