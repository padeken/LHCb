/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "UTDet/DeUTBaseElement.h"
#include <string>

namespace UTDetFun {

  struct SortByY {
    template <typename Element>
    bool operator()( Element* obj1, Element* obj2 ) const {
      return ( !obj1 ) ? true : ( !obj2 ) ? false : obj1->globalCentre().y() < obj2->globalCentre().y();
    }
  };

  /// equal by (nick)name
  inline auto equal_by_name( std::string_view name ) {
    return [name]( const auto& obj ) { return obj->nickname() == name; };
  }

} // namespace UTDetFun
