/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Plane3DTypes.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/LineTraj.h"
#include "Kernel/UTChannelID.h"
#include "LHCbMath/LineTypes.h"
#include "UTDet/DeUTBaseElement.h"
#include "UTDet/DeUTSensor.h"
#include "UTDet/DeUTStave.h"
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

/** @class DeUTSector DeUTSector.h UTDet/DeUTSector.h
 *
 *  Class representing a UT Sector
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

static const CLID CLID_DeUTSector = 9320;

class DeUTSector : public DeUTBaseElement {

public:
  /** status enum
   * <b> For details on definitions see:</b>
   * \li <a href="http://ckm.physik.unizh.ch/software/det/deadStrips.php"><b>documentation</b></a><p>
   */
  enum Status {
    OK              = 0,
    Open            = 1,
    Short           = 2,
    Pinhole         = 3,
    ReadoutProblems = 4,
    NotBonded       = 5,
    LowGain         = 6,
    Noisy           = 7,
    OtherFault      = 9,
    Dead            = 10,
    UnknownStatus   = 100
  };

  using Sensors = std::vector<DeUTSensor const*>;
  using UTTraj  = LHCb::LineTraj<double>;

  /** parent type */
  using parent_type = UTDetTraits<DeUTSector>::parent;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTSector; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /**
   * Retrives the hybrid type
   * @return the hybrid type
   */
  std::string hybridType() const;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** column number */
  unsigned int column() const { return m_parent->column(); }

  /** row Number.... */
  unsigned int row() const { return m_row; };

  /** production ID --> in fact parent ID */
  unsigned int prodID() const;

  /** get the noise of the corresponding strip
   * @param aChannel channel
   * @return float noise of the strip
   */
  float noise( LHCb::UTChannelID aChannel ) const;

  /** get the average noise in the sector
   * @return float average noise
   */
  float sectorNoise() const;

  /** get the average noise of a beetle
   * @param beetle beetle number (1-4)
   * @return float average noise
   */
  float beetleNoise( unsigned int beetle ) const;

  /** get the average noise of a beetle port
   * @param beetle beetle number (1-4)
   * @param port beetle port number (1-3)
   * @return float average noise
   */
  float portNoise( unsigned int beetle, unsigned int port ) const;

  /** set the Noise of the corresponding strip
   * @param strip strip number
   * @param value Noise value
   */
  void setNoise( unsigned int strip, double value );

  /** set the Noise of the corresponding channel
   * @param chan channel
   * @param value Noise value
   */
  void setNoise( LHCb::UTChannelID chan, double value ) { setNoise( chan.strip(), value ); }

  /** set the Noise vector
   * @param values Noise vector
   */
  void setNoise( std::vector<double> values );

  /** get the Noise of the corresponding strip
   * @param aChannel channel
   * @return float noise of the strip
   */
  float rawNoise( LHCb::UTChannelID aChannel ) const;

  /** get the average raw noise in the sector
   * @return float average noise
   */
  float rawSectorNoise() const;

  /** get the average raw noise of a beetle
   * @param beetle beetle number (1-4)
   * @return float average noise
   */
  float rawBeetleNoise( unsigned int beetle ) const;

  /** get the average raw noise of a beetle port
   * @param beetle beetle number (1-4)
   * @param port beetle port number (1-3)
   * @return float average noise
   */
  float rawPortNoise( unsigned int beetle, unsigned int port ) const;

  /** get the common mode noise of the corresponding strip
   * @param aChannel channel
   * @return float noise of the strip
   */
  float cmNoise( LHCb::UTChannelID aChannel ) const;

  /** get the average common noise in the sector
   * @return float average noise
   */
  float cmSectorNoise() const;

  /** get the average common mode noise of a beetle
   * @param beetle beetle number (1-4)
   * @return float average noise
   */
  float cmBeetleNoise( unsigned int beetle ) const;

  /** get the average common mode noise of a beetle port
   * @param beetle beetle number (1-4)
   * @param port beetle port number (1-3)
   * @return float average noise
   */
  float cmPortNoise( unsigned int beetle, unsigned int port ) const;

  /** set the cmNoise of the corresponding strip
   * @param strip strip number
   * @param value cmNoise value
   */
  void setCMNoise( unsigned int strip, double value );

  /** set the cmNoise of the corresponding channel
   * @param chan channel
   * @param value cmNoise value
   */
  void setCMNoise( LHCb::UTChannelID chan, double value ) { setCMNoise( chan.strip(), value ); }

  /** set the cmNoise vector
   * @param values cmNoise vector
   */
  void setCMNoise( std::vector<double> values );

  /** set the ACD count from the electron number vector
   * @param values
   */
  void setADCConversion( std::vector<double> values );

  /** get the ADC count from the electron number
   * @param e electron number
   * @param aChannel channel
   * @return ADC count
   */
  double toADC( double e, LHCb::UTChannelID aChannel ) const;

  /** get the ADC count from the electron number
   * @param e electron number
   * @param aStrip strip number
   * @return ADC count
   */
  double toADC( double e, unsigned int aStrip ) const;

  /** get the electron number from the ADC count
   * @param val ADV count
   * @param aChannel channel
   * @return electron number
   */
  double toElectron( double val, LHCb::UTChannelID aChannel ) const;

  /** get the electron number from the ADC count
   * @param val ADV count
   * @param aStrip strip number
   * @return electron number
   */
  double toElectron( double val, unsigned int aStrip ) const;

  /** sector identfier
   * @return id
   */
  unsigned int id() const { return m_id; }

  /** set sector id */
  DeUTSector& setID( const unsigned int id ) {
    m_id = id;
    return *this;
  }

  /** check whether contains
   *  @param  aChannel channel
   *  @return bool
   */
  bool contains( LHCb::UTChannelID aChannel ) const override;

  /** detector pitch
   * @return pitch
   */
  double pitch() const { return m_pitch; }

  /** number of strips
   * @return number of strips
   */
  unsigned int nStrip() const { return m_nStrip; }

  /**
   * check if valid strip number
   *
   */
  bool isStrip( unsigned int strip ) const;

  /** trajectory
   * @return trajectory for the fit
   */
  LHCb::LineTraj<double> trajectory( LHCb::UTChannelID aChan, double offset ) const;

  /** trajectory
   * @return trajectory of the first strip
   */
  LHCb::LineTraj<double> trajectoryFirstStrip() const;

  /** trajectory
   * @return trajectory of the last strip
   */
  LHCb::LineTraj<double> trajectoryLastStrip() const;

  DeUTSector& setstripflip( bool stripflip ) {
    m_stripflip = stripflip;
    return *this;
  }

  bool getstripflip() const { return m_stripflip; }

  /** Trajectory<double> parameterized along y-axis */
  void trajectory( unsigned int strip, double offset, double& dxdy, double& dzdy, double& xAtYEq0, double& zAtYEq0,
                   double& ybegin, double& yend ) const;

  /**
   * @return total capacitance
   * ie sensors, cable + pitch adaptor
   */
  double capacitance() const { return m_capacitance; }

  /**
   * @return sensor Capacitance
   */
  double sensorCapacitance() const;

  /** strip length
   * @return strip length
   */
  double stripLength() const { return m_stripLength; }

  /** thickness
   * @return double thickness
   */
  double thickness() const { return m_thickness; }

  /** get the next channel left
   * @return next chan left
   */
  LHCb::UTChannelID nextLeft( const LHCb::UTChannelID testChan ) const;

  /** get the next channel right
   * @return next chan left
   */
  LHCb::UTChannelID nextRight( const LHCb::UTChannelID testChan ) const;

  /// Workaround to prevent hidden base class function
  const std::type_info& type( const std::string& name ) const override { return ParamValidDataObject::type( name ); }
  /**
   * @return std::string type
   */
  std::string type() const { return m_type; }

  /** @return double stereo angle */
  double angle() const { return m_angle; }

  /** @return double sin of stereo angle */
  double sinAngle() const { return m_sinAngle; }

  /** @return cosine of stereo angle */
  double cosAngle() const { return m_cosAngle; }

  /** @return check if is a stereo ladder */
  bool isStereo() const { return m_isStereo; }

  /** beetle corresponding to channel  1-3 (IT) 1-4 (TT)*/
  unsigned int beetle( const LHCb::UTChannelID& chan ) const { return beetle( chan.strip() ); }

  /** beetle corresponding to channel  1-3 (IT) 1-4 (TT)*/
  unsigned int beetle( const unsigned int strip ) const {
    return ( ( strip - 1u ) / LHCbConstants::nStripsInBeetle ) + 1u;
  };

  /** n beetle
   * @return double nBeetles
   */
  unsigned int nBeetle() const { return nStrip() / LHCbConstants::nStripsInBeetle; }

  /** measured efficiency
   * @ return double measured Eff
   */
  double measEff() const { return m_measEff; }

  /** set measured Eff of sector  */
  void setMeasEff( const double measEff );

  /** Status of sector
   @return Status of readout sector
  */
  Status sectorStatus() const { return m_status; }

  /** Status of the Beetle corresponding to strip */
  Status beetleStatus( LHCb::UTChannelID chan ) const { return beetleStatus( beetle( chan ) ); }

  /** Status of the Beetle with given id  1-3 (IT), 1-4 (TT) */
  Status beetleStatus( unsigned int id ) const;

  /** vector of beetle status */
  std::vector<DeUTSector::Status> beetleStatus() const;

  /** Status of channel */
  Status stripStatus( LHCb::UTChannelID chan ) const;

  /** get vector of strip status for all strips in sector */
  std::vector<Status> stripStatus() const;

  /** set the sector status */
  void setSectorStatus( const Status& newStatus );

  /** set vector of beetleStatus
   * @param unsigned int beetle [numbering from 1]
   * @param Status newStatus
   **/
  void setBeetleStatus( unsigned int beetle, const Status& newStatus );

  /** set vector of beetleStatus
   * @param LHCb::UTChannelID chan id of beetle
   * @param Status newStatus
   **/
  void setBeetleStatus( LHCb::UTChannelID chan, const Status& newStatus ) {
    setBeetleStatus( beetle( chan ), newStatus );
  }

  /** set vector of beetleStatus
   * @param unsigned int strip [numbering from 1]
   * @param Status newStatus
   **/
  void setStripStatus( unsigned int strip, const Status& newStatus );

  /** set vector of beetleStatus
   * @param LHCb::UTChannelID chan id of strip
   * @param Status newStatus
   **/
  void setStripStatus( LHCb::UTChannelID chan, const Status& newStatus ) { setStripStatus( chan.strip(), newStatus ); };

  /** short cut for strip status ok
   * @return isOKStrip
   */
  bool isOKStrip( LHCb::UTChannelID chan ) const { return stripStatus( chan ) == DeUTSector::OK; }

  /** strip to channel
   * @param strip
   * @return corresponding channel */
  LHCb::UTChannelID stripToChan( const unsigned int strip ) const;

  /** version */
  const std::string& versionString() const { return m_versionString; };

  /** dead width */
  double deadWidth() const { return m_deadWidth; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to msgstream */
  MsgStream& printOut( MsgStream& os ) const override;

  /** flat vector of sensors
   * @return vector of sensors
   */
  const Sensors& sensors() const { return m_sensors; }

  /** locate sensor based on a point
   * @return stave */
  const DeUTSensor* findSensor( const Gaudi::XYZPoint& point ) const;

  /** find the middle sensor. rounding down if odd **/
  const DeUTSensor* middleSensor() const { return m_sensors[m_sensors.size() / 2u]; }

  /** check if inside the active area
   * @param  point point in global frame
   * @param  tol   tolerance
   * @return bool isInside
   **/
  bool globalInActive( const Gaudi::XYZPoint& point, Gaudi::XYZPoint tol = {0., 0., 0.} ) const;

  /** globalInActive
   * @param  point point in global frame
   * @param  tol   tolerance
   * @return bool in bondgap
   */
  bool globalInBondGap( const Gaudi::XYZPoint& point, double tol = 0 ) const;

  /**
   * Nickname for the sensor
   **/
  const std::string& nickname() const { return m_nickname; }

  /**
   * fraction active channels
   * @return bool fraction active
   */
  double fractionActive() const;

  /** direct access to the status condition, for experts only */
  const Condition* statusCondition() const { return condition( m_statusString ); }

  /** direct access to the noise condition, for experts only */
  const Condition* noiseCondition() const { return condition( m_noiseString ); }

  /** x sense of local frame relative to global */
  bool xInverted() const { return middleSensor()->xInverted(); }

  /** y sense of local frame relative to global */
  bool yInverted() const { return middleSensor()->yInverted(); }

  std::string conditionsPathName() const;

  /** stave type */
  std::string staveType() const { return m_parent->type(); }

  /** ouput operator for class DeUTSector
   *  @see DeUTSector
   *  @see MsgStream
   *  @param os      reference to STL output stream
   *  @param aSector reference to DeUTSector object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTSector& aSector ) { return aSector.printOut( os ); }

  /** ouput operator for class DeUTSector
   *  @see DeUTSector
   *  @see MsgStream
   *  @param os      reference to MsgStream output stream
   *  @param aSector reference to DeUTSector object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTSector& aSector ) { return aSector.printOut( os ); }

  /** stream operator for status */
  friend std::ostream& operator<<( std::ostream& s, DeUTSector::Status e );

private:
  bool m_isStereo = false;

  StatusCode registerConditionsCallbacks();
  StatusCode cacheInfo();

  Sensors     m_sensors;
  double      m_thickness = 0.0;
  std::string m_nickname;

private:
  typedef std::map<unsigned int, Status> StatusMap;

  parent_type* m_parent = nullptr;
  unsigned int m_row    = 0u;
  std::string  m_hybridType;
  std::string  m_conditionPathName;

  std::string staveNumber( unsigned int chan, unsigned int reg ) const;

  StatusCode             updateStatusCondition();
  StatusCode             updateNoiseCondition();
  LHCb::LineTraj<double> createTraj( const unsigned int strip, const double offset ) const;
  void setStatusCondition( const std::string& type, const unsigned int entry, const DeUTSector::Status& newStatus );

  unsigned int m_firstStrip  = 1;
  unsigned int m_firstBeetle = 1;
  unsigned int m_id          = 0u;
  double       m_pitch       = 0.0;
  unsigned int m_nStrip      = 0u;
  double       m_capacitance = 0.0;
  double       m_stripLength = 0.0;
  // std::pair<double, double> m_range;

  double      m_deadWidth = 0.0;
  std::string m_type;

  double           m_dxdy = 0.0;
  double           m_dzdy = 0.0;
  double           m_dy   = 0.0;
  Gaudi::XYZVector m_dp0di;
  Gaudi::XYZPoint  m_p0;
  double           m_angle    = 0.0;
  double           m_cosAngle = 0.0;
  double           m_sinAngle = 0.0;
  double           m_measEff  = 0.0;

  // status info
  Status      m_status = OK;
  StatusMap   m_beetleStatus;
  StatusMap   m_stripStatus;
  std::string m_statusString  = "Status";
  std::string m_versionString = "DC06";

  // Noise info
  std::string         m_noiseString = "Noise";
  std::vector<double> m_noiseValues;
  std::vector<double> m_electronsPerADC;
  std::vector<double> m_cmModeValues;

  bool m_stripflip = true;
};

inline bool DeUTSector::contains( const LHCb::UTChannelID aChannel ) const {
  return aChannel.uniqueSector() == elementID().uniqueSector();
}

inline bool DeUTSector::isStrip( const unsigned int strip ) const {
  return strip >= m_firstStrip && strip < m_firstStrip + m_nStrip;
}

inline double DeUTSector::sensorCapacitance() const {
  const Sensors& theSensors = sensors();
  return theSensors.size() * theSensors.front()->capacitance();
}

inline void DeUTSector::trajectory( unsigned int strip, double offset, double& dxdy, double& dzdy, double& xAtYEq0,
                                    double& zAtYEq0, double& ybegin, double& yend ) const {
  auto i         = offset + strip - m_firstStrip;
  auto numstrips = ( ( m_stripflip && xInverted() ) ? ( 512 - i - 1 ) : i );

  dxdy    = m_dxdy;
  dzdy    = m_dzdy;
  xAtYEq0 = m_p0.x() + numstrips * m_dp0di.x();
  zAtYEq0 = m_p0.z() + numstrips * m_dp0di.z();
  ybegin  = m_p0.y() + numstrips * m_dp0di.y();
  yend    = ybegin + m_dy;
}

inline DeUTSector::Status DeUTSector::beetleStatus( unsigned int id ) const {
  DeUTSector::Status theStatus = sectorStatus();
  if ( theStatus == DeUTSector::OK ) {
    if ( auto iter = m_beetleStatus.find( id ); iter != m_beetleStatus.end() ) theStatus = iter->second;
  }
  return theStatus;
}

inline DeUTSector::Status DeUTSector::stripStatus( LHCb::UTChannelID chan ) const {
  DeUTSector::Status theStatus = beetleStatus( chan );
  if ( theStatus == DeUTSector::OK ) {
    if ( auto iter = m_stripStatus.find( chan.strip() ); iter != m_stripStatus.end() ) theStatus = iter->second;
  }
  return theStatus;
}

inline std::vector<DeUTSector::Status> DeUTSector::beetleStatus() const {
  std::vector<Status> vec;
  vec.resize( nBeetle() );
  for ( unsigned int iBeetle = m_firstBeetle; iBeetle <= nBeetle(); ++iBeetle ) {
    if ( sectorStatus() != DeUTSector::OK ) {
      vec[iBeetle - 1] = sectorStatus();
      continue;
    }
    auto iter = m_beetleStatus.find( iBeetle );
    if ( iter != m_beetleStatus.end() ) {
      vec[iBeetle - 1] = iter->second;
    } else {
      vec[iBeetle - 1] = DeUTSector::OK;
    }
  } // nStrip
  return vec;
}

inline std::vector<DeUTSector::Status> DeUTSector::stripStatus() const {
  std::vector<Status> vec;
  vec.resize( nStrip() );
  for ( unsigned int iStrip = m_firstStrip; iStrip <= nStrip(); ++iStrip ) {
    if ( sectorStatus() != DeUTSector::OK ) {
      vec[iStrip - 1] = sectorStatus();
      continue;
    }
    LHCb::UTChannelID chan = stripToChan( iStrip );
    if ( beetleStatus( chan ) != DeUTSector::OK ) {
      vec[iStrip - 1] = beetleStatus( chan );
      continue;
    }
    auto iter       = m_stripStatus.find( iStrip );
    vec[iStrip - 1] = ( iter != m_stripStatus.end() ? iter->second : DeUTSector::OK );
  } // nStrip
  return vec;
}

inline LHCb::UTChannelID DeUTSector::stripToChan( unsigned int strip ) const {
  return isStrip( strip ) ? LHCb::UTChannelID( elementID().type(), elementID().station(), elementID().layer(),
                                               elementID().detRegion(), elementID().sector(), strip )
                          : LHCb::UTChannelID( 0 );
}

[[deprecated( "please deref first" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTSector* aSector ) {
  return os << *aSector;
}
[[deprecated( "please deref first" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTSector* aSector ) {
  return os << *aSector;
}
