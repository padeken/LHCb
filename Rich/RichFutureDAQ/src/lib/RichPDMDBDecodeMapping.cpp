/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBDecodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

using namespace Rich::Future::DAQ;
using namespace Rich::DAQ;

void PDMDBDecodeMapping::fillRType( const DeRichSystem& richSys ) {

  using namespace Rich::DAQ;

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // Load the R Type encoding condition for this RICH
    const std::string richS = ( rich == Rich::Rich1 ? "R1" : "R2" );
    const std::string condS = richS + "_PDMDB_R_DecodePixelMap";
    if ( UNLIKELY( !richSys.hasCondition( condS ) ) ) {
      m_isInitialised = false;
    } else {

      // load the condition
      const auto cond = richSys.condition( condS );

      // load the active PDBDBs
      const auto activePDMDBs = cond->paramVect<int>( "ActivePDMDBs" );

      // Loop over active PDMDBs
      for ( const auto pdmdb : activePDMDBs ) {
        const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "R";

        // load the active Frames
        const auto activeFrames = cond->paramVect<int>( pdmdbS + "_ActiveFrames" );

        // loop over active frames
        for ( const auto frame : activeFrames ) {
          const auto frameS = "Frame" + std::to_string( frame );

          // load ECs, PMTs and Anodes data
          const std::string pA = pdmdbS + "_" + frameS;
          const auto ECs    = Rich::toArray<ElementaryCell::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_ECs" ) );
          const auto PMTs   = Rich::toArray<PMTInEC::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_PMTs" ) );
          const auto Anodes = Rich::toArray<AnodeIndex::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_Anodes" ) );

          // fill the data cache
          assert( (std::size_t)rich < m_pdmDataR.size() );
          auto& richD = m_pdmDataR[rich];
          assert( (std::size_t)pdmdb < richD.size() );
          auto& pdmdbD = richD[pdmdb];
          assert( (std::size_t)frame < pdmdbD.size() );
          auto& frameD = pdmdbD[frame];

          // loop over bit data and fill
          std::uint16_t bit{0};
          for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
            assert( (std::size_t)bit < frameD.size() ); // check bit index in range
            assert( !frameD[bit].isValid() );           // should not be initialised yet
            frameD[bit++] = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
          } // bit loop

        } // frame loop
      }   // PDMDB loop

    } // condition exists

  } // RICH loop
}

void PDMDBDecodeMapping::fillHType( const DeRichSystem& richSys ) {

  using namespace Rich::DAQ;

  // Load the H Type encoding condition for this RICH
  const std::string condS = "R2_PDMDB_H_DecodePixelMap";
  if ( UNLIKELY( !richSys.hasCondition( condS ) ) ) {
    m_isInitialised = false;
  } else {

    // load condition
    const auto cond = richSys.condition( condS );

    // load the active PDBDBs
    const auto activePDMDBs = cond->paramVect<int>( "ActivePDMDBs" );

    // Loop over active PDMDBs
    for ( const auto pdmdb : activePDMDBs ) {
      const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "H";

      // load the active Frames
      const auto activeFrames = cond->paramVect<int>( pdmdbS + "_ActiveFrames" );

      // loop over active frames
      for ( const auto frame : activeFrames ) {
        const auto frameS = "Frame" + std::to_string( frame );

        // load ECs, PMTs and Anodes data
        const std::string pA = pdmdbS + "_" + frameS;
        const auto ECs       = Rich::toArray<ElementaryCell::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_ECs" ) );
        const auto PMTs      = Rich::toArray<PMTInEC::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_PMTs" ) );
        const auto Anodes    = Rich::toArray<AnodeIndex::Type, BitsPerFrame>( cond->paramVect<int>( pA + "_Anodes" ) );

        // fill the data cache
        assert( (std::size_t)pdmdb < m_pdmDataH.size() );
        auto& pdmdbD = m_pdmDataH[pdmdb];
        assert( (std::size_t)frame < pdmdbD.size() );
        auto& frameD = pdmdbD[frame];

        // loop over bit data and fill
        std::uint16_t bit{0};
        for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
          assert( (std::size_t)bit < frameD.size() ); // check bit index in range
          assert( !frameD[bit].isValid() );           // should not be initialised yet
          frameD[bit++] = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
        } // bit loop
      }
    }

  } // condition exists
}
