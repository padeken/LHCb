/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

using namespace Rich::Future::DAQ;

void PDMDBEncodeMapping::fillRType( const DeRichSystem& richSys ) {

  using namespace Rich::DAQ;

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // Load the R Type encoding condition for this RICH
    const std::string richS = ( rich == Rich::Rich1 ? "R1" : "R2" );
    const std::string condS = richS + "_PDMDB_R_EncodePixelMap";
    if ( UNLIKELY( !richSys.hasCondition( condS ) ) ) {
      m_isInitialised = false;
    } else {

      // load the condition
      const auto cond = richSys.condition( condS );

      // get the active ECs
      const auto active_ecs = cond->paramVect<int>( "ActiveECs" );

      // Loop over active ECs
      for ( const auto ec : active_ecs ) {

        // Load the active PMTs for this EC
        const auto ecS         = "EC" + std::to_string( ec ) + "_";
        const auto active_pmts = cond->paramVect<int>( ecS + "ActivePMTs" );

        // data shortcuts
        assert( rich < Rich::NRiches );
        auto& rich_data = m_rTypeData[rich];
        assert( (std::size_t)ec < rich_data.size() );
        auto& ec_data = rich_data[ec];

        // loop over PMTs ..
        for ( const auto pmt : active_pmts ) {

          // Load the PMT data
          const std::string pmtS   = ecS + "PMT" + std::to_string( pmt ) + "_";
          const auto        pdmdbs = Rich::toArray<PDMDBID::Type, NumAnodes>( cond->paramVect<int>( pmtS + "PDMDB" ) );
          const auto frames = Rich::toArray<PDMDBFrame::Type, NumAnodes>( cond->paramVect<int>( pmtS + "Frame" ) );
          const auto bits   = Rich::toArray<FrameBitIndex::Type, NumAnodes>( cond->paramVect<int>( pmtS + "Bit" ) );

          // Fill data structure for each anode
          assert( (std::size_t)pmt < ec_data.size() );
          auto&         pmt_data = ec_data[pmt];
          std::uint16_t anode{0};
          for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
            assert( (std::size_t)anode < pmt_data.size() ); // check anode in range
            assert( !pmt_data[anode].isValid() );           // check not yet initialised
            pmt_data[anode++] = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
          } // conditions data loop

        } // active PMT loop

      } // ECs loop

    } // condition exists

  } // RICH loop
}

void PDMDBEncodeMapping::fillHType( const DeRichSystem& richSys ) {

  using namespace Rich::DAQ;

  // Load the H Type encoding condition for this RICH
  const std::string condS = "R2_PDMDB_H_EncodePixelMap";
  if ( UNLIKELY( !richSys.hasCondition( condS ) ) ) {
    m_isInitialised = false;
  } else {

    // load condition
    const auto cond = richSys.condition( condS );

    // get the active ECs
    const auto active_ecs = cond->paramVect<int>( "ActiveECs" );

    // Loop over active ECs
    for ( const auto ec : active_ecs ) {

      // Load the active PMTs for this EC
      const auto ecS         = "EC" + std::to_string( ec ) + "_";
      const auto active_pmts = cond->paramVect<int>( ecS + "ActivePMTs" );

      // data shortcuts
      assert( (std::size_t)ec < m_hTypeData.size() );
      auto& ec_data = m_hTypeData[ec];

      // loop over PMTs ..
      for ( const auto pmt : active_pmts ) {

        // Load the PMT data
        const std::string pmtS   = ecS + "PMT" + std::to_string( pmt ) + "_";
        const auto        pdmdbs = Rich::toArray<PDMDBID::Type, NumAnodes>( cond->paramVect<int>( pmtS + "PDMDB" ) );
        const auto        frames = Rich::toArray<PDMDBFrame::Type, NumAnodes>( cond->paramVect<int>( pmtS + "Frame" ) );
        const auto        bits = Rich::toArray<FrameBitIndex::Type, NumAnodes>( cond->paramVect<int>( pmtS + "Bit" ) );

        // Fill data structure for each anode
        assert( (std::size_t)pmt < ec_data.size() );
        auto&         pmt_data = ec_data[pmt];
        std::uint16_t anode{0};
        for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
          assert( (std::size_t)anode < pmt_data.size() ); // check anode in range
          assert( !pmt_data[anode].isValid() );           // check not yet initialised
          pmt_data[anode++] = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
        } // conditions data loop

      } // active PMT loop

    } // ECs loop

  } // condition exists
}
