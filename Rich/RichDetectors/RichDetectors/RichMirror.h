/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// RichDet
#include "RichDet/DeRichSphMirror.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Mirror RichMirror.h
   *
   *  Rich Spherical Mirror helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class Mirror final {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using DetElem = DeRichSphMirror;

  public:
    /// Constructor from DetDesc
    Mirror( const DetElem& m ) : m_mirr( &m ) {}

    /// Default contructor
    Mirror() = default;

  public:
    // Accesssors
    // Note for now just forward to DetDesc implementations but eventually
    // aim is to port (some?) functionality here, in preparation for (simplier) DD4HEP objects.

    /// Access cached SIMD mirror data
    inline decltype( auto ) mirrorData() const noexcept { return m_mirr->mirrorData(); }

    /// Intersection method forwarding
    template <typename... ARGS>
    inline decltype( auto ) intersects( ARGS&&... args ) const {
      return m_mirr->intersects( std::forward<ARGS>( args )... );
    }

    /// Mirror number
    inline decltype( auto ) mirrorNumber() const noexcept { return m_mirr->mirrorNumber(); }

    /// Retrieves the centre of curvarute of this mirror
    inline decltype( auto ) centreOfCurvature() const noexcept { return m_mirr->centreOfCurvature(); }

    /// Retrieves the centre this mirror on the reflective surface.
    inline decltype( auto ) mirrorCentre() const noexcept { return m_mirr->mirrorCentre(); }

    /// Retrieves the radius of this spherical mirror
    inline decltype( auto ) radius() const noexcept { return m_mirr->radius(); }

    /// Retrieves the normal vector at the centre of the mirror
    inline decltype( auto ) centreNormal() const noexcept { return m_mirr->centreNormal(); }

    /// Retrieves the plane defined by the centre normal and the centre
    inline decltype( auto ) centreNormalPlane() const noexcept { return m_mirr->centreNormalPlane(); }

    /// Returns a pointer to the tabulated property that holds the reflectivity
    inline decltype( auto ) reflectivity() const noexcept { return m_mirr->reflectivity(); }

  private:
    // data

    /// DetDesc Rich1 object
    const DetElem* m_mirr{nullptr};
  };

} // namespace Rich::Detector
