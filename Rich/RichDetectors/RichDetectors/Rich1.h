/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Local
#include "RichDetectors/RichX.h"

// RichDet
#include "RichDet/DeRich1.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Rich1 Rich1.h
   *
   *  Rich1 helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class Rich1 final : public Rich::Detector::RichX {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using DetElem = DeRich1;

  public:
    /// Constructor from DetDesc Rich1
    Rich1( const DetElem& deRich ) : RichX( deRich ) {}

  public:
    // conditions handling

    /// Default conditions name
    static constexpr const char* DefaultConditionKey = "Rich1Detector";

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static decltype( auto )                                                       //
    addConditionDerivation( PARENT*                     parent,                   ///< Pointer to parent algorithm
                            LHCb::DetDesc::ConditionKey key = DefaultConditionKey ///< Derived object name
    ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "Rich1::addConditionDerivation : Key=" << key << endmsg;
      }
      return LHCb::DetDesc::addConditionDerivation<Rich1( const DetElem& )> //
          ( parent->conditionDerivationMgr(),                               // manager
            {DeRichLocations::Rich1},                                       // input condition locations
            std::move( key ) );                                             // output derived condition location
    }
  };

} // namespace Rich::Detector
