/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// RichDet
#include "RichDet/DeRichPD.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class PD RichPD.h
   *
   *  Rich Spherical Mirror helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class PD final {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using DetElem = DeRichPD;

  public:
    /// Constructor from DetDesc
    PD( const DetElem& pd ) : m_pd( &pd ) {}

  public:
    // Accesssors
    // Note for now just forward to DetDesc implementations but eventually
    // aim is to port (some?) functionality here, in preparation for (simplier) DD4HEP objects.

    /// Forward ray tracing methods to DetDesc. To be moved here...
    template <typename... ARGS>
    inline decltype( auto ) detectionPoint( ARGS&&... args ) const {
      return m_pd->detectionPoint( std::forward<ARGS>( args )... );
    }

  private:
    // data

    /// DetDesc Rich1 object
    const DetElem* m_pd{nullptr};
  };

} // namespace Rich::Detector
