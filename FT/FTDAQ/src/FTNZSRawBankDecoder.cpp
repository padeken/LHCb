/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FTDigit.h"
#include "Event/FTLiteCluster.h"
#include "Event/RawEvent.h"
#include "FTDAQ/FTDAQHelper.h"
#include "FTRawBankParams.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Transformer.h"
#include "IFTReadoutTool.h"
#include "Kernel/MultiIndexedContainer.h"
#include "range/v3/view/subrange.hpp"
#include "range/v3/view/transform.hpp"
#include <cstddef> //Necessary for bit to int conversion

using namespace Gaudi::Functional;

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
using FTDigits       = LHCb::FTDigit::FTDigits;

/** @class FTNZSRawBankDecoder FTNZSRawBankDecoder.h
 *  Decode the FTNZS raw bank into FTDigits and FTLiteClusters
 *
 *  @author Lex Greeven, Sevda Esen
 *  @date   2020-02-10
 */
class FTNZSRawBankDecoder
    : public MultiTransformer<std::tuple<FTDigits, FTLiteClusters>( const EventContext& evtCtx, const LHCb::RawEvent& ),
                              Gaudi::Functional::Traits::BaseClass_t<FixTESPath<Gaudi::Algorithm>>> {
public:
  /// Standard constructor
  FTNZSRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  std::tuple<FTDigits, FTLiteClusters> operator()( const EventContext&   evtCtx,
                                                   const LHCb::RawEvent& rawEvent ) const override;

private:
  PublicToolHandle<IFTReadoutTool> m_readoutTool = {this, "FTReadoutTool", "FTReadoutTool"};

  Gaudi::Property<unsigned int> m_decodingVersion{
      this,
      "DecodingVersion",
      6u,
      [=]( auto& ) { this->m_readoutTool.setEnabled( this->m_decodingVersion > 3u ); },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
      "Set the decoding version"};

  template <unsigned int version>
  std::tuple<FTDigits, FTLiteClusters> decode( const EventContext& evtCtx, LHCb::span<const LHCb::RawBank*>,
                                               unsigned int nDigits, unsigned int nClusters ) const;

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_corrupt{this, "Possibly corrupt data. Ignoring the cluster."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nonExistingModule{
      this, "Skipping digit(s) for non-existing module."};
};

//-----------------------------------------------------------------------------
// Implementation file for class : FTNZSRawBankDecoder
//
// 2020-02-10 : Lex Greeven, Sevda Esen
//-----------------------------------------------------------------------------

namespace {
  unsigned quarterFromChannel( LHCb::FTChannelID id ) { return id.uniqueQuarter() - 16u; }

  constexpr unsigned channelInBank( uint16_t c ) { return ( c >> FTRawBank::cellShift ); }

  constexpr int cell( short int c ) { return ( c >> FTRawBank::cellShift ) & FTRawBank::cellMaximum; }

  constexpr int fraction( short int c ) { return ( c >> FTRawBank::fractionShift ) & FTRawBank::fractionMaximum; }

  constexpr bool cSize( short int c ) { return ( c >> FTRawBank::sizeShift ) & FTRawBank::sizeMaximum; }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTNZSRawBankDecoder )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FTNZSRawBankDecoder::FTNZSRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer{
          name,
          pSvcLocator,
          KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives( LHCb::RawEventLocation::Other,
                                                                                LHCb::RawEventLocation::Default )},
          {KeyValue{"OutputLocation", LHCb::FTDigitLocation::Default},
           KeyValue{"NZSClusterLocation", LHCb::FTLiteClusterLocation::Default + "NZS"}}} {}

template <>
std::tuple<FTDigits, FTLiteClusters>
FTNZSRawBankDecoder::decode<0>( const EventContext& evtCtx, LHCb::span<const LHCb::RawBank*> banks,
                                unsigned int nDigits, unsigned int nClusters ) const {
  // Define SiPM and channel number
  int SiPMnumber = -1;
  int channelnumber;

  // Define the containers
  FTDigits       digits{nDigits, LHCb::getMemResource( evtCtx )};
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};

  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks

    LHCb::FTChannelID offset  = m_readoutTool->channelIDShift( bank->sourceID() );
    auto              quarter = quarterFromChannel( offset );
    auto              range   = bank->range<std::byte>();

    // Define Lambda functions to be used in loop
    auto make_digits = [&]( std::byte c ) {
      // 1 byte contains 4 channels, so loop 4 times
      for ( int i = 0; i < 4; i++ ) {
        auto bits      = ( ( c & std::byte{192} ) >> 6 ); // Select last 2 bits and shift 6 places
        auto adc_count = std::to_integer<int>( bits );
        // First bit is second bit of adc count, so shift 6 places
        auto bit1 = std::to_integer<int>( ( c & std::byte{128} ) >> 6 );
        // Second bit is first bit of adc count, so also shift 6 places
        auto bit2 = std::to_integer<int>( ( c & std::byte{64} ) >> 6 );

        auto              chan    = --channelnumber;
        LHCb::FTChannelID channel = ( ( offset.channelID() + chan ) + ( SiPMnumber << 7 ) );

        // Print to see what happened
        if ( msgLevel( MSG::DEBUG ) ) {
          if ( adc_count != 0 )
            debug() << " SiPM number: " << SiPMnumber << " channelID: " << channel << " adc count: " << adc_count
                    << " bit1: " << bit1 << " bit2: " << bit2 << endmsg;
        }

        // Add digit
        digits.addHit( std::forward_as_tuple( channel, adc_count ), quarter );

        // Shift left by 2
        c <<= 2;
      }
    }; // end of make_digits

    auto make_cluster = [&]( unsigned chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( chan, fraction, size ), quarter );
    };

    // Loop over the bytes until bank is empty
    while ( range.size() != 0 ) {
      SiPMnumber++;
      channelnumber = 128;

      // Create a 256 bit bitset and fill with rawbank
      std::bitset<256> total_bit_set;
      int              i_bit = 0;

      for ( auto chunk : range.subspan( 0, 32 ) ) {
        i_bit++;
        std::bitset<256> sub_bit_set( std::to_integer<int>( chunk ) );

        // info() << " " << std::to_integer<int>( chunk );
        total_bit_set = total_bit_set | sub_bit_set;
        if ( i_bit < 32 ) total_bit_set <<= 8;
      }

      // info() << " " << endmsg;
      // info() << total_bit_set << endmsg;

      // Skip the 22 bit header
      total_bit_set <<= 22;

      // Contains max of 16 clusters
      for ( int i = 0; i < 26; i++ ) {

        // Select the 9 bits for the cluster
        uint16_t cluster_bits = ( uint16_t )( total_bit_set >> ( 256 - 9 ) ).to_ulong();
        if ( cluster_bits == 0 ) break; // Check if we read all clusters

        // Get channel, fraction, and size
        LHCb::FTChannelID channel = offset + channelInBank( cluster_bits ) + ( SiPMnumber << 7 );

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "cluster bits " << channel << " channelID " << cell( cluster_bits ) << ", fraction bit "
                  << fraction( cluster_bits ) << " and size bit " << ( cluster_bits >> 8 ) << endmsg;
        make_cluster( channel, fraction( cluster_bits ), cSize( cluster_bits ) );
        total_bit_set <<= 9; // Shift total bits 9 so we can read the next cluster
      }

      range = range.subspan( 32 ); // Skip header and clusters we read
      // Create FTDigits
      for ( auto chunck : range.subspan( 0, 32 ) ) { make_digits( chunck ); }
      range = range.subspan( 32 ); // Skip digits we read

    } // end of loop over single rawbank
  }   // end loop over rawbanks
  return {std::move( digits ), std::move( clus )};
  ;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<FTDigits, FTLiteClusters> FTNZSRawBankDecoder::operator()( const EventContext&   evtCtx,
                                                                      const LHCb::RawEvent& rawEvent ) const {
  const auto& banks = rawEvent.banks( LHCb::RawBank::FTNZS );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of raw banks " << banks.size() << endmsg;
  if ( banks.empty() ) return {};

  // Testing the bank version
  unsigned int vrsn = banks[0]->version();
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Bank version=v" << vrsn << " with decoding version=v" << m_decodingVersion.toString() << endmsg;

  // Check if decoding version corresponds with bank version (only for first bank).
  if ( UNLIKELY( vrsn != m_decodingVersion ) ) {
    error() << "Bank version=v" << vrsn << " is not compatible with decoding "
            << "version=v" << m_decodingVersion.toString() << endmsg;
    throw GaudiException( "Wrong decoding version", "FTNZSRawBankDecoder", StatusCode::FAILURE );
  }

  // Estimate total number of digits from bank sizes
  auto digitsAndClusters = [&]( unsigned int nDigits, unsigned int nClusters ) {
    switch ( m_decodingVersion.value() ) {
    case 0:
      return decode<0>( evtCtx, banks, nDigits, nClusters );
    default:
      throw GaudiException( "Unknown decoder version: " + std::to_string( vrsn ), __FILE__, StatusCode::FAILURE );
    };
  }( LHCb::FTDAQ::nbFTDigits( banks ), LHCb::FTDAQ::nbFTClusters( banks ) );

  return digitsAndClusters;
}
