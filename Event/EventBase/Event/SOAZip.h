/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LHCbMath/SIMDWrapper.h"
#include "Proxy.h"
#include "Zip.h"

#if defined( __clang__ ) && ( __clang_major__ < 11 ) || defined( __APPLE__ ) && ( __clang_major__ < 12 )
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN                                                                    \
    _Pragma( "clang diagnostic push" ) _Pragma( "clang diagnostic ignored \"-Wunused-lambda-capture\"" )
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END _Pragma( "clang diagnostic pop" )
#else
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN
#  define SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END
#endif

/**
 * Zip of SOACollections
 *
 * A zip is a thin wrapper arround one or more SOACollection based structures. It
 * is specialized using one of the SIMDWrapper backend and provides ways to iterate,
 * filter or copy a set of collections synchronously.
 *
 * The iterator returns a ZipProxy object (see Proxy.h)
 *
 * To construct a zip of multiple containers, use:
 * `LHCb::v2::Event:make_zip<SIMDWrapper::InstructionSet::Best>( containerA, containerB, ... )`
 * (Best is the default SIMD and can be omitted)
 *
 * To construct a zip with a single container, prefer the shorter versions:
 * `container.scalar()` or `container.simd<SIMDWrapper::InstructionSet::Best>()`
 * (Best is the default SIMD and can be omitted)
 *
 * Note: a Zip only keep pointers to existing containers and does not own any memory
 * thus it should always live on the stack where it can be optimized away by the
 * compiler.
 */
namespace LHCb::v2::Event {
  /** Helper to map Best onto its current meaning, leaving other values alone.
   */
  template <SIMDWrapper::InstructionSet s>
  inline constexpr SIMDWrapper::InstructionSet resolve_instruction_set_v = SIMDWrapper::type_map<s>::instructionSet();

  template <SIMDWrapper::InstructionSet Simd, typename... ContainerTypes>
  struct Zip {
    using container_ptr_tuple          = std::tuple<ContainerTypes*...>;
    static constexpr auto default_simd = Simd;
    using default_simd_t               = typename SIMDWrapper::type_map_t<Simd>;
    using mask_v                       = typename default_simd_t::mask_v;

    struct Iterator {
      using value_type        = ZipProxy<typename ContainerTypes::template proxy_type<
          resolve_instruction_set_v<Simd>, LHCb::Pr::ProxyBehaviour::Contiguous, ContainerTypes>...>;
      using pointer           = value_type const*;
      using reference         = value_type&;
      using const_reference   = value_type const&;
      using difference_type   = int;
      using iterator_category = std::random_access_iterator_tag;
      using simd_t            = default_simd_t;
      container_ptr_tuple m_containers;
      int                 m_offset{0};

      Iterator() : m_containers{}, m_offset{} {}
      Iterator( container_ptr_tuple containers, int offset ) : m_containers{containers}, m_offset{offset} {}

      auto operator*() const {
        return std::apply( [&]( auto&&... args ) { return value_type{{args, m_offset}...}; }, m_containers );
      }

      Iterator& operator++() {
        m_offset += simd_t::size;
        return *this;
      }
      Iterator& operator--() {
        m_offset -= simd_t::size;
        return *this;
      }
      Iterator operator++( int ) {
        Iterator retval = *this;
        m_offset += simd_t::size;
        return retval;
      }
      Iterator operator--( int ) {
        Iterator retval = *this;
        m_offset -= simd_t::size;
        return retval;
      }
      Iterator& operator+=( difference_type n ) {
        m_offset += n * simd_t::size;
        return *this;
      }
      friend bool operator==( Iterator const& lhs, Iterator const& rhs ) {
        assert( lhs.m_containers == rhs.m_containers );
        return lhs.m_offset == rhs.m_offset;
      }
      friend bool            operator!=( Iterator const& lhs, Iterator const& rhs ) { return not( lhs == rhs ); }
      friend difference_type operator-( Iterator const& lhs, Iterator const& rhs ) {
        assert( ( lhs.m_offset - rhs.m_offset ) % simd_t::size == 0 );
        return ( lhs.m_offset - rhs.m_offset ) / simd_t::size;
      }
    };

    Zip( ContainerTypes*... containers ) : m_containers( containers... ) {}

    template <SIMDWrapper::InstructionSet OtherDefaultSimd>
    Zip( Zip<OtherDefaultSimd, ContainerTypes...> old ) : m_containers{std::move( old.m_containers )} {}

    auto begin() const { return Iterator{m_containers, 0}; }
    auto end() const {
      // m_offset is incremented by dType::size each time, so repeatedly
      // incrementing begin() generally misses {m_tracks, m_tracks->size()}
      int num_chunks = ( size() + default_simd_t::size - 1 ) / default_simd_t::size;
      int max_offset = num_chunks * default_simd_t::size;
      return Iterator{m_containers, max_offset};
    }

    auto operator[]( int i ) const { return *Iterator{m_containers, i}; }

    template <LHCb::Pr::ProxyBehaviour Behaviour>
    using zip_proxy_type = ZipProxy<
        typename ContainerTypes::template proxy_type<resolve_instruction_set_v<Simd>, Behaviour, ContainerTypes>...>;
    template <LHCb::Pr::ProxyBehaviour Behaviour>
    using const_zip_proxy_type =
        ZipProxy<typename ContainerTypes::template proxy_type<resolve_instruction_set_v<Simd>, Behaviour,
                                                              ContainerTypes const>...>;

    auto gather( typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>::OffsetType const& indices ) {
      return std::apply(
          [&]( auto&&... args ) {
            return zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>{{args, indices}...};
          },
          m_containers );
    }
    auto
    gather( typename const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>::OffsetType const& indices ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>{{args, indices}...};
          },
          m_containers );
    }
    auto scalar_fill( typename zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>::OffsetType const& offset ) {
      return std::apply(
          [&]( auto&&... args ) {
            return zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>{{args, offset}...};
          },
          m_containers );
    }
    auto
    scalar_fill( typename const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>::OffsetType const& offset ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return const_zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>{{args, offset}...};
          },
          m_containers );
    }

    auto size() const { return std::get<0>( m_containers )->size(); }
    auto empty() const { return std::get<0>( m_containers )->empty(); }
    auto zipIdentifier() const { return std::get<0>( m_containers )->zipIdentifier(); }

    void reserve( std::size_t size ) const {
      std::apply( [&]( auto&&... container ) { ( container->reserve( size ), ... ); }, m_containers );
    }

    template <typename... ContainerTypes2>
    void copy_back( Zip<Simd, ContainerTypes2...> const& old, int offset, mask_v mask ) const {
      SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_BEGIN
      std::apply(
          [&]( auto&... new_container ) {
            std::apply(
                // FIXME: new_container... is added to the lambda capture as a workarround for clang 8
                [&, new_container...]( const auto&... old_container ) {
                  ( new_container->template copy_back<default_simd_t>( *old_container, offset, mask ), ... );
                },
                old.m_containers );
          },
          m_containers );
      SOA_SUPPRESS_SPURIOUS_CLANG_WARNING_END
    }

    std::tuple<std::remove_const_t<ContainerTypes>...>
    clone( Zipping::ZipFamilyNumber zf = Zipping::generateZipIdentifier() ) const {
      return std::apply(
          [&]( auto&&... args ) {
            return std::make_tuple<std::remove_const_t<ContainerTypes>...>( {zf, *args}... );
          },
          m_containers );
    }

    template <typename F>
    auto filter( F&& filt ) const {
      auto out      = this->clone();
      using zip_t   = Zip<Simd, std::remove_const_t<ContainerTypes>...>;
      zip_t out_zip = std::apply( [&]( auto&&... args ) { return zip_t( &args... ); }, out );
      out_zip.reserve( this->size() );
      for ( auto const& chunk : *this ) {
        auto filt_mask = std::invoke( filt, chunk );
        out_zip.copy_back( *this, chunk.offset(), chunk.loop_mask() && filt_mask );
      }
      return out;
    }

    /** @brief Get a clone of this zip with a different default SIMD backend.
     *  @tparam NewDefaultSimd New default SIMD backend.
     */
    template <SIMDWrapper::InstructionSet NewDefaultSimd>
    auto with() const {
      return Zip<NewDefaultSimd, ContainerTypes...>{*this};
    }

    /** @brief Check if two zips refer to the same containers.
     */
    friend bool operator==( Zip const& lhs, Zip const& rhs ) { return lhs.m_containers == rhs.m_containers; }

    /** @brief Check if two zips refer to different containers.
     */
    friend bool operator!=( Zip const& lhs, Zip const& rhs ) { return !( lhs == rhs ); }

    template <SIMDWrapper::InstructionSet, typename...>
    friend struct Zip;

  private:
    container_ptr_tuple m_containers;
  };

  template <SIMDWrapper::InstructionSet def_simd = SIMDWrapper::InstructionSet::Best, typename... ContainerTypes>
  auto make_zip( ContainerTypes&... containers ) {
    return Zip<def_simd, ContainerTypes...>( &containers... );
  }

  template <typename>
  struct is_zip : std::false_type {};

  template <SIMDWrapper::InstructionSet def_simd, typename... Ts>
  struct is_zip<Zip<def_simd, Ts...>> : std::true_type {};

  /** Helper to determine if a given type is a Zip<...> type.
   */
  template <typename T>
  inline constexpr bool is_zip_v = is_zip<T>::value;

  namespace detail {
    template <typename, typename = void>
    struct is_zippable : std::false_type {};

    template <typename... Ts>
    struct is_zippable<std::tuple<Ts...>,
                       std::void_t<typename Ts::template proxy_type<resolve_instruction_set_v<SIMDWrapper::Best>,
                                                                    LHCb::Pr::ProxyBehaviour::Contiguous, Ts>...,
                                   decltype( make_zip( std::declval<std::add_lvalue_reference_t<Ts>>()... ) )>>
        : std::true_type {};
  } // namespace detail

  /** Helper to determine if the given types can be zipped together.
   */
  template <typename... Ts>
  inline constexpr bool is_zippable_v = detail::is_zippable<std::tuple<std::remove_cv_t<Ts>...>>::value;
} // namespace LHCb::v2::Event

// Enable header lookup of zips
template <SIMDWrapper::InstructionSet simd, typename... Ts>
struct LHCb::header_map<LHCb::v2::Event::Zip<simd, Ts...>> {
  static constexpr auto value = ( LHCb::header_map_v<std::remove_const_t<Ts>> + ... ) + "Event/SOAZip.h";
};
