/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/detected.h"
#include "Kernel/HeaderMapping.h"
#include "LHCbMath/SIMDWrapper.h"
#include "SOAExtensions/ZipUtils.h"

#include <boost/mp11/algorithm.hpp>
#include <boost/mp11/bind.hpp>
#include <boost/type_index/ctti_type_index.hpp>

#include <tuple>
namespace LHCb::Pr {
  /** Helper for getting from container type -> proxy type. When proxies are
   *  defined, specialisations of this template should also be added so that
   *  they can be found.
   */
  template <typename>
  struct Proxy {
    /** This is only defined in the unspecialised Proxy struct, it is used to
     *  detect whether or not LHCb::Pr::Proxy<T> is explicitly defined or not.
     */
    static constexpr bool unspecialised = true;
  };

  /** This enum is used to tag the data-loading and data-writing behaviour of a
   *  proxy object.
   *   -      Compress: write-only, proxy is initialised with an offset and a
   *                    mask, setters generate compress-stores operations using
   *                    the offset and mask.
   *   -    Contiguous: getters read contiguously from the given scalar offset,
   *                    setters write contiguously to the same offset.
   *   -    ScalarFill: read-only, getters read a single value from the given
   *                    scalar offset and return a SIMD vector filled with
   *                    copies of this value.
   *   - ScatterGather: proxy is initialised with a SIMD vector of offsets,
   *                    getters gather from these offsets, setters scatter to
   *                    them [the scatter is not implemented yet].
   */
  enum struct ProxyBehaviour { Compress, Contiguous, ScalarFill, ScatterGather };
} // namespace LHCb::Pr

/** Helper macros for defining LHCb::Pr::Proxy specialisations
 */

/** Start the class declaration of a proxy type
 *
 *  Typically a proxy declaration would look something like:
 *
 *  namespace Some::Appropriate::Namespace {
 *    DECLARE_PROXY( Proxy ) {
 *      PROXY_METHODS( Proxy, dType, ContainerType, m_container );
 *      auto someField() const { return m_container->field<dType>( this->offset() ); }
 *    };
 *  } // namespace Some::Appropriate::Namespace
 *
 *  REGISTER_PROXY( ContainerType, Some::Appropriate::Namespace::Proxy );
 */
#define DECLARE_PROXY( Proxy )                                                                                         \
  template <typename LHCb__Pr__MergedProxy, SIMDWrapper::InstructionSet LHCb__Pr__Proxy__simd,                         \
            LHCb::Pr::ProxyBehaviour LHCb__Pr__Proxy__behaviour__tparam, typename LHCb__Pr__Container>                 \
  struct Proxy

/** Declare a proxy type as a friend of the current class -- the point here is
 *  to avoid hardcoding the template signature of the proxies, in case we need
 *  to change it in future
 */
#define DECLARE_PROXY_FRIEND( Proxy )                                                                                  \
  template <typename, SIMDWrapper::InstructionSet, LHCb::Pr::ProxyBehaviour, typename>                                 \
  friend struct Proxy

/** Define methods that are common to all proxy types.
 *  To minimise the amount of #define'd code, wherever possible these defer to
 *  the derived proxy type.
 *  @todo Drop the Container argument, it isn't **really** used anymore
 *  @todo Make the enum value (`LHCb__Pr__Proxy__simd`) available instead of the `simd_t` value
 *  @todo Concretely @olupton proposes:
 *        - PROXY_METHODS( Proxy, Behaviour, simd_enum )
 *        - static constexpr Behaviour = LHCb__Pr__Proxy__behaviour__tparam;
 *        - full_proxy() as defined below
 *        - zipIdentifier() as container().zipIdentifier()
 *        - LHCb__Pr__Container& container() const { return *m_magic_container_name; }
 *        - Proxy implementations can get the old simd_t with SIMDWrapper::type_map_t<simd_enum>
 *        - operator== and operator!= as defined below
 *        - Maybe some kind of conditionally-enabled helper in case the container type is based on SOACollection.
 */
#define PROXY_METHODS( Proxy, simd_t, Container, m_container )                                                         \
  using Container = LHCb__Pr__Container;                                                                               \
  using simd_t    = typename SIMDWrapper::type_map_t<LHCb__Pr__Proxy__simd>;                                           \
  LHCb__Pr__Container* m_container{nullptr};                                                                           \
  Proxy( LHCb__Pr__Container* container ) : m_container{container} {}                                                  \
  LHCb__Pr__Container*         container_ptr() const { return m_container; }                                           \
  LHCb__Pr__MergedProxy const& full_proxy() const { return static_cast<LHCb__Pr__MergedProxy const&>( *this ); }       \
  friend bool    operator==( Proxy const& lhs, Proxy const& rhs ) { return lhs.m_container == rhs.m_container; }       \
  friend bool    operator!=( Proxy const& lhs, Proxy const& rhs ) { return !( lhs == rhs ); }                          \
  auto           size() const { return container_ptr()->size(); }                                                      \
  constexpr auto offset() const { return full_proxy().offset(); }                                                      \
  constexpr auto indices() const { return full_proxy().indices(); }                                                    \
  constexpr auto loop_mask() const { return full_proxy().loop_mask(); }                                                \
  constexpr auto zipIdentifier() const { return container_ptr()->zipIdentifier(); }                                    \
  template <typename Data>                                                                                             \
  constexpr auto load_vector( Data const* ptr ) const {                                                                \
    return full_proxy().load_vector( ptr );                                                                            \
  }                                                                                                                    \
  template <typename Data, typename Value>                                                                             \
  constexpr auto set_vector( Data* ptr, Value const& value ) const {                                                   \
    return full_proxy().set_vector( ptr, value );                                                                      \
  }                                                                                                                    \
  static constexpr auto width() { return LHCb__Pr__MergedProxy::width(); }                                             \
  static constexpr auto behaviour() { return LHCb__Pr__MergedProxy::behaviour(); }

/** Register the given proxy type as being the one needed to iterate over the
 *  given key type. This must be called at global scope.
 */
#define REGISTER_PROXY( KeyType, ProxyType )                                                                           \
  template <>                                                                                                          \
  struct LHCb::Pr::Proxy<KeyType> {                                                                                    \
    template <typename MergedProxy, SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour,              \
              typename Container>                                                                                      \
    using type = ProxyType<MergedProxy, simd, behaviour, Container>;                                                   \
  };                                                                                                                   \
  static_assert( LHCb::Pr::is_zippable_v<KeyType>, "REGISTER_PROXY used with a non-zippable key type." )

namespace LHCb::Pr::detail {
  /** Helper to determine if the given type has a reserve( std::size_t ) method
   */
  template <typename T>
  using has_reserve_ = decltype( std::declval<T>().reserve( std::size_t{} ) );

  template <typename T>
  inline constexpr bool has_reserve_v = Gaudi::cpp17::is_detected_v<has_reserve_, T>;

  /** Helper to deterimine if the given type has a .size() method
   */
  template <typename T>
  using has_size_ = decltype( std::declval<T>().size() );

  template <typename T>
  inline constexpr bool has_size_v = Gaudi::cpp17::is_detected_v<has_size_, T>;

  /** Helper to determine if the given type has a .get_allocator() method
   */
  template <typename T>
  using has_get_allocator_t = decltype( std::declval<T>().get_allocator() );

  template <typename T>
  using has_get_allocator = Gaudi::cpp17::is_detected<has_get_allocator_t, T>;

  /** Helper to check that all types in a tuple are convertible to the first type.
   */
  template <typename>
  struct convertible_from_first : std::false_type {};

  template <typename U, typename... Us>
  struct convertible_from_first<std::tuple<U, Us...>> : std::bool_constant<( std::is_convertible_v<U, Us> && ... )> {};

  /** Helper to deterimine if the given type has a .zipIdentifier method that
   *  returns a Zipping::ZipFamilyNumber
   */
  template <typename T>
  using has_zipIdentifier_ = decltype( std::declval<T>().zipIdentifier() );

  template <typename T>
  inline constexpr bool has_zipIdentifier_v =
      std::is_same_v<Gaudi::cpp17::detected_or_t<void, has_zipIdentifier_, T>, Zipping::ZipFamilyNumber>;

  /** Helper to determine if the given type has an LHCb::Pr::Proxy
   *  specialisation.
   */
  template <typename T>
  using proxy_is_unspecialised_ = decltype( LHCb::Pr::Proxy<T>::unspecialised );

  template <typename T>
  inline constexpr bool has_proxy_specialisation_v = !Gaudi::cpp17::is_detected_v<proxy_is_unspecialised_, T>;

  template <typename First, typename... Others>
  struct first {
    using type = First;
  };
} // namespace LHCb::Pr::detail

namespace LHCb::Pr {
  /** Helper that checks that the given type has .size() and .zipIdentifier()
   *  methods, and a specialisation of LHCb:Pr::Proxy<>. These are the basic
   *  criteria for make_zip() and the LHCb::Pr::Zip constructor to be
   *  considered.
   */
  template <typename T>
  inline constexpr bool       is_zippable_v =
      detail::has_size_v<T>&& detail::has_zipIdentifier_v<T>&& detail::has_proxy_specialisation_v<T>;
} // namespace LHCb::Pr

namespace LHCb::Pr::detail {
  // This is a type that we can return if we conditionally copy from all
  // elements of the zip into a single new container representing the union
  // of all of these elements
  template <typename... ContainerTypes>
  class merged_t : public ContainerTypes... {
    using index_seq           = typename std::index_sequence_for<ContainerTypes...>;
    using container_ptr_tuple = std::tuple<ContainerTypes const*...>;

    // Get the type of our I-th base class
    template <std::size_t I>
    using base_t = std::tuple_element_t<I, std::tuple<ContainerTypes...>>;

  public:
    // Adopt the convention that all containers take a ZipNumber as their
    // first constructor argument, which is the new identifier for the new
    // container, and the old instance of themself as the second argument,
    // which they should copy other data (but not container contents) from.
    // The result should be that each part of the merged_t object is copied,
    // but the size is zero
    merged_t( container_ptr_tuple const& container_ptrs )
        : merged_t( Zipping::generateZipIdentifier(), container_ptrs, index_seq{} ) {}

    template <typename dType, typename Mask>
    void copy_back( container_ptr_tuple const& container_ptrs, int offset, Mask mask ) {
      // Delegate to each part of ourself
      copy_back<dType>( container_ptrs, offset, mask, index_seq{} );
    }

    [[nodiscard]] std::size_t size() const { return base_t<0>::size(); }

    void reserve( std::size_t capacity ) { reserve( capacity, index_seq{} ); }

    using detail::first<ContainerTypes...>::type::empty;

  private:
    // Call each base class constructor:
    //   A( new_family, *a ),
    //   B( new_family, *b ),
    //   ...
    // The identifier is passed first explicitly because it changes,
    // everything else that needs to be copied can be taken from the second
    // argument
    template <std::size_t... Is>
    merged_t( Zipping::ZipFamilyNumber new_family, container_ptr_tuple const& container_ptrs,
              std::index_sequence<Is...> )
        : base_t<Is>( new_family, *std::get<Is>( container_ptrs ) )... {}

    template <typename dType, typename Mask, std::size_t... Is>
    void copy_back( container_ptr_tuple const& container_ptrs, int offset, Mask mask, std::index_sequence<Is...> ) {
      // Delegate to each part of ourself...
      ( base_t<Is>::template copy_back<dType>( *std::get<Is>( container_ptrs ), offset, mask ), ... );
    }

    template <std::size_t... Is>
    void reserve( std::size_t capacity, std::index_sequence<Is...> ) {
      ( call_reserve<Is>( capacity ), ... );
    }

    // Call reserve() on the I-th base class if it exists
    template <std::size_t I>
    void call_reserve( [[maybe_unused]] std::size_t capacity ) {
      if constexpr ( has_reserve_v<base_t<I>> ) { base_t<I>::reserve( capacity ); }
    }
  };

  template <ProxyBehaviour Behaviour, SIMDWrapper::InstructionSet Backend>
  using offset_t = std::conditional_t<
      Behaviour == ProxyBehaviour::Compress,
      // Compress
      std::tuple<int, typename SIMDWrapper::type_map_t<Backend>::mask_v>,
      std::conditional_t<Behaviour == ProxyBehaviour::Contiguous || Behaviour == ProxyBehaviour::ScalarFill,
                         // Contiguous or ScalarFill
                         int,
                         std::conditional_t<Behaviour == ProxyBehaviour::ScatterGather,
                                            // ScatterGather
                                            typename SIMDWrapper::type_map_t<Backend>::int_v,
                                            // Someone added a new ProxyBehaviour and forgot to
                                            // update offset_t
                                            void>>>;

  // Make a new proxy type that inherits from all the component ones
  // The data structure looks something like:
  //
  //    ProxyA           ProxyB
  //  ----------       ----------
  // |    A*    |     |    B*    |
  // |    a()   |     |    b()   |
  //  ----------       ----------
  //       |               |
  //  ---------------------------
  // |           offset          |
  //  ---------------------------
  //       proxy_type<A, B>
  //
  // So the "zipped" proxy inherits from the two component proxy types, and
  // the offset (which is by construction valid into containers A and B) is
  // stored in the derived class. ProxyA and ProxyB access the offset using
  // CRTP.
  template <SIMDWrapper::InstructionSet simd_, ProxyBehaviour Behaviour, typename... ContainerTypes>
  struct proxy_type : public Proxy<std::remove_const_t<ContainerTypes>>::template type<
                          proxy_type<simd_, Behaviour, ContainerTypes...>, simd_, Behaviour, ContainerTypes>... {
    static_assert( simd_ != SIMDWrapper::Best );
    static constexpr auto simd          = simd_;
    static constexpr auto behaviour_tag = Behaviour;
    using simd_t                        = SIMDWrapper::type_map_t<simd_>;
    using mask_v                        = typename simd_t::mask_v;
    using offset_type                   = offset_t<Behaviour, simd_>;
    using full_proxy_type               = proxy_type<simd_, Behaviour, ContainerTypes...>;
    using non_const_full_proxy_type     = proxy_type<simd_, Behaviour, std::remove_const_t<ContainerTypes>...>;

  private:
    // Shorthand for our base classes, indexed by the `ContainerTypes` pack.
    template <typename ContainerType>
    using member_proxy_t = typename Proxy<std::remove_const_t<ContainerType>>::template type<full_proxy_type, simd_,
                                                                                             Behaviour, ContainerType>;
    template <typename ContainerType>
    using non_const_member_proxy_t =
        typename Proxy<std::remove_const_t<ContainerType>>::template type<non_const_full_proxy_type, simd_, Behaviour,
                                                                          std::remove_const_t<ContainerType>>;

    // Get the type of the first proxy type we inherit from, this lets us
    // query the size of the underlying containers via that proxy's pointer
    // to the underlying container. By construction all of the underlying
    // containers are the same length, this should be checked in the
    // constructor.
    using first_container_t = typename std::tuple_element_t<0, std::tuple<ContainerTypes...>>;
    using first_proxy_t     = member_proxy_t<first_container_t>;

  public:
    // Make the size() method added by the PROXY_METHODS macro visible in
    // the zipped proxy types -- it doesn't matter which parent type we take
    // them from, the first one is simplest...
    using first_proxy_t::size;
    using first_proxy_t::zipIdentifier;

    constexpr static auto behaviour() { return Behaviour; }
    offset_type           offset() const { return m_offset; }
    auto                  indices() const {
      if constexpr ( behaviour_tag == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( std::is_same_v<offset_type, int> );
        return typename simd_t::int_v{offset()};
      } else if constexpr ( behaviour_tag == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        static_assert( std::is_same_v<offset_type, typename simd_t::int_v> );
        return offset();
      } else if constexpr ( behaviour_tag == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        static_assert( std::is_same_v<offset_type, int> );
        return simd_t::indices( offset() );
      } else {
        // Compress is currently the only one without an explicit branch.
        static_assert( behaviour_tag == LHCb::Pr::ProxyBehaviour::Compress,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        // This is the type for a Compress proxy.
        static_assert( std::is_same_v<offset_type, std::tuple<int, typename simd_t::mask_v>> );
        // The condition is just designed to always be false...
        static_assert( behaviour_tag != LHCb::Pr::ProxyBehaviour::Compress,
                       "indices() not implemented for write-only Compress proxy type." );
      }
    }
    static constexpr auto width() { return simd_t::size; }
    constexpr auto        loop_mask() const {
      if constexpr ( behaviour_tag == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        return simd_t::loop_mask( offset(), size() );
      } else if constexpr ( behaviour_tag == LHCb::Pr::ProxyBehaviour::ScalarFill ||
                            behaviour_tag == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        // TODO we might like to reconsider this for Gather proxies, there one
        //      could imagine having a proxy that knows which of its elements
        //      are valid
        return simd_t::mask_true();
      } else {
        static_assert( behaviour_tag == LHCb::Pr::ProxyBehaviour::Compress );
        // TODO what makes sense here?
      }
    }
    static constexpr mask_v mask_true() { return mask_v{true}; }

    /** This is a utility for writing concrete proxy implementations. Given a
     *  pointer to the start of a range of values, it performs the
     *  Behaviour-dependent operation that is needed to yield a SIMD vector
     *  filled with the correct values from the correct offsets
     */
    template <typename Data>
    constexpr auto load_vector( Data const* ptr ) const {
      using pod_t = std::decay_t<Data>;
      // We only support int and float for now
      static_assert( std::is_same_v<pod_t, int> || std::is_same_v<pod_t, float> );
      using vec_t = std::conditional_t<std::is_same_v<pod_t, int>, typename simd_t::int_v, typename simd_t::float_v>;
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        static_assert( std::is_same_v<offset_type, typename simd_t::int_v> );
        return gather( ptr, offset() );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( std::is_same_v<offset_type, int> );
        return vec_t{*std::next( ptr, offset() )};
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        // Just an offset, load a chunk
        static_assert( std::is_same_v<offset_type, int> );
        return vec_t{std::next( ptr, offset() )};
      } else {
        // Compress proxies are write-only
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress,
                       "You added a ProxyBehaviour without updating everything else!" );
        // Condition just supposed to always be false
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::Compress, "Compress proxies are write-only." );
        static_assert( std::is_same_v<offset_type, std::tuple<int, typename simd_t::mask_v>> );
      }
    }

    /** This is a utility for writing concrete proxy implementations. Given a
     *  pointer to the start of a range of values, it performs the
     *  Behaviour-dependent operation that stores `value` in that column.
     *  @todo Add a new type of proxy where "offset()" is a mask and compressstore operations are generated?
     */
    template <typename Data, typename Value>
    constexpr void set_vector( Data* ptr, Value const& value ) const {
      using pod_t = std::decay_t<Data>;
      static_assert( std::is_same_v<pod_t, int> || std::is_same_v<pod_t, float> );
      using vec_t = std::conditional_t<std::is_same_v<pod_t, int>, typename simd_t::int_v, typename simd_t::float_v>;
      static_assert( std::is_same_v<vec_t, Value> );
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        static_assert( std::is_same_v<offset_type, int> );
        value.store( std::next( ptr, offset() ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Compress ) {
        auto const [start_index, mask] = offset();
        value.compressstore( mask, std::next( ptr, start_index ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScalarFill, "ScalarFill proxies are read-only." );
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather,
                       "ScatterGather writing is not implemented yet." );
      }
    }

    /** Template alias that yields a proxy type that is the same as 'this' apart from the vector backend.
     */
    template <SIMDWrapper::InstructionSet new_simd>
    using rebind_simd = proxy_type<new_simd, Behaviour, ContainerTypes...>;

    proxy_type( ContainerTypes*... containers, offset_type offset )
        : member_proxy_t<ContainerTypes>( containers )..., m_offset{offset} {}

    /** @brief  Turn a proxy-to-non-const into a proxy-to-const.
     */
    proxy_type( non_const_full_proxy_type const& old )
        : member_proxy_t<ContainerTypes>(
              static_cast<non_const_member_proxy_t<ContainerTypes> const&>( old ).container_ptr() )...
        , m_offset{old.offset()} {}

    /** @fn     clone
     *  @brief  Return a possibly-narrower copy of this proxy.
     *  @tparam SelectedContainerTypes... Requested subset of container types.
     *
     *  Given a proxy into a zip of containers `ContainerTypes...`, return a
     *  proxy that refers only to a subset of these `SelectedContainerTypes...`
     *
     *  This is useful to get a lighter-weight proxy object if it is known that
     *  other members of the zip will not be needed. If the requested set of
     *  containers is just `ContainerTypes...` then this is equivalent to
     *  copying the proxy.
     *
     *  @todo Consider including the CTTI sort in here so that clone<A, B>() is
     *        equivalent to clone<B, A>() in the same way make_zip( a, b ) and
     *        make_zip( b, a ) are equivalent.
     */
    template <typename... SelectedContainerTypes>
    auto clone() const {
      // Check that the template parameters were sensible
      static_assert( sizeof...( SelectedContainerTypes ) > 0,
                     "You must provide at least one template argument to proxy_type::clone<Ts...>(). Otherwise you "
                     "would be requesting a proxy into zero containers." );
      using possible_types = typename boost::mp11::mp_list<ContainerTypes...>;
      using selected_types = typename boost::mp11::mp_list<SelectedContainerTypes...>;
      using is_possible_t  = boost::mp11::mp_bind_front<boost::mp11::mp_set_contains, possible_types>;
      static_assert( boost::mp11::mp_is_set<selected_types>::value,
                     "You cannot provide the same template argument to proxy_type::<Ts...>() multiple times, because "
                     "zips of several containers of the same type are not supported." );
      static_assert( boost::mp11::mp_all_of_q<selected_types, is_possible_t>::value,
                     "You cannot request a type in the 'narrow' proxy that was not present in the 'wide' proxy." );
      // The new proxy type we want to return; this is a "narrower" copy of the
      // type of `this`
      using new_proxy_type = proxy_type<simd_, Behaviour, SelectedContainerTypes...>;
      // To make an object of type new_proxy_type we need to extract the
      // container pointers corresponding to `SelectedContainerTypes...` from
      // `this`. They are held by the various base classes.
      return new_proxy_type{
          Proxy<std::remove_const_t<SelectedContainerTypes>>::template type<full_proxy_type, simd_, Behaviour,
                                                                            SelectedContainerTypes>::container_ptr()...,
          m_offset};
    }

    /** Equality means the same index and that all of the pointers to the
     *  actual containers are the same. Defer to comparison operators in our
     *  ancestors to compare those pointers.
     */
    friend bool operator==( proxy_type const& lhs, proxy_type const& rhs ) {
      return lhs.m_offset == rhs.m_offset &&
             ( ( static_cast<typename Proxy<std::remove_const_t<ContainerTypes>>::template type<
                     full_proxy_type, simd_, Behaviour, ContainerTypes> const&>( lhs ) ==
                 static_cast<typename Proxy<std::remove_const_t<ContainerTypes>>::template type<
                     full_proxy_type, simd_, Behaviour, ContainerTypes> const&>( rhs ) ) &&
               ... );
    }

    friend bool operator!=( proxy_type const& lhs, proxy_type const& rhs ) { return !( lhs == rhs ); }

  private:
    offset_type m_offset{0};
  };

  template <typename>
  struct merged_object_helper;
} // namespace LHCb::Pr::detail

namespace LHCb::Pr {
  /** This is the type returned by LHCb::Pr::make_zip */
  template <SIMDWrapper::InstructionSet def_simd, typename... ContainerTypes>
  class Zip {
    // Shorthand for the tuple of pointers to the underlying containers, these
    // are things like LHCb::Pr::Velo::Tracks const* and so on, which are
    // assumed to be stored on the TES and have a sufficiently long lifetime
    using container_ptr_tuple = std::tuple<ContainerTypes*...>;

    // std::tuple<A, B, ...> for those types in ContainerTypes that have a .get_allocator() method
    using containers_with_get_allocator_tuple =
        boost::mp11::mp_copy_if<std::tuple<ContainerTypes...>, detail::has_get_allocator>;

    // std::tuple<Alloc1, Alloc2, ...> allocator types corresponding to containers_with_get_allocator_tuple
    using allocator_types_tuple =
        boost::mp11::mp_transform<detail::has_get_allocator_t, containers_with_get_allocator_tuple>;

    // is the get_allocator() method going to be enabled based on the above?
    static constexpr bool has_get_allocator = detail::convertible_from_first<allocator_types_tuple>::value;

    // Storage of underlying container pointers for the iterable tracks wrapper
    container_ptr_tuple m_containers;

    // This is needed so that we can have a copy constructor that re-binds the
    // default SIMD setting to a different value.
    template <SIMDWrapper::InstructionSet, typename...>
    friend class Zip;

    template <typename>
    friend struct detail::merged_object_helper;

  public:
    using ContainedTypes               = std::tuple<ContainerTypes...>;
    static constexpr auto default_simd = def_simd;
    using default_simd_t               = typename SIMDWrapper::type_map_t<def_simd>;

    template <SIMDWrapper::InstructionSet simd>
    struct Iterator {
      static_assert( simd != SIMDWrapper::Best );
      using value_type        = detail::proxy_type<simd, ProxyBehaviour::Contiguous, ContainerTypes...>;
      using pointer           = value_type const*;
      using reference         = value_type&;
      using const_reference   = value_type const&;
      using difference_type   = int;
      using iterator_category = std::random_access_iterator_tag;
      using simd_t            = typename SIMDWrapper::type_map<simd>::type;
      container_ptr_tuple m_containers;
      int                 m_offset{0};
      Iterator() : m_containers{}, m_offset{} {}
      Iterator( container_ptr_tuple containers, int offset ) : m_containers{containers}, m_offset{offset} {}

      auto operator*() const {
        return std::make_from_tuple<value_type>( std::tuple_cat( m_containers, std::tuple{m_offset} ) );
      }

      Iterator& operator++() {
        m_offset += simd_t::size;
        return *this;
      }
      Iterator& operator--() {
        m_offset -= simd_t::size;
        return *this;
      }
      Iterator operator++( int ) {
        Iterator retval = *this;
        m_offset += simd_t::size;
        return retval;
      }
      Iterator operator--( int ) {
        Iterator retval = *this;
        m_offset -= simd_t::size;
        return retval;
      }
      Iterator& operator+=( difference_type n ) {
        m_offset += n * simd_t::size;
        return *this;
      }
      friend bool operator==( Iterator const& lhs, Iterator const& rhs ) {
        return lhs.m_containers == rhs.m_containers && lhs.m_offset == rhs.m_offset;
      }
      friend bool            operator!=( Iterator const& lhs, Iterator const& rhs ) { return not( lhs == rhs ); }
      friend difference_type operator-( Iterator const& lhs, Iterator const& rhs ) {
        assert( ( lhs.m_offset - rhs.m_offset ) % simd_t::size == 0 );
        return ( lhs.m_offset - rhs.m_offset ) / simd_t::size;
      }
    };

    // member types
    template <SIMDWrapper::InstructionSet s>
    static constexpr SIMDWrapper::InstructionSet resolve = SIMDWrapper::type_map<s>::instructionSet();
    using const_iterator                                 = const Iterator<resolve<default_simd>>;
    using iterator                                       = Iterator<resolve<default_simd>>;

    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour Behaviour>
    using proxy_type = detail::proxy_type<resolve<simd>, Behaviour, ContainerTypes...>;

    using value_type           = proxy_type<resolve<default_simd>, ProxyBehaviour::Contiguous>;
    using gather_value_type    = proxy_type<resolve<default_simd>, ProxyBehaviour::ScatterGather>;
    using broadcast_value_type = proxy_type<resolve<default_simd>, ProxyBehaviour::ScalarFill>;

    // Not sure if we would need the type_map<> stuff here or better to make the caller do it
    template <SIMDWrapper::InstructionSet simd>
    using rebind_value_type = proxy_type<resolve<simd>, ProxyBehaviour::Contiguous>;
    template <SIMDWrapper::InstructionSet simd>
    using rebind_gather_value_type = proxy_type<resolve<simd>, ProxyBehaviour::ScatterGather>;
    template <SIMDWrapper::InstructionSet simd>
    using rebind_broadcast_value_type = proxy_type<resolve<simd>, ProxyBehaviour::ScalarFill>;

    using iterator_category = typename iterator::iterator_category;
    using reference         = typename iterator::reference;
    using pointer           = typename iterator::pointer;
    using difference_type   = typename iterator::difference_type;

    /** Construct an iterable zip of the given containers. */
    template <typename std::enable_if_t<( is_zippable_v<std::remove_const_t<ContainerTypes>> && ... ), int> = 0>
    Zip( ContainerTypes&... containers ) : m_containers{&containers...} {
      // We assume that size() and zipIdentifier() can just be taken from the
      // 0th container, now's the time to check that assumption...!
      if ( !Zipping::areSemanticallyCompatible( containers... ) ) {
        auto info = ( std::string{System::typeinfoName( typeid( ContainerTypes ) )} + ", " + ... );
        throw GaudiException{"Asked to zip containers that are not semantically compatible: " + info, "LHCb::Pr::Zip",
                             StatusCode::FAILURE};
      }
      if ( !Zipping::areSameSize( containers... ) ) {
        auto info = ( std::string{System::typeinfoName( typeid( ContainerTypes ) )} + ", " + ... );
        throw GaudiException{"Asked to zip containers that are not the same size: " + info, "LHCb::Pr::Zip",
                             StatusCode::FAILURE};
      }
    }

    /** Move constructor that allows the vector behaviour to be changed.
     */
    template <SIMDWrapper::InstructionSet other_default_simd>
    Zip( Zip<other_default_simd, ContainerTypes...>&& other ) : m_containers{std::move( other.m_containers )} {}

    /** Copy constructor that allows the vector behaviour to be changed.
     */
    template <SIMDWrapper::InstructionSet other_default_simd>
    Zip( Zip<other_default_simd, ContainerTypes...> const& other ) : m_containers{other.m_containers} {}

    /** Get an iterator to the first element in the containers.
     *  The iterator only refers to the underlying containers, and may persist
     *  after the lifetime of the iterable zip itself (Zip) has
     *  ended.
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    Iterator<resolve<simd>> begin() const {
      return {m_containers, 0};
    }

    /** Get an iterator to one past the final element in the containers.
     *  As with begin(), the iterator only refers to the underlying containers
     *  and may be used after the lifetime of the iterable zip object has ended
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    Iterator<resolve<simd>> end() const {
      using simd_t = typename SIMDWrapper::type_map<simd>::type;
      // m_offset is incremented by dType::size each time, so repeatedly
      // incrementing begin() generally misses {m_tracks, m_tracks->size()}
      int num_chunks = ( size() + simd_t::size - 1 ) / simd_t::size;
      int max_offset = num_chunks * simd_t::size;
      return {m_containers, max_offset};
    }

    /** Return a proxy object referring to the given element of the underlying
     *  containers. If the iterable zip object was constructed with a vector
     *  'dType' then the proxy returned here will also yield vector outputs
     *  starting from the given offset.
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    auto operator[]( int offset ) const {
      return std::make_from_tuple<detail::proxy_type<resolve<simd>, ProxyBehaviour::Contiguous, ContainerTypes...>>(
          std::tuple_cat( m_containers, std::tuple{offset} ) );
    }

    /** Return a gathering proxy object referring to the given elements of the
     *  underlying containers.
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    auto gather( detail::offset_t<ProxyBehaviour::ScatterGather, simd> indices ) const {
      return std::make_from_tuple<detail::proxy_type<resolve<simd>, ProxyBehaviour::ScatterGather, ContainerTypes...>>(
          std::tuple_cat( m_containers, std::tuple{indices} ) );
    }

    /** Return a broadcasting proxy object that yields 'simd'-wide copies of
     *  the given element in the underlying containers.
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    auto scalar_fill( int offset ) const {
      return std::make_from_tuple<detail::proxy_type<resolve<simd>, ProxyBehaviour::ScalarFill, ContainerTypes...>>(
          std::tuple_cat( m_containers, std::tuple{offset} ) );
    }

    /** Return a compressing proxy object that can be used to write, using
     *  compress-store operations with the given mask, at the given offset.
     */
    template <SIMDWrapper::InstructionSet simd = default_simd>
    auto compress( int offset, typename SIMDWrapper::type_map_t<resolve<simd>>::mask_v const& mask ) const {
      return std::make_from_tuple<detail::proxy_type<resolve<simd>, ProxyBehaviour::Compress, ContainerTypes...>>(
          std::tuple_cat( m_containers, std::make_tuple( std::tuple{offset, mask} ) ) );
    }

    /** Get the size of the underlying container
     */
    [[nodiscard]] std::size_t size() const { return std::get<0>( m_containers )->size(); }

    /** Check if the underlying container is empty
     */
    [[nodiscard]] bool empty() const { return !size(); }

    /** Retrieve the zip family of the underlying container.
     */
    [[nodiscard]] Zipping::ZipFamilyNumber zipIdentifier() const {
      return std::get<0>( m_containers )->zipIdentifier();
    }

    /** Get an allocator from the zip.
     *
     * This method is only enabled if at least one member of the zip has a method named
     * get_allocator() and all members providing that method yield types are mutually
     * convertible. The allocator from the first such member is returned.
     *
     * @todo We don't actually check that all the allocator types are mutually
     *       convertible, just that the first one (which we return) is convertible to all
     *       of the others. This should be good enough...
     */
    template <bool enable = has_get_allocator, std::enable_if_t<enable, int> = 0>
    [[nodiscard]] auto get_allocator() const noexcept {
      using container_to_query_t = std::tuple_element_t<0, containers_with_get_allocator_tuple>;
      return std::get<container_to_query_t*>( m_containers )->get_allocator();
    }

    /** Get a component of the zip.
     */
    template <typename T>
    auto& get() const {
      // If T is in ContainerTypes use that, otherwise try std::add_const_t<T>
      return *std::get<std::conditional_t<boost::mp11::mp_contains<ContainedTypes, T>::value, T, std::add_const_t<T>>*>(
          m_containers );
    }

    /** Make a new structure by conditionally copying the underlying structures
     */
    template <SIMDWrapper::InstructionSet simd = default_simd, typename F>
    auto filter( F&& filt ) const {
      using simd_t = typename SIMDWrapper::type_map<simd>::type;
      // If the current object is a zip of multiple containers, we need to
      // return a detail::merged_t object. If there's only one container, we
      // can just return a new one of those.
      if constexpr ( sizeof...( ContainerTypes ) > 1 ) {
        // Zip contains multiple inputs, need a custom output type merging them
        // together.
        detail::merged_t<std::remove_const_t<ContainerTypes>...> out{m_containers};
        out.reserve( this->size() );
        for ( auto iter = begin<simd>(); iter != end<simd>(); ++iter ) {
          auto const& chunk     = *iter;
          auto        filt_mask = std::invoke( filt, chunk );
          out.template copy_back<simd_t>( m_containers, chunk.offset(), chunk.loop_mask() && filt_mask );
        }
        return out;
      } else {
        // If we're a "zip" of just one container, we can directly output a new
        // instance of that container instead of a merged_t wrapper around it
        using container_t = std::remove_const_t<typename std::tuple_element_t<0, std::tuple<ContainerTypes...>>>;
        auto const& old_container{*std::get<0>( m_containers )};
        container_t out{Zipping::generateZipIdentifier(), old_container};
        if constexpr ( detail::has_reserve_v<container_t> ) out.reserve( this->size() );
        for ( auto iter = begin<simd>(); iter != end<simd>(); ++iter ) {
          auto const& chunk     = *iter;
          auto        filt_mask = std::invoke( filt, chunk );
          out.template copy_back<simd_t>( old_container, chunk.offset(), chunk.loop_mask() && filt_mask );
        }
        return out;
      }
    }

    /** Return a copy of ourself that has a different default vector backend.
     */
    template <SIMDWrapper::InstructionSet simd>
    auto with() const {
      return Zip<simd, ContainerTypes...>{*this};
    }

    /** Two zips are the same if they have the same type and they have the same
     *  set of pointers to the actual owning containers.
     */
    friend bool operator==( Zip const& lhs, Zip const& rhs ) { return lhs.m_containers == rhs.m_containers; }
    friend bool operator!=( Zip const& lhs, Zip const& rhs ) { return !( lhs == rhs ); }
  };

  namespace detail {
    template <typename T>
    struct merged_object_helper {
      using tuple_t = std::tuple<T>;
      static constexpr auto decompose( T& x ) { return std::tie( x ); }
      static constexpr bool is_zippable_v = ::LHCb::Pr::is_zippable_v<std::remove_const_t<T>>;
    };

    template <typename... T>
    struct merged_object_helper<merged_t<T...>> {
      using tuple_t = std::tuple<T...>;
      static constexpr auto decompose( merged_t<T...>& x ) { return std::tie( static_cast<T&>( x )... ); }
      static constexpr bool is_zippable_v = ( ::LHCb::Pr::is_zippable_v<std::remove_const_t<T>> && ... );
    };

    template <typename... T>
    struct merged_object_helper<merged_t<T...> const> {
      using tuple_t = std::tuple<T const...>;
      static constexpr auto decompose( merged_t<T...> const& x ) { return std::tie( static_cast<T const&>( x )... ); }
      static constexpr bool is_zippable_v = ( ::LHCb::Pr::is_zippable_v<std::remove_const_t<T>> && ... );
    };

    template <SIMDWrapper::InstructionSet def_simd, typename... T>
    struct merged_object_helper<::LHCb::Pr::Zip<def_simd, T...>> {
      using tuple_t = std::tuple<T...>;
      // Convert std::tuple<A const*, ...> to std::tuple<A const&, ...>
      static constexpr auto decompose( ::LHCb::Pr::Zip<def_simd, T...>& x ) {
        return std::tie( *std::get<T*>( x.m_containers )... );
      }
      // This was already a zip, so we know that the contents were zippable
      static constexpr bool is_zippable_v = true;
    };

    template <SIMDWrapper::InstructionSet def_simd, typename... T>
    struct merged_object_helper<::LHCb::Pr::Zip<def_simd, T...> const> {
      using tuple_t = std::tuple<T...>;
      // Convert std::tuple<A const*, ...> to std::tuple<A const&, ...>
      static constexpr auto decompose( ::LHCb::Pr::Zip<def_simd, T...> const& x ) {
        return std::tie( *std::get<T*>( x.m_containers )... );
      }
      // This was already a zip, so we know that the contents were zippable
      static constexpr bool is_zippable_v = true;
    };

    /** Helper to sort a std::tuple of const references according to a
     *  different tuple type's ordering. e.g. given an instance of
     *    std::tuple<A const&, B const&>
     *  and the template parameter
     *    std::tuple<B, A>
     *  return an instance of std::tuple<B const&, A const&> populated from the
     *  given instance. This only works if the types A, B, ... are unique in
     *  each tuple.
     */
    template <typename>
    struct tuple_sort {};

    template <typename... SArgs>
    struct tuple_sort<std::tuple<SArgs...>> {
      template <typename... Args>
      constexpr static auto apply( std::tuple<Args...>&& in ) {
        return std::tuple<SArgs&...>{std::get<SArgs&>( std::move( in ) )...};
      }
    };

    /** Comparison of two types using Boost CTTI
     */
    template <typename T1, typename T2>
    struct ctti_sort {
      using tidx                  = boost::typeindex::ctti_type_index;
      constexpr static bool value = tidx::type_id<T1>() < tidx::type_id<T2>();
    };

    /** Do some template magic to get from the parameter pack (Args...)
     *  corresponding to the arguments of make_zip() to the sorted list of
     *  unpacked types that will be passed to Zip as template parameters.
     *  This involves applying unpacking rules for merged_t and Zip arguments
     *  and then sorting the result using Boost CTTI.
     *
     *  e.g.
     *    sorted_t<merged_t<B, C>, A>
     *  might yield
     *    std::tuple<A, B, C>
     *  (although the actual ordering is unspecified, we rely on Boost to
     *  ensure that it is stable)
     */
    template <typename... Args>
    using sorted_t =
        boost::mp11::mp_sort<boost::mp11::mp_append<typename detail::merged_object_helper<Args>::tuple_t...>,
                             ctti_sort>;

    /** Helper for full_zip_t
     */
    template <typename>
    struct full_zip {};

    template <typename... Ts>
    struct full_zip<std::tuple<Ts...>> {
      template <SIMDWrapper::InstructionSet def_simd>
      using type = Zip<def_simd, Ts...>;
    };

    /** Deduce the return type of make_zip from a parameter pack representing
     *  its arguments. This is a relatively trivial wrapper around sorted_t.
     *  The "full" in the name is because the user is required to explicitly
     *  specify the extra template parameter def_simd.
     *
     *  e.g.
     *    full_zip_t<def_simd, merged_t<B, C>, A>
     *  might yield
     *    Zip<def_simd, A, B, C>
     */
    template <SIMDWrapper::InstructionSet def_simd, typename... Args>
    using full_zip_t = typename full_zip<sorted_t<Args...>>::template type<def_simd>;
  } // namespace detail

  /** Construct an appropriate iterable wrapper from the given container(s)
   *
   *  e.g. transform LHCb::Pr::Velo::Tracks to an iterable wrapper around that
   *
   *  This is a helper function that avoids writing out too many explicit types
   *  and handles various special cases to try and minimise the number of
   *  different return types that we have to deal with.
   *
   *  There is some special handling in case some of the arguments are
   *  instances of merged_t. This is the type produced by filtering a zip. The
   *  special handling here ensures that `iterable( zip.filter(...) )` has the
   *  same type as `zip`.
   *
   *  There is also special handling for the case that some of the arguments
   *  are instances of LHCb::Pr::Zip. The special handling ensures that if one
   *  does
   *    auto x = LHCb::Pr::make_zip( a, b );
   *    auto y = LHCb::Pr::make_zip( x, c, d );
   *    auto z = LHCb::Pr::make_zip( a, b, c, d );
   *  then y and z should have the same type.
   *
   *  Finally, there is a step that re-orders the template parameters
   *  representing the types of a, b, etc. above so that the argument order
   *  doesn't matter. Note that given the expansion behaviour discussed just
   *  above, one could not always easily achieve this by hand.
   */
  template <SIMDWrapper::InstructionSet def_simd = SIMDWrapper::InstructionSet::Best, typename... PrTracks,
            typename std::enable_if_t<
                ( detail::merged_object_helper<std::remove_const_t<PrTracks>>::is_zippable_v && ... ), int> = 0>
  auto make_zip( PrTracks&... tracks ) {
    // If some of PrTracks... are merged_t<T...> then try to unpack, e.g.
    // PrTracks = {A, merged_t<B, C>} (types)
    //   tracks = {a, bc}             (const&)
    // Should unpack to
    // {A, B, C}                                                     (types)
    // {a, static_cast<B const&>( bc ), static_cast<C const&>( bc )} (const&)
    //
    // (Note that merged_t<B, C> inherits from B and C)

    // Get something like std::tuple<B, A, C> that has been unpacked following
    // the rules above and sorted using CTTI. Reordering the arguments passed
    // to this function (i.e. "tracks") would not affect sorted_t.
    using sorted_t = detail::sorted_t<PrTracks...>;

    // Get the full Zip<def_simd, B, A, C> type that we'll return
    using ret_t = typename detail::full_zip<sorted_t>::template type<def_simd>;

    // Unpack the arguments, as specified above, into a tuple of const&. This
    // will generally not be ordered correctly (i.e. it will be something like
    // std::tuple<A const&, B const&, C const&> that doesn't match sorted_t).
    auto expanded_tracks = std::tuple_cat( detail::merged_object_helper<PrTracks>::decompose( tracks )... );

    // Reorder to make a tuple of references in the order demanded by sorted_t
    auto sorted_tracks = detail::tuple_sort<sorted_t>::apply( std::move( expanded_tracks ) );

    // Finally, construct the zip object using this sorted list of references
    return std::make_from_tuple<ret_t>( std::move( sorted_tracks ) );
  }

  // Helper to get the type of a zip of the types T...
  template <typename... T>
  using zip_t = detail::full_zip_t<SIMDWrapper::InstructionSet::Best, T const...>;

  // Helper to get a scalar zip of the types T...
  template <typename... T>
  using scalar_zip_t = detail::full_zip_t<SIMDWrapper::InstructionSet::Scalar, T const...>;

  template <typename...>
  struct is_zip : std::false_type {};

  template <SIMDWrapper::InstructionSet def_simd, typename... Args>
  struct is_zip<Zip<def_simd, Args...>> : std::true_type {};

  template <typename T>
  inline constexpr bool is_zip_v = is_zip<T>::value;

  template <typename T>
  struct is_proxy : std::false_type {};

  template <SIMDWrapper::InstructionSet simd, ProxyBehaviour Behaviour, typename... Ts>
  struct is_proxy<detail::proxy_type<simd, Behaviour, Ts...>> : std::true_type {};

  template <typename T>
  inline constexpr bool is_proxy_v = is_proxy<T>::value;
} // namespace LHCb::Pr

// Enable header lookup for non-owning zips
template <SIMDWrapper::InstructionSet def_simd, typename... ContainerTypes>
struct LHCb::header_map<LHCb::Pr::Zip<def_simd, ContainerTypes...>> {
  static constexpr auto value = ( LHCb::header_map_v<std::remove_const_t<ContainerTypes>> + ... ) + "Event/Zip.h";
};

// Enable header lookup of non-owning zip proxies
template <SIMDWrapper::InstructionSet def_simd, LHCb::Pr::ProxyBehaviour Behaviour, typename... ContainerTypes>
struct LHCb::header_map<typename LHCb::Pr::detail::proxy_type<def_simd, Behaviour, ContainerTypes...>> {
  static constexpr auto value = ( LHCb::header_map_v<std::remove_const_t<ContainerTypes>> + ... ) + "Event/Zip.h";
};
