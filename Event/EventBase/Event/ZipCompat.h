/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOAZip.h"
#include "Event/Zip.h"
#include "LHCbMath/SIMDWrapper.h"

namespace LHCb::v2::Event::ZipCompat {
  namespace detail {
    /** @brief Should this container be zipped using the older
     *         LHCb::Pr::make_zip machinery?
     *  @tparam T Container type to check.
     */
    template <typename T>
    inline constexpr bool use_pr_zip_impl = LHCb::Pr::is_zip_v<T> || LHCb::Pr::is_zippable_v<T>;
  } // namespace detail

  /** @brief Make an iterable zip using an appropriate zip backend.
   */
  template <SIMDWrapper::InstructionSet TargetSIMD = SIMDWrapper::Best, typename T>
  auto make_zip( T& a ) {
    if constexpr ( detail::use_pr_zip_impl<std::remove_cv_t<T>> ) {
      return LHCb::Pr::make_zip<TargetSIMD>( a );
    } else {
      // the new make_zip does not transparently handle passing something that
      // is already a zip
      if constexpr ( LHCb::v2::Event::is_zip_v<std::remove_cv_t<T>> ) {
        return a.template with<TargetSIMD>();
      } else {
        return LHCb::v2::Event::make_zip<TargetSIMD>( a );
      }
    }
  }

  /** Get the zip type returned by make_zip for this container type.
   */
  template <SIMDWrapper::InstructionSet TargetSIMD, typename T>
  using zip_t = decltype( make_zip<TargetSIMD>( std::declval<T const&>() ) );

  namespace detail {
    /** Helper for simultaneously supporting LHCb::Pr::make_zip() and
     *  LHCb::v2::Event::make_zip(). This way is more powerful than
     *  just using std::conditional_t as it avoids the not-chosen branch having
     *  to be valid.
     *  @todo Remove this helper and file once LHCb::Pr::make_zip() etc. are gone
     */
    template <bool /* use LHCb::Pr implementation? */>
    struct impl {};

    /** Implementation for LHCb::Pr::make_zip()
     */
    template <>
    struct impl<true> {
      template <SIMDWrapper::InstructionSet TargetSIMD, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType>
      using proxy_type = typename zip_t<TargetSIMD, ContainerType>::template proxy_type<TargetSIMD, Behaviour>;
    };
    /** Implementation for LHCb::v2::Event::make_zip()
     */
    template <>
    struct impl<false> {
      template <SIMDWrapper::InstructionSet TargetSIMD, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType>
      using proxy_type = typename zip_t<TargetSIMD, ContainerType>::template zip_proxy_type<Behaviour>;
    };
  } // namespace detail

  /** Get a proxy type with a specific backend
   */
  template <SIMDWrapper::InstructionSet TargetSIMD, LHCb::Pr::ProxyBehaviour Behaviour, typename T>
  using proxy_t = typename detail::impl<detail::use_pr_zip_impl<T>>::template proxy_type<TargetSIMD, Behaviour, T>;

  /** Get the size of the object returned by calling .filter() on a zip. This
   *  overload should cover LHCb::Pr::make_zip(), where .size() can be called
   *  directly.
   */
  template <typename FilterResult>
  auto filtered_size( FilterResult const& filtered ) -> decltype( filtered.size() ) {
    return filtered.size();
  }
  /** Get the size of the object returned by calling .filter() on a zip. This
   *  overload should cover LHCb::v2::Event::make_zip(), where a tuple of
   *  containers is returned.
   */
  template <typename... Ts>
  auto filtered_size( std::tuple<Ts...> const& filtered_tuple ) {
    auto const first_size = std::get<0>( filtered_tuple ).size();
    assert( ( ( std::get<Ts>( filtered_tuple ).size() == first_size ) && ... ) );
    return first_size;
  }
} // namespace LHCb::v2::Event::ZipCompat