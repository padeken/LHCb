/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/CaloHypo.h"
#include "Event/PackedCaloHypo.h"

#include "GaudiAlg/Consumer.h"

/**
 * Unpacker for CaloHypo
 *
 * FIXME : despite this looks functional, the input and output is still handled the
 * old way and read/written from/to the TES via getOrCreate/put.
 * This should be fixed but requires that the algorithm consuming the data is able
 * to deal with an empty list of CaloHypos and does not seg fault in such a case as
 * it is the case now and also that DaVinci configuration does not try to unpack non
 * existing input (Tesla.default_2015 test)
 */
struct UnpackCaloHypo : Gaudi::Functional::Consumer<void()> {

  UnpackCaloHypo( const std::string& name, ISvcLocator* pSvcLocator ) : Consumer( name, pSvcLocator ) {}

  void                                        operator()() const override;
  DataObjectReadHandle<LHCb::PackedCaloHypos> m_packedHypos{this, "InputName", LHCb::PackedCaloHypoLocation::Electrons};
  DataObjectWriteHandle<LHCb::CaloHypos>      m_hypos{this, "OutputName", LHCb::CaloHypoLocation::Electrons};
};

void UnpackCaloHypo::operator()() const {
  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_packedHypos.exist() ) return;
  const auto* dst = m_packedHypos.getOrCreate();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of PackedCaloHypos = " << dst->hypos().size() << endmsg;
  auto* newCaloHypos = m_hypos.put( std::make_unique<LHCb::CaloHypos>() );
  LHCb::CaloHypoPacker{this}.unpack( *dst, *newCaloHypos );
}

DECLARE_COMPONENT( UnpackCaloHypo )
