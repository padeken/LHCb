/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ----------------------------------------------------------------------------
// Implementation file for class: ConversionDODMapper
//
// 17/01/2012: Marco Clemencic
// ----------------------------------------------------------------------------

#include "MapperToolBase.h"
#include <GaudiKernel/ParsersFactory.h>
#include <iterator>
#include <regex>

namespace Gaudi {
  namespace Parsers {
    static StatusCode parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input ) {
      return parse_( result, input );
    }
  } // namespace Parsers
} // namespace Gaudi

/** @class ConversionDODMapper ConversionDODMapper.h
 *
 * Tool for automatic conversions in the transient store.
 *
 * Implements the IDODAlgMapper interface to dynamically instruct the DataOnDemandSvc
 * to call conversion algorithms that will convert some input object in the T.S.
 * the the requested one.
 *
 * The Tool must be configured with a list of path transformation rules
 * (property "Transformations") in the form of pairs of strings, where the first
 * one is a regular expression matching a possible path to produce and the
 * second one is the format of the corresponding source path.
 * E.g.: to generate the entries in ".../Phys/..." from the corresponding path
 * in ".../pPhys/...", tha pair to use is ("(.*)/Phys/(.*)", "$1/pPhys/$2").
 *
 * In addition to the path transformation rules, a mapping from source ClassID
 * to the name of the converter algorithm must be set via the property
 * "Algorithms".
 *
 * It is possible to change the default OutputLevel for all the instances of an
 * algorithm type triggered by the tool using the property
 * "AlgorithmsOutputLevels", which is a map from algorithm class name to
 * MSG::Level.
 *
 * @author Marco Clemencic
 * @date 17/01/2012
 */
class ConversionDODMapper : public MapperToolBase {

public:
  /// Standard constructor
  ConversionDODMapper( const std::string& type, const std::string& name, const IInterface* parent );

  /// Initialize the tool instance.
  StatusCode initialize() override;

public:
  /// Return the algorithm type/name to produce the requested entry.
  ///
  /// For the given path in the transient store, try to transform it to a source
  /// location and, if it is possible, load the source object to find the type
  /// of the conversion algorithm.
  /// A unique name for the algorithm instance is chosen and the JobOptionsSvc
  /// is fed with the InputName and OutputName properties for that instance.
  ///
  /// Then the TypeNameString of the algorithm instance is returned.
  ///
  /// @see IDODAlgMapper
  Gaudi::Utils::TypeNameString algorithmForPath( const std::string& path ) override;

public:
  /// Instruct the DataOnDemandSvc to create the DataObjects for the
  /// intermediate levels of a path we can handle.
  ///
  /// If the requested path can be transformed via the known rules and the
  /// source object is a trivial DataObject, we tell the DataOnDemandSvc to
  /// create the node.
  ///
  /// @see IDODNodeMapper
  std::string nodeTypeForPath( const std::string& path ) override;

private:
  /// Convert a string using the configured mapping rules.
  /// All the rules are tried until one matches. If there is no match an empty
  /// string is returned.
  std::string transform( const std::string& input ) const;

  /// Helper function to get the source candidate.
  DataObject* candidate( const std::string& path ) const;

private:
  /// @{
  /// Data members corresponding to properties
  typedef std::vector<std::pair<std::string, std::string>> RulesMapProp;
  RulesMapProp                                             m_pathTransfRules; ///!< Transformations

  typedef std::map<CLID, std::string> AlgForTypeMap;
  AlgForTypeMap                       m_algTypes; ///!< Algorithms

  typedef std::map<std::string, unsigned int> OutLevelsMap;
  OutLevelsMap                                m_algOutLevels; ///!< AlgorithmsOutputLevels

  std::string m_inputOptionName;  ///< Job option name for inputs
  std::string m_outputOptionName; ///< Job option name for outputs
  /// @}

private:
  /// Helper class to manage the regex translation rules.
  class Rule {
  public:
    /// Constructor.
    inline Rule( const std::string& _regexp, const std::string& _format ) : regexp( _regexp ), format( _format ) {}

    /// Apply the conversion rule to the input string.
    /// If the regex does not match the input, an empty string is returned.
    inline std::string apply( const std::string& input ) const {
      return std::regex_replace( input, regexp, format,
                                 std::regex_constants::match_default | std::regex_constants::format_no_copy );
    }

    /// Helper to create a Rule from a pair of strings.
    inline static Rule make( const std::pair<std::string, std::string>& p ) { return Rule( p.first, p.second ); }

  private:
    /// Regular expression object.
    std::regex regexp;

    /// Format string (see std documentation).
    std::string format;
  };

  ///@{
  /// List of translation rules
  typedef std::list<Rule> RulesList;
  RulesList               m_rules;
  ///@}
};

DECLARE_COMPONENT( ConversionDODMapper )

// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
ConversionDODMapper::ConversionDODMapper( const std::string& type, const std::string& name, const IInterface* parent )
    : MapperToolBase( type, name, parent ) {
  declareProperty( "Transformations", m_pathTransfRules,
                   "Dictionary string->string to define the transformation rules. "
                   "The key of each rule is a regular expression to match in the requested "
                   "path and the value is the format of the source path." );
  declareProperty( "Algorithms", m_algTypes,
                   "Dictionary ClassID -> AlgorithmType to be used to convert "
                   "sources in destinations" );
  declareProperty( "AlgorithmsOutputLevels", m_algOutLevels,
                   "Dictionary string->MsgLevel to change the message level "
                   "of a specific converter algorithm type" );
  declareProperty( "InputOptionName", m_inputOptionName = "InputName" );
  declareProperty( "OutputOptionName", m_outputOptionName = "OutputName" );
}

// ============================================================================
// Initialize
// ============================================================================
StatusCode ConversionDODMapper::initialize() {
  const StatusCode sc = MapperToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // convert the map in a list of rules
  std::transform( m_pathTransfRules.begin(), m_pathTransfRules.end(), std::back_inserter( m_rules ),
                  ConversionDODMapper::Rule::make );

  // FIXME: we should check if the properties Algorithms and AlgorithmsOutputLevels are consistent
  // In Python it would be:
  // unknown = set(m_algOutLevels.keys()) - set(m_algTypes.values())
  // if unknown:
  //     print "WARNING OutputLevel setting required for unknown algorithms:", list(unknown)
  return sc;
}

// ============================================================================

DataObject* ConversionDODMapper::candidate( const std::string& path ) const {
  LOG_VERBOSE << " -> ConversionDODMapper::candidate '" << path << "'" << endmsg;
  DataObject* obj( nullptr );
  if ( !path.empty() ) {
    obj = getIfExists<DataObject>( evtSvc(), path );
    if ( nullptr != obj ) {
      // ... get the source object...
      LOG_VERBOSE << "  -> Found object of type " << System::typeinfoName( typeid( *obj ) ) << ", classID "
                  << obj->clID() << endmsg;
    } else {
      LOG_VERBOSE << "  -> No source object found" << endmsg;
    }
  }
  return obj;
}

// ============================================================================

Gaudi::Utils::TypeNameString ConversionDODMapper::algorithmForPath( const std::string& path ) {
  LOG_VERBOSE << "ConversionDODMapper::algorithmForPath '" << path << "'" << endmsg;

  // source path in the transient store
  const auto src = transform( path );

  // If we have a source path and it points to an actual object get the source object...
  const auto obj = candidate( src );
  if ( obj ) {
    LOG_VERBOSE << " -> Found source data at '" << src << "'" << endmsg;

    // ... and choose the correct converter based on the ClassID.
    auto item = m_algTypes.find( obj->clID() );
    if ( item != m_algTypes.end() ) {
      // Get the algorithm type
      const auto& algType = item->second;

      // Choose a unique name for the algorithm instance
      auto algName = src + "_Converter";
      std::replace( algName.begin(), algName.end(), '/', '_' );

      // Add the configuration of algorithm instance to the JobOptionsSvc
      setProp( algName, m_inputOptionName, src );
      setProp( algName, m_outputOptionName, path );
      // ... including the output level
      auto level = m_algOutLevels.find( algType );
      if ( level != m_algOutLevels.end() ) { setProp( algName, "OutputLevel", level->second ); }

      // Return the algorithm type/name.
      LOG_VERBOSE << " -> Use algorithm type '" << algType << "'"
                  << " name '" << algName << "'" << endmsg;
      return Gaudi::Utils::TypeNameString( algName, algType );

    } else {

      std::ostringstream msg;
      msg << " -> Unknown packer algorithm for ClassID " << obj->clID() << " ("
          << System::typeinfoName( typeid( *obj ) ) << "), IGNORED";
      Warning( msg.str(), StatusCode::SUCCESS ).ignore();
    }
  } else {
    LOG_VERBOSE << " -> Source data missing at '" << src << "'" << endmsg;
  }

  return "";
}

// ============================================================================

std::string ConversionDODMapper::nodeTypeForPath( const std::string& path ) {
  LOG_VERBOSE << "ConversionDODMapper::nodeTypeForPath '" << path << "'" << endmsg;

  std::string retS = "";

  // If we have a source path and it points to an actual object get the source object...
  const std::string src = transform( path );
  const DataObject* obj = candidate( src );
  if ( obj ) {
    // handle only plain DataObject instances here
    if ( obj->clID() == DataObject::classID() ) { retS = "DataObject"; }
  }
  LOG_VERBOSE << " -> NodeType = '" << retS << "'" << endmsg;

  return retS;
}

// ============================================================================

std::string ConversionDODMapper::transform( const std::string& input ) const {
  LOG_VERBOSE << " -> ConversionDODMapper::transform '" << input << "'" << endmsg;
  std::string result;

  // try each mapping rule on the input, stopping on the first non-empty result.
  for ( RulesList::const_iterator r = m_rules.begin(); r != m_rules.end() && result.empty(); ++r ) {
    result = r->apply( input );
  }

  ON_VERBOSE {
    if ( result.empty() ) {
      verbose() << "  -> no source candidate" << endmsg;
    } else {
      verbose() << "  -> source candidate '" << result << "'" << endmsg;
    }
  }

  // either is empty because of no match or not because of early exit.
  return result;
}

// ============================================================================
