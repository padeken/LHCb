/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedProtoParticle.h"
#include "MapperToolBase.h"
#include <iterator>
#include <map>
#include <string>

// ----------------------------------------------------------------------------
// Implementation file for class: ParticlesAndVerticesMapper
//
// 17/01/2012: Marco Clemencic
// ----------------------------------------------------------------------------

/** @class ChargedProtoParticleMapper ChargedProtoParticleMapper.h
 *
 *  Tool for mappings for charged ProtoParticles
 *
 * @author Chris Jones
 * @date 16/04/2013
 */
class ChargedProtoParticleMapper : public MapperToolBase {

public:
  /// Standard constructor
  ChargedProtoParticleMapper( const std::string& type, const std::string& name, const IInterface* parent );

public:
  /** Returns the correctly configured and name instance of the
   *  Clusters unpacker, for the given path
   */
  Gaudi::Utils::TypeNameString algorithmForPath( const std::string& path ) override;

public:
  /** Instruct the DataOnDemandSvc to create DataObjects for the
   *  intermediate levels of a path we can handle.
   */
  std::string nodeTypeForPath( const std::string& path ) override;

private:
  /// Add a path to the node type mappings
  void addPath( const std::string& path );

  /// Check if a given path is in the list of data locations created
  bool pathIsHandled( const std::string& path ) const {
    // See if we have an entry for this path
    NodeTypeMap::const_iterator it = m_nodeTypeMap.find( fixPath( path ) );
    return ( it != m_nodeTypeMap.end() );
  }

  /// Check the node mappings are updated for the given path stream
  void updateNodeTypeMap( const std::string& path );

  /// Get the location of the packed ProtoParticles for the given stream
  std::string packedProtoLocation( const std::string& stream ) const {
    return stream + "/" + LHCb::PackedProtoParticleLocation::Charged;
  }

  /// Get the location of the packed ProtoParticles for the given stream
  std::string protoLocation( const std::string& stream ) const {
    return stream + "/" + LHCb::ProtoParticleLocation::Charged;
  }

private:
  /// Map to say which stream roots have been configured
  std::map<std::string, bool> m_streamsDone;

  /// Mapping between TES path and node type
  typedef std::map<std::string, std::string> NodeTypeMap;
  NodeTypeMap                                m_nodeTypeMap;

  /// List of track types for PID recalibration
  std::vector<std::string> m_tkTypes;

  /// List of hypo types for PID recalibration
  std::vector<std::string> m_pidTypes;

  /// ANN PID recalibration tuning
  std::string m_pidTune;

  /// String match regex for the Brunel application version for rerunning the ANNPID
  std::string m_regex;

  /// Outputlevel for unpackers created
  int m_unpackersOutputLevel;
};

// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
ChargedProtoParticleMapper::ChargedProtoParticleMapper( const std::string& type, const std::string& name,
                                                        const IInterface* parent )
    : MapperToolBase( type, name, parent ) {
  declareProperty( "UnpackerOutputLevel", m_unpackersOutputLevel = -1 );
  declareProperty( "TrackTypes", m_tkTypes = {"Long", "Downstream", "Upstream"} );
  declareProperty( "PIDTypes", m_pidTypes = {"Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"} );
  declareProperty( "ANNPIDTune", m_pidTune = "" );
  declareProperty( "VersionRegex", m_regex = "" );
}

// ============================================================================

Gaudi::Utils::TypeNameString ChargedProtoParticleMapper::algorithmForPath( const std::string& path ) {
  LOG_VERBOSE << "ChargedProtoParticleMapper::algorithmForPath '" << path << "'" << endmsg;

  updateNodeTypeMap( path );

  if ( pathIsHandled( path ) ) {
    // Choose a unique name
    const auto baseName = streamName( path ) + "_ChargedPP";

    // Use a sequencer as the main type
    const std::string unpackerType = "GaudiSequencer";
    const auto        seqName      = baseName + "_Seq";

    // List of algorithms to run in the sequencer
    std::vector<std::string> algs;

    // Add the basic unpacker
    const auto unpackName = baseName + "_Unpack";
    algs.emplace_back( "UnpackProtoParticle/" + unpackName );
    setProp( unpackName, "InputName", packedProtoLocation( streamRoot( path ) ) );
    setProp( unpackName, "OutputName", protoLocation( streamRoot( path ) ) );

    // PID recalibration
    if ( !m_pidTune.empty() ) {
      if ( !m_regex.empty() ) {
        const auto name = "Reco14Filter_" + baseName;
        setProp( name, "HeaderLocation", "Rec/Header" );
        setProp( name, "VersionRegex", m_regex );
        algs.emplace_back( "ApplicationVersionFilter/" + name );
      }
      for ( const auto& tkType : m_tkTypes ) {
        for ( const auto& pidType : m_pidTypes ) {
          const auto name = "ANNPID" + tkType + pidType + "_" + baseName;
          setProp( name, "TrackType", tkType );
          setProp( name, "PIDType", pidType );
          setProp( name, "NetworkVersion", m_pidTune );
          setProp( name, "ProtoParticleLocation", protoLocation( streamRoot( path ) ) );
          algs.emplace_back( "ANNGlobalPID::ChargedProtoANNPIDAlg/" + name );
        }
      }
    }

    // Set the sequencer alg list
    setProp( seqName, "Members", algs );

    // Set output levels, if required
    if ( m_unpackersOutputLevel > 0 ) {
      setProp( seqName, "OutputLevel", m_unpackersOutputLevel );
      for ( const auto& a : algs ) {
        const auto slash = a.find_last_of( "/" );
        const auto N     = ( slash != std::string::npos ? a.substr( slash + 1 ) : a );
        setProp( N, "OutputLevel", m_unpackersOutputLevel );
      }
    }

    // Return the algorithm type/name.
    LOG_VERBOSE << " -> Use algorithm type '" << unpackerType << "'"
                << " name '" << seqName << "'" << endmsg;
    return Gaudi::Utils::TypeNameString( seqName, unpackerType );
  }

  return "";
}

// ============================================================================

std::string ChargedProtoParticleMapper::nodeTypeForPath( const std::string& path ) {
  updateNodeTypeMap( path );

  auto it = m_nodeTypeMap.find( fixPath( path ) );

  const auto retS = ( it != m_nodeTypeMap.end() ? it->second : "" );

  LOG_VERBOSE << "ChargedProtoParticleMapper::nodeTypeForPath '" << path << "' NodeType '" << retS << "'" << endmsg;

  return retS;
}

// ============================================================================

void ChargedProtoParticleMapper::updateNodeTypeMap( const std::string& path ) {
  // The stream TES root
  const auto streamR = streamRoot( path );

  LOG_VERBOSE << "ChargedProtoParticleMapper::updateNodeTypeMap Running for " << streamR << endmsg;

  // See if the packed clusters object exists for this stream
  if ( !m_streamsDone[streamR] ) {
    m_streamsDone[streamR] = true;
    const auto packedLoc   = packedProtoLocation( streamR );
    LOG_VERBOSE << "ChargedProtoParticleMapper::updateNodeTypeMap Looking for " << packedLoc << endmsg;
    if ( exist<LHCb::PackedProtoParticles*>( packedLoc ) ) {
      // Update the node type paths handled
      addPath( protoLocation( streamR ) );
    } else {
      LOG_VERBOSE << " -> Missing" << endmsg;
    }
  }
}

// ============================================================================

void ChargedProtoParticleMapper::addPath( const std::string& path ) {
  // Make sure paths start with /Event/
  const auto npath = fixPath( path );

  // if not already there, add.
  if ( m_nodeTypeMap.find( npath ) == m_nodeTypeMap.end() ) {
    LOG_VERBOSE << " -> Path " << npath << endmsg;

    // Main path
    m_nodeTypeMap[npath] = "";

    // Data Node paths ...
    auto tmp   = npath;
    auto slash = tmp.find_last_of( "/" );
    while ( !tmp.empty() && slash != std::string::npos ) {
      tmp = tmp.substr( 0, slash );
      if ( !tmp.empty() ) {
        LOG_VERBOSE << "  -> Node " << tmp << endmsg;
        m_nodeTypeMap[tmp] = "DataObject";
      }
      slash = tmp.find_last_of( "/" );
    }
  }
}

// ============================================================================

DECLARE_COMPONENT( ChargedProtoParticleMapper )

// ============================================================================
