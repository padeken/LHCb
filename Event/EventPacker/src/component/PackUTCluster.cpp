/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedCluster.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class PackUTCluster PackUTCluster.h
 *
 *  Pack the LHCb::UTCluster objects from a specified container.
 */
class PackUTCluster : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;

private:
  Gaudi::Property<std::string> m_inputName{this, "InputName", LHCb::UTClusterLocation::UTClusters};

  Gaudi::Property<std::string> m_outputName{this, "OutputName", LHCb::PackedClusterLocation::UT};

  /// Flag to turn on the removal of the input data after packing
  Gaudi::Property<bool> m_deleteInput{this, "DeleteInput", false};

  /// Flag to turn on the creation of output, even when input is missing
  Gaudi::Property<bool> m_alwaysOutput{this, "AlwaysCreateOutput", false};

  Gaudi::Accumulators::StatCounter<> m_nbPackedUTClusters{this, "# PackedUTClusters"};
};

DECLARE_COMPONENT( PackUTCluster )

StatusCode PackUTCluster::execute() {
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "==> Execute" << endmsg; }

  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_alwaysOutput && !exist<LHCb::UTClusters>( m_inputName ) ) { return StatusCode::SUCCESS; }

  // Check to see if packed output already exists. If it does print a warning and return
  auto out = getIfExists<LHCb::PackedClusters>( m_outputName );
  if ( out ) {
    return Warning( "Packed Clusters already exist at '" + m_outputName + "' -> Abort", StatusCode::SUCCESS );
  }

  // Create and save the output container
  out = new LHCb::PackedClusters();
  out->setPackingVersion( LHCb::PackedClusters::defaultPackingVersion() );
  put( out, m_outputName );

  // Load the input. If not existing just return
  const auto clusters = getIfExists<LHCb::UTClusters>( m_inputName );
  if ( !clusters ) { return StatusCode::SUCCESS; }

  // Pack the clusters
  for ( const auto& cluster : *clusters ) {
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Packing " << cluster << endmsg; }

    if ( cluster->isUT() ) {
      out->addUTCluster( cluster );
    } else {
      Warning( "Unsupported cluster type, not packing", StatusCode::SUCCESS ).ignore();
    }
  }

  // If requested, remove the input data from the TES and delete
  if ( UNLIKELY( m_deleteInput ) ) {
    const StatusCode sc = evtSvc()->unregisterObject( clusters ).andThen( [&] { delete clusters; } );
  } else {
    // Clear the registry address of the unpacked container, to prevent reloading
    auto* pReg = clusters->registry();
    if ( pReg ) pReg->setAddress( nullptr );
  }

  // Summary of the size of the packed container
  m_nbPackedUTClusters += out->clusters().size();

  return StatusCode::SUCCESS;
}
