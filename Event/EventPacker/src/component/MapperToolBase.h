/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Gaudi/Interfaces/IOptionsSvc.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/IDODAlgMapper.h>
#include <GaudiKernel/IDODNodeMapper.h>
#include <GaudiKernel/ToStream.h>
#include <string>
#include <string_view>

// Helpers for printing
#define ON_VERBOSE if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) )
#define ON_DEBUG if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
#define LOG_VERBOSE ON_VERBOSE verbose()
#define LOG_DEBUG ON_DEBUG debug()

/** @class MapperToolBase MapperToolBase.h
 *
 *  Base class for mapper tools
 *
 *  @author Chris Jones
 *  @date   2012-03-26
 */
class MapperToolBase : public extends<GaudiTool, IDODAlgMapper, IDODNodeMapper> {

public:
  /// Standard constructor
  using base_class::base_class;

protected:
  /// Get the Stream name from a data path
  std::string streamName( const std::string& path ) const;

  /// Get the stream root from a data path
  inline std::string streamRoot( const std::string& path ) const { return "/Event/" + streamName( path ); }

  /// Make sure a path starts with /Event/
  inline std::string fixPath( const std::string& path ) const {
    return path.compare( 0, 7, "/Event/" ) != 0 ? "/Event/" + path : path;
  }

  /// Helper to set a property via the options service.
  template <typename T>
  void setProp( std::string component, std::string_view property, const T& value ) const {
    component += '.'; // used as a buffer to host the full name of the property
    component += property;
    serviceLocator()->getOptsSvc().set( component, Gaudi::Utils::toString( value ) );
  }
};
