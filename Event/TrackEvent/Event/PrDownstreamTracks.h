/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/SOACollection.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "LHCbMath/bit_cast.h"
#include "PrSeedTracks.h"
#include "PrTracksTag.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Downstream track made with a FT seed and UT hits
 */
namespace V2        = LHCb::v2::Event;
namespace TracksTag = LHCb::Pr::TracksTag;

namespace LHCb::Pr::Downstream {

  namespace Tag {
    struct trackFT : V2::int_field {};
    struct StateQoP : V2::float_field {};
    struct nUTHits : V2::int_field {};
    struct nFTHits : V2::int_field {};
    struct ut_indices : V2::ints_field<TracksTag::MaxUTHits> {};
    struct ft_indices : V2::ints_field<TracksTag::MaxFTHits> {};
    struct lhcbIDs : V2::ints_field<TracksTag::MaxFTHits + TracksTag::MaxUTHits> {};
    struct StatePosition : V2::state_field {};

    template <typename T>
    using downstream_t =
        V2::SOACollection<T, trackFT, StateQoP, nFTHits, nUTHits, ft_indices, ut_indices, lhcbIDs, StatePosition>;
  } // namespace Tag

  struct Tracks : Tag::downstream_t<Tracks> {
    using base_t = typename Tag::downstream_t<Tracks>;

    using base_t::allocator_type;
    Tracks( LHCb::Pr::Seeding::Tracks const* ft_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_ft_ancestors{ft_ancestors} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}, m_ft_ancestors{old.m_ft_ancestors} {}

    // Return pointer to ancestor container
    [[nodiscard]] LHCb::Pr::Seeding::Tracks const* getFTAncestors() const { return m_ft_ancestors; };

  private:
    LHCb::Pr::Seeding::Tracks const* m_ft_ancestors{nullptr};
  };
} // namespace LHCb::Pr::Downstream

REGISTER_HEADER( LHCb::Pr::Downstream::Tracks, "Event/PrDownstreamTracks.h" );
