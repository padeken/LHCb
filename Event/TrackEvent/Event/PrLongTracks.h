/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/PrProxyHelpers.h"
#include "Event/PrSeedTracks.h"
#include "Event/PrTracksTag.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/VPChannelID.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "PrTracksInfo.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Track data for exchanges between FT and Fit
 *
 * @author: Arthur Hennequin
 * 2020-05-04
 * add hit indices by Peilian Li
 */

namespace V2        = LHCb::v2::Event;
namespace TracksTag = LHCb::Pr::TracksTag;

namespace LHCb::Pr::Long {

  namespace detail {
    /** Helper type for fitted track proxies */
    template <typename TrackProxy>
    struct ScifiState {
      TrackProxy const& m_proxy;
      ScifiState( TrackProxy const& proxy ) : m_proxy{proxy} {}
      decltype( auto ) qOverP() const { return m_proxy.qOverP(); }
      decltype( auto ) slopes() const { return m_proxy.endScifiStateDir(); }
      decltype( auto ) position() const { return m_proxy.endScifiStatePos(); }
      // TODO maybe stick these in a base class via CRTP if it gets a bit cumbersome?
      decltype( auto ) x() const { return position().X(); }
      decltype( auto ) y() const { return position().Y(); }
      decltype( auto ) z() const { return position().Z(); }
      decltype( auto ) tx() const { return slopes().X(); }
      decltype( auto ) ty() const { return slopes().Y(); }
    };
  } // namespace detail

  namespace Tag {
    using trackVP   = TracksTag::trackVP;
    using trackUT   = TracksTag::trackUT;
    using trackSeed = TracksTag::trackSeed;
    using StateQoP  = TracksTag::StateQoP;
    using nVPHits   = TracksTag::nVPHits;
    using nUTHits   = TracksTag::nUTHits;
    using nFTHits   = TracksTag::nFTHits;
    struct vp_indices : V2::ints_field<TracksTag::MaxVPHits> {};
    struct ut_indices : V2::ints_field<TracksTag::MaxUTHits> {};
    struct ft_indices : V2::ints_field<TracksTag::MaxFTHits> {};
    struct lhcbIDs : V2::ints_field<TracksTag::MaxFTHits + TracksTag::MaxVPHits + TracksTag::MaxUTHits> {};
    struct StatePositions : V2::floats_field<TracksTag::NumLongStates, TracksTag::NumPosPars> {};

    template <typename T>
    using long_t = V2::SOACollection<T, trackVP, trackUT, trackSeed, StateQoP, nVPHits, nUTHits, nFTHits, vp_indices,
                                     ut_indices, ft_indices, lhcbIDs, StatePositions>;
  } // namespace Tag

  struct Tracks : Tag::long_t<Tracks> {
    using base_t = typename Tag::long_t<Tracks>;

    constexpr static auto NumPosPars       = TracksTag::NumPosPars;
    constexpr static auto NumLongStates    = TracksTag::NumLongStates; // number of state 0: vstate, 1: Tstate
    constexpr static auto NumLongStatePars = NumPosPars * NumLongStates;
    constexpr static auto MaxVPHits        = TracksTag::MaxVPHits;
    constexpr static auto MaxUTHits        = TracksTag::MaxUTHits;
    constexpr static auto MaxFTHits        = TracksTag::MaxFTHits;
    constexpr static auto MaxLHCbIDs       = TracksTag::MaxFTHits + TracksTag::MaxVPHits + TracksTag::MaxUTHits;

    using base_t::allocator_type;

    Tracks( LHCb::Pr::Velo::Tracks const* velo_ancestors, LHCb::Pr::Upstream::Tracks const* upstream_ancestors,
            LHCb::Pr::Seeding::Tracks const* seed_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}
        , m_velo_ancestors{velo_ancestors}
        , m_upstream_ancestors{upstream_ancestors}
        , m_seed_ancestors{seed_ancestors} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}
        , m_velo_ancestors{old.m_velo_ancestors}
        , m_upstream_ancestors{old.m_upstream_ancestors}
        , m_seed_ancestors{old.m_seed_ancestors} {}

    // Return pointer to ancestor container
    [[nodiscard]] LHCb::Pr::Velo::Tracks const*     getVeloAncestors() const { return m_velo_ancestors; };
    [[nodiscard]] LHCb::Pr::Upstream::Tracks const* getUpstreamAncestors() const { return m_upstream_ancestors; };
    [[nodiscard]] LHCb::Pr::Seeding::Tracks const*  getSeedAncestors() const { return m_seed_ancestors; };

    template <typename F, typename M = std::true_type>
    void store_StatePosDir( int at, int state, Vec3<F> const& pos, Vec3<F> const& dir, M&& mask = {} ) {
      // state 0: velo, state 1: T
      store<Tag::StatePositions>( at, state, 0, pos.x, mask );
      store<Tag::StatePositions>( at, state, 1, pos.y, mask );
      store<Tag::StatePositions>( at, state, 2, pos.z, mask );
      store<Tag::StatePositions>( at, state, 3, dir.x, mask );
      store<Tag::StatePositions>( at, state, 4, dir.y, mask );
    }

    template <typename I, typename M = std::true_type>
    void store_vp_index( int at, int i, I const& vpidx, M&& mask = {} ) {
      store<Tag::vp_indices>( at, i, vpidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_ut_index( int at, int i, I const& utidx, M&& mask = {} ) {
      store<Tag::ut_indices>( at, i, utidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_ft_index( int at, int i, I const& ftidx, M&& mask = {} ) {
      store<Tag::ft_indices>( at, i, ftidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_lhcbID( int at, int i, I const& lhcbid, M&& mask = {} ) {
      store<Tag::lhcbIDs>( at, i, lhcbid, mask );
    }

  private:
    LHCb::Pr::Velo::Tracks const*     m_velo_ancestors{nullptr};
    LHCb::Pr::Upstream::Tracks const* m_upstream_ancestors{nullptr};
    LHCb::Pr::Seeding::Tracks const*  m_seed_ancestors{nullptr};

    DECLARE_PROXY_FRIEND( TrackProxy );
  };

  DECLARE_PROXY( TrackProxy ) {
    PROXY_METHODS( TrackProxy, dType, Tracks, m_Tracks );
    using IType = typename dType::int_v;
    using FType = typename dType::float_v;

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_Tracks->template data<Tag>( Is... ) );
    }

  public:
    [[nodiscard]] auto trackVP() const { return loader<Tag::trackVP>(); }
    [[nodiscard]] auto trackUT() const { return loader<Tag::trackUT>(); }
    [[nodiscard]] auto trackSeed() const { return loader<Tag::trackSeed>(); }
    [[nodiscard]] auto qOverP() const { return loader<Tag::StateQoP>(); }
    [[nodiscard]] auto p() const { return abs( 1.0 / qOverP() ); }
    [[nodiscard]] auto nVPHits() const { return loader<Tag::nVPHits>(); }
    [[nodiscard]] auto nUTHits() const { return loader<Tag::nUTHits>(); }
    [[nodiscard]] auto nFTHits() const { return loader<Tag::nFTHits>(); }
    [[nodiscard]] auto nHits() const { return nVPHits() + nUTHits() + nFTHits(); }
    [[nodiscard]] auto nLHCbIDs() const { return nVPHits() + nUTHits() + nFTHits(); }
    [[nodiscard]] auto vp_index( std::size_t i ) const { return loader<Tag::vp_indices>( i ); }
    [[nodiscard]] auto ut_index( std::size_t i ) const { return loader<Tag::ut_indices>( i ); }
    [[nodiscard]] auto ft_index( std::size_t i ) const { return loader<Tag::ft_indices>( i ); }
    [[nodiscard]] auto lhcbID( std::size_t i ) const { return loader<Tag::lhcbIDs>( i ); }
    [[nodiscard]] auto StatePosElement( std::size_t i, std::size_t j ) const {
      return loader<Tag::StatePositions>( i, j );
    }

    // Retrieve state Info
    [[nodiscard]] auto StatePosDir( std::size_t i ) const {
      using LHCb::Utils::unwind;
      LHCb::LinAlg::Vec<decltype( this->StatePosElement( 0, 0 ) ), TracksTag::NumPosPars> out;
      unwind<0, TracksTag::NumPosPars>( [&]( auto j ) { out( j ) = this->StatePosElement( i, j ); } );
      return out;
    }
    [[nodiscard]] auto StatePos( std::size_t i ) const {
      return Vec3<FType>( StatePosDir( i )( 0 ), StatePosDir( i )( 1 ), StatePosDir( i )( 2 ) );
    }
    [[nodiscard]] auto StateDir( std::size_t i ) const {
      return Vec3<FType>( StatePosDir( i )( 3 ), StatePosDir( i )( 4 ), 1.f );
    }
    [[nodiscard]] auto closestToBeamStatePos() const {
      // FIXME this is broken
      static_assert( behaviour() == LHCb::Pr::ProxyBehaviour::ScatterGather );
      auto const* velo_tracks = this->m_tracks->getVeloAncestors();
      auto const  indices     = trackVP();
      return velo_tracks->gather( indices )->closestToBeamStatePos();
    }
    [[nodiscard]] auto closestToBeamStateDir() const {
      // FIXME this is broken
      static_assert( behaviour() == LHCb::Pr::ProxyBehaviour::ScatterGather );
      auto const* velo_tracks = this->m_tracks->getVeloAncestors();
      auto const  indices     = trackVP();
      return velo_tracks->gather( indices )->closestToBeamStateDir();
    }
    auto closestToBeamState() const {
      return LHCb::Pr::detail::VeloState{closestToBeamStatePos(), closestToBeamStateDir()};
    }

    auto phi() const { return closestToBeamState().slopes().phi(); }
    auto pseudoRapidity() const { return closestToBeamState().slopes().eta(); }

    auto endScifiState() const { return detail::ScifiState{*this}; }

    auto endScifiStatePos() const {
      return Gaudi::XYZVectorF{this->StatePosElement( 1, 0 ).cast(), this->StatePosElement( 1, 1 ).cast(),
                               this->StatePosElement( 1, 2 ).cast()};
    }
    auto endScifiStateDir() const {
      return Gaudi::XYZPointF{this->StatePosElement( 1, 3 ).cast(), this->StatePosElement( 1, 4 ).cast(), 1.0};
    }
    auto pt() const {
      auto const mom = p();
      auto const dir = closestToBeamStateDir();
      auto const pt2 = mom * mom * ( dir.X() * dir.X() + dir.Y() * dir.Y() ) / dir.mag2();
      using std::sqrt;
      return sqrt( pt2 );
    }

    // Retrieve the set of LHCbIDs
    [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
      std::vector<LHCbID> ids;
      ids.reserve( Tracks::MaxLHCbIDs );
      static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
      for ( auto i = 0; i < nHits().cast(); i++ ) { ids.emplace_back( LHCb::LHCbID( lhcbID( i ).cast() ) ); }
      return ids;
    }
  };
} // namespace LHCb::Pr::Long

REGISTER_PROXY( LHCb::Pr::Long::Tracks, LHCb::Pr::Long::TrackProxy );
REGISTER_HEADER( LHCb::Pr::Long::Tracks, "Event/PrLongTracks.h" );
