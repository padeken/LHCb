/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/SIMDWrapper.h"

/** @class PrTracksInfo PrTracksInfo.h
 *
 ** Define maximum numbers of hits for Pr::Tracks which are detector specific
 *  @author Peilian Li
 *  @date   2020-06-22
 */

namespace TracksInfo {

  // The total layers of each sub-detector
  enum class DetLayers { VPLayers = 26, FTLayers = 12, UTLayers = 4 };

  // The maximum number of hits for PrLongTracks, depends on the pattern recognition
  enum class MaxHits { VPHits = 26, FTHits = 15, UTHits = 8 };

} // namespace TracksInfo
