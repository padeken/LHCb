/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestTrackV2
#include <boost/test/unit_test.hpp>
#include <type_traits>

#include "Event/CaloDigits_v2.h"

using namespace LHCb::Event::Calo;

static_assert( std::is_nothrow_move_constructible_v<Digit> );
static_assert( std::is_nothrow_copy_constructible_v<Digit> );
static_assert( std::is_nothrow_move_assignable_v<Digit> );
static_assert( std::is_nothrow_copy_assignable_v<Digit> );

static_assert( std::is_trivially_copyable_v<Digit> );
static_assert( std::is_trivially_destructible_v<Digit> );

static_assert( std::is_move_assignable_v<Digits> );
// static_assert( std::is_nothrow_move_assignable_v<Digits> );
static_assert( std::is_nothrow_move_constructible_v<Digits> );
// note: container should _not_ by copyable, as that would allow wastefull code...
static_assert( !std::is_copy_constructible_v<Digits> );
static_assert( !std::is_copy_assignable_v<Digits> );
// static_assert( !std::is_assignable_v<Digits,Digits> );

BOOST_AUTO_TEST_CASE( test_calodigits ) {

  Digits digits;

  auto id1 = LHCb::CaloCellID{CaloCellCode::CaloIndex::EcalCalo, 0, 6, 0};
  auto r1  = digits.try_emplace( {CaloCellCode::CaloIndex::EcalCalo, 0, 6, 0}, 0.5, 23 );
  BOOST_CHECK( r1 );
  const auto& d1 = *r1;

  auto id2 = LHCb::CaloCellID{CaloCellCode::CaloIndex::EcalCalo, 0, 7, 1};
  auto r2  = digits.try_emplace( id2, 1.5, 42 );
  BOOST_CHECK( r2 );
  const auto& d2 = *r2;

  // check what happens if a duplicate cellID is encountered
  auto r3 = digits.try_emplace( d2.cellID(), 99.9, 100 );
  BOOST_CHECK( !r3 );

  auto        begin = digits.begin();
  const auto& dig1  = *begin++;
  const auto& dig2  = *begin++;

  BOOST_CHECK( d1 == dig1 );
  BOOST_CHECK( d2 == dig2 );

  BOOST_CHECK( begin == digits.end() );

  BOOST_CHECK( dig1.energy() == 0.5 );
  BOOST_CHECK( dig2.energy() == 1.5 );

  BOOST_CHECK( dig1.adc() == 23 );
  BOOST_CHECK( dig2.adc() == 42 );
  BOOST_CHECK( dig1.cellID() == id1 );
  BOOST_CHECK( dig2.cellID() == id2 );

  // verify lookup
  BOOST_CHECK( digits.find( id1 ).value() == dig1 );
  BOOST_CHECK( digits.find( id2 ).value() == dig2 );

  // auto other_digits = digits; -- should not compile!
  Digits other_digits = std::move( digits );
  // check that after 'move' the data is still in the same spot...
  // BOOST_CHECK( other_digits.begin() == r1.first );
  // BOOST_CHECK( &*other_digits.begin() == &*r1.first );
  // BOOST_CHECK( std::next( other_digits.begin() ) == r2.first );

  // verify lookup after move
  // BOOST_CHECK( other_digits( id1 ) == &dig1 );
  // BOOST_CHECK( other_digits( id2 ) == &dig2 );

  int i = 0;
  for ( const auto& digit : other_digits ) {
    const auto& expected_dig = ( i == 0 ? dig1 : dig2 );
    BOOST_CHECK( digit == expected_dig );
    ++i;
  }
}
