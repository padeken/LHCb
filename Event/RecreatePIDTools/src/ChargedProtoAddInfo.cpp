/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Interfaces/IProtoParticleTool.h"

namespace LHCb::Rec::ProtoParticle::Charged {

  class AddInfo final : public GaudiAlgorithm {
  public:
    using GaudiAlgorithm::GaudiAlgorithm;

    StatusCode execute() override {
      // update the proto particles from the TES 'in situ' (for backwards compatibility)
      LHCb::ProtoParticles* protos = m_protoPath.get();
      for ( auto& addInfo : m_addInfo ) ( *addInfo )( *protos ).ignore();
      return StatusCode::SUCCESS;
    }

  private:
    ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
    DataObjectReadHandle<LHCb::ProtoParticles>              m_protoPath{
        this, "ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged}; ///< Location in TES of ProtoParticles
  };
  DECLARE_COMPONENT_WITH_ID( AddInfo, "ChargedProtoParticleAddInfo" )
} // namespace LHCb::Rec::ProtoParticle::Charged
