/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** Define some numbers for the UT which are detector specific
 *
 *
 *  @author Michel De Cian
 *  @date   2019-05-31
 */

namespace UTInfo {

  enum class DetectorNumbers { Sectors = 98, Regions = 3, Layers = 2, Stations = 2, TotalLayers = 4 };
  enum class MasksBits { LayerMask = 0x180000, StationMask = 0x600000, LayerBits = 19, StationBits = 21 };
  enum class SectorNumbers {
    EffectiveSectorsPerColumn = 28,
    MaxSectorsPerRegion       = 98
  }; // this is used in the conversion from position to Sector numbers

} // namespace UTInfo
