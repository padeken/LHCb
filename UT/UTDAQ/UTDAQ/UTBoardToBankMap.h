/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTBoardToBankMap UTBoardToBankMap.h
 *
 *  Helper class for mapping boards to banks
 *  basically hides a map - used in 2 places....
 *
 *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
 *  @date   2020-05-11
 */

#pragma once
#include "Kernel/UTDAQID.h"
#include <map>

class UTBoardToBankMap final {

public:
  // add entry to map
  void addEntry( UTDAQID aBoard, unsigned int aBank ) { m_bankMapping[aBoard] = aBank; }

  // board to bank
  UTDAQID findBoard( const unsigned int aBank ) const {
    auto i = std::find_if( m_bankMapping.begin(), m_bankMapping.end(),
                           [&]( const std::pair<const UTDAQID, unsigned int>& p ) { return p.second == aBank; } );
    return i != m_bankMapping.end() ? i->first : UTDAQID( UTDAQID::nullBoard );
  }

  // bank to board
  unsigned int findBank( const UTDAQID aBoard ) const { return m_bankMapping.at( aBoard ); }

  void clear() { m_bankMapping.clear(); }

private:
  std::map<UTDAQID, unsigned int> m_bankMapping;
};
