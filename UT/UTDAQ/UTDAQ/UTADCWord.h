/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <bitset>
#include <iostream>
#include <string>

/** @class UTADCWord UTADCWord.h "UTDAQ/UTADCWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Xuhao Yuan (based on codes by M.Needham)
 *  @date   2020-06-17
 */

class UTADCWord final {

public:
  /** constructer with int
    @param value
    */
  explicit UTADCWord( unsigned int value ) : m_value( value ) {}

  UTADCWord( unsigned int value, unsigned int nlane ) : m_value( value ), m_lane( nlane ){};

  /** The actual value
    @return value
    */
  unsigned int value() const { return m_value; };

  unsigned int channelID() const {
    return ( ( m_value & static_cast<int>( mask::strip ) ) >> static_cast<int>( bits::strip ) ) +
           static_cast<int>( bits::maxstrip ) * m_lane;
  }

  unsigned int adc() const { return ( m_value & static_cast<int>( mask::adc ) ) >> static_cast<int>( bits::adc ); }

  unsigned int fracStripBits() const { return 0; }

  unsigned int pseudoSizeBits() const { return 0; }

  bool hasHighThreshold() const { return true; }

  /** Operator overloading for stringoutput */
  friend std::ostream& operator<<( std::ostream& s, const UTADCWord& obj ) { return obj.fillStream( s ); }

  /** Fill the ASCII output stream */
  std::ostream& fillStream( std::ostream& s ) const {
    return s << "{ "
             << " value:\t" << value() << " }\n";
  }

private:
  enum class bits { adc = 0, strip = 5, maxstrip = 512 };
  enum class mask { adc = 0x1F, strip = 0xFFE0 };

  unsigned int m_value = 0;
  unsigned int m_lane  = 0;
};
