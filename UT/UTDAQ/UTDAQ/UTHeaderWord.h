/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#pragma once
#include <bitset>
#include <iostream>
#include <sstream>
#include <string>

/** @class UTHeaderWord UTHeaderWord.h "UTDAQ/UTHeaderWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Xuhao Yuan ( based on codes by M.Needham)
 *  @date   2020-06-17
 */

class UTHeaderWord final {

public:
  /** constructer
    @param value
    */
  UTHeaderWord() : m_value( 0 ) {}
  UTHeaderWord( unsigned int value ) : m_value( value ) {}
  UTHeaderWord( std::array<unsigned int, 6> nHitsInLanes, unsigned int n_lane ) {

    switch ( n_lane ) {
    case 1:
      m_value = ( EventIDFromTFC << static_cast<unsigned int>( bits::EventIDFromTFC ) ) +
                ( paddedValue << static_cast<unsigned int>( bits::paddedBank ) ) +
                ( nHitsInLanes[5] << static_cast<unsigned int>( bits::lane5 ) ) +
                ( nHitsInLanes[4] << static_cast<unsigned int>( bits::lane4 ) );
      break;
    case 2:
      m_value = ( nHitsInLanes[3] << static_cast<unsigned int>( bits::lane3 ) ) +
                ( nHitsInLanes[2] << static_cast<unsigned int>( bits::lane2 ) ) +
                ( nHitsInLanes[1] << static_cast<unsigned int>( bits::lane1 ) ) +
                ( nHitsInLanes[0] << static_cast<unsigned int>( bits::lane0 ) );
      break;
    default:
      throw std::domain_error( "invalid UT lane" );
    }
  }

  /** The actual value
    @return value
    */
  unsigned int value() const;

  unsigned int nClustersLane0() const;
  unsigned int nClustersLane1() const;
  unsigned int nClustersLane2() const;
  unsigned int nClustersLane3() const;
  unsigned int nClustersLane4() const;
  unsigned int nClustersLane5() const;

  unsigned int getEventIDFromTFC() const;

  /** Operator overloading for stringoutput */
  friend std::ostream& operator<<( std::ostream& s, const UTHeaderWord& obj ) {
    s << obj.value() << " \n";
    return s;
  }

private:
  enum class bits {
    EventIDFromTFC = 24,
    paddedBank     = 16,
    lane5          = 8,
    lane4          = 0,
    lane3          = 24,
    lane2          = 16,
    lane1          = 8,
    lane0          = 0
  };

  enum class mask : unsigned int {
    EventIDFromTFC = 0xFF000000,
    paddedBank     = 0xFF0000,
    lane5          = 0xFF00,
    lane4          = 0xFF,
    lane3          = 0xFF000000,
    lane2          = 0xFF0000,
    lane1          = 0xFF00,
    lane0          = 0xFF
  };

  unsigned int EventIDFromTFC = 255;
  unsigned int paddedValue    = 0;
  unsigned int m_value        = 0;
};

inline unsigned int UTHeaderWord::value() const { return m_value; }

inline unsigned int UTHeaderWord::nClustersLane0() const {
  return ( m_value & static_cast<unsigned int>( mask::lane0 ) ) >> static_cast<unsigned int>( bits::lane0 );
}
inline unsigned int UTHeaderWord::nClustersLane1() const {
  return ( m_value & static_cast<unsigned int>( mask::lane1 ) ) >> static_cast<unsigned int>( bits::lane1 );
}
inline unsigned int UTHeaderWord::nClustersLane2() const {
  return ( m_value & static_cast<unsigned int>( mask::lane2 ) ) >> static_cast<unsigned int>( bits::lane2 );
}
inline unsigned int UTHeaderWord::nClustersLane3() const {
  return ( m_value & static_cast<unsigned int>( mask::lane3 ) ) >> static_cast<unsigned int>( bits::lane3 );
}
inline unsigned int UTHeaderWord::nClustersLane4() const {
  return ( m_value & static_cast<unsigned int>( mask::lane4 ) ) >> static_cast<unsigned int>( bits::lane4 );
}
inline unsigned int UTHeaderWord::nClustersLane5() const {
  return ( m_value & static_cast<unsigned int>( mask::lane5 ) ) >> static_cast<unsigned int>( bits::lane5 );
}

inline unsigned int UTHeaderWord::getEventIDFromTFC() const {
  return ( m_value & static_cast<unsigned int>( mask::EventIDFromTFC ) ) >>
         static_cast<unsigned int>( bits::EventIDFromTFC );
}
