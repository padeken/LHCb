/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file UTCommonBase.h
 *
 *  Header file for UT base class : UTCommonBase
 *
 *  @author Andy Beiter (based on code by Matthew Needham)
 *  @date   2018-09-04
 */
//-----------------------------------------------------------------------------
#pragma once
#include "Event/ProcStatus.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTLexicalCaster.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <map>
#include <string>
#include <vector>

namespace UT {

  //-----------------------------------------------------------------------------
  /** @class CommonBase UTCommonBase.h UTKernel/UTCommonBase.h
   *
   *  Base class providing common functionality for all UT tools and algorithms
   *
   *  @author Andy Beiter (based on code by Matthew Needham)
   *  @date   2018-09-04
   */
  //-----------------------------------------------------------------------------

  template <class PBASE>
  class CommonBase : public PBASE {

  public:
    using PBASE::PBASE;

    /** Initialization of the algorithm after creation
     *
     * @return The status of the initialization
     * @retval StatusCode::SUCCESS Initialization was successful
     * @retval StatusCode::FAILURE Initialization failed
     */
    StatusCode initialize() override;

    /** get the top level detector element */
    const DeUTDetector* tracker() const;

    /** get the readout tool */
    const IUTReadoutTool* readoutTool() const;

    /** station as a string */
    std::string station( const LHCb::UTChannelID& chan ) const;

    /** region as string */
    std::string uniqueDetRegion( const LHCb::UTChannelID& chan ) const;

    /** layer as a string */
    std::string uniqueLayer( const LHCb::UTChannelID& chan ) const;

    /** sector as a string */
    std::string uniqueSector( const LHCb::UTChannelID& chan ) const;

    /** beetle as a string */
    std::string uniqueBeetle( const LHCb::UTChannelID& chan ) const;

    /** port */
    std::string uniquePort( const LHCb::UTChannelID& chan ) const;

    /** detector type as a string */
    std::string detectorType( const LHCb::UTChannelID& chan ) const;

    /** safe finding of the sector - exception thrown if not valid */
    const DeUTSector* findSector( const LHCb::UTChannelID& aChannel ) const;

    /** return a procstatus in case event is aborted */
    StatusCode procFailure( const std::string& reason, const bool aborted = false ) const;

  private:
    const DeUTDetector*          m_tracker{nullptr};
    const IUTReadoutTool*        m_readoutTool{nullptr};
    Gaudi::Property<std::string> m_readoutToolName{this, "ReadoutTool", "UTReadoutTool"};
  };

  //=============================================================================
  // Initialisation
  //=============================================================================
  template <class PBASE>
  StatusCode CommonBase<PBASE>::initialize() {
    // Execute the base class initialize
    return PBASE::initialize().andThen( [&] {
      m_tracker     = this->template getDet<DeUTDetector>( DeUTDetLocation::location() );
      m_readoutTool = this->template tool<IUTReadoutTool>( m_readoutToolName, m_readoutToolName );
    } );
  }
  //=============================================================================

  template <class PBASE>
  const DeUTDetector* CommonBase<PBASE>::tracker() const {
    return m_tracker;
  }

  template <class PBASE>
  const DeUTSector* CommonBase<PBASE>::findSector( const LHCb::UTChannelID& aChannel ) const {
    const DeUTSector* sector = tracker()->findSector( aChannel );
    if ( !sector ) throw GaudiException( "No sector found", this->name(), StatusCode::FAILURE );
    return sector;
  }

  template <class PBASE>
  const IUTReadoutTool* CommonBase<PBASE>::readoutTool() const {
    return m_readoutTool;
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::station( const LHCb::UTChannelID& chan ) const {
    return LHCb::UTNames().StationToString( chan );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::uniqueDetRegion( const LHCb::UTChannelID& chan ) const {
    return LHCb::UTNames().UniqueRegionToString( chan );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::uniqueLayer( const LHCb::UTChannelID& chan ) const {
    return LHCb::UTNames().UniqueLayerToString( chan );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::uniqueSector( const LHCb::UTChannelID& chan ) const {
    return LHCb::UTNames().UniqueSectorToString( chan );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::uniqueBeetle( const LHCb::UTChannelID& chan ) const {
    const DeUTSector* theSector = findSector( chan );
    return theSector->nickname() + "Beetle" + toString( theSector->beetle( chan ) );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::uniquePort( const LHCb::UTChannelID& chan ) const {
    const unsigned int port = ( ( chan.strip() - 1u ) / LHCbConstants::nStripsInPort ) + 1u;
    return uniqueBeetle( chan ) + "Port" + toString( port );
  }

  template <class PBASE>
  std::string CommonBase<PBASE>::detectorType( const LHCb::UTChannelID& chan ) const {
    const DeUTSector* sector = tracker()->findSector( chan );
    return sector ? sector->type() : "Unknown";
  }

  template <class PBASE>
  StatusCode CommonBase<PBASE>::procFailure( const std::string& reason, const bool aborted ) const {

    LHCb::ProcStatus* procStat =
        this->template getOrCreate<LHCb::ProcStatus, LHCb::ProcStatus>( LHCb::ProcStatusLocation::Default );

    procStat->addAlgorithmStatus( this->name(), "UT", reason, -3, aborted );
    return this->Warning( "Processing failed: " + reason, StatusCode::SUCCESS, 1 );
  }
} // namespace UT
