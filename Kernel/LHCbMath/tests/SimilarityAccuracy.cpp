/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbMath/Similarity.h"

#include <cstdlib>
#include <iostream>

using gsl::span;

/// gives a number before 0 and 1
template <typename T>
T r() {
  return ( (T)rand() ) / RAND_MAX;
}

template <typename T>
void testSimilarity51() {
  std::array<T, 15> C{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 5>  F{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 1>  R;
  LHCb::Math::detail::similarity_5_1( C, F, R );
  std::cout << R[0] << std::endl;
}

template <typename T>
void testSimilarity55( span<const T, 15> C, span<const T, 25> F ) {
  std::array<T, 15> R{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  LHCb::Math::detail::similarity_5_5( C, F, R );
  for ( int i = 0; i < 15; i++ ) { std::cout << R[i] << " "; }
  std::cout << std::endl;
}

template <typename T>
void testSimilarity55() {
  std::array<T, 15> C{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 25> F{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  testSimilarity55<T>( C, F );
}

template <typename T>
void testSimilarity55Chosenvalues() {
  std::array<T, 25> F{1,        0,         -69.9631, -0.0150666, 44.7208,   0,        1, 0.00912927, -69.973,
                      -3.11877, 0,         0,        0.99953,    0.0004306, -1.27811, 0, 0,          -0.000260913,
                      0.999815, 0.0891337, 0,        0,          0,         0,        1};
  std::array<T, 15> Co{0.202209,    2.24285,    26.178,       -0.00418414,  -0.0519109,
                       0.000171261, -0.0344422, -0.368847,    0.000313205,  0.00992311,
                       0.00235896,  0.0311176,  -0.000125439, -4.15536e-05, 9.72633e-05};
  std::array<T, 15> Cn{0.201002,    2.22998,    26.0407,      -0.00417452,  -0.0518084,
                       0.000171184, -0.0341609, -0.365847,    0.000310967,  0.00811565,
                       0.00235843,  0.031112,   -0.000125434, -4.14304e-05, 9.72631e-05};
  testSimilarity55<T>( Co, F );
  testSimilarity55<T>( Cn, F );
}

template <typename T>
void testSimilarity57() {
  std::array<T, 15> C{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 35> F{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 28> R{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  LHCb::Math::detail::similarity_5_7( C, F, R );
  for ( int i = 0; i < 28; i++ ) { std::cout << R[i] << " "; }
  std::cout << std::endl;
}

template <typename T>
void testAverage() {
  std::array<T, 15> C1{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                       r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 15> C2{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                       r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 5>  X1{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 5>  X2{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 15> C{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  std::array<T, 5>  X{0, 0, 0, 0, 0};
  LHCb::Math::detail::average( X1, C1, X2, C2, X, C );
  for ( int i = 0; i < 5; i++ ) { std::cout << X[i] << " "; }
  for ( int i = 0; i < 15; i++ ) { std::cout << C[i] << " "; }
  std::cout << std::endl;
}

template <typename T>
void testFilter( span<T, 5> X, span<T, 15> C, span<const T, 5> Xref, span<const T, 5> H, T res, T error ) {
  auto output = LHCb::Math::detail::filter( X, C, Xref, H, res, error );
  for ( int i = 0; i < 5; i++ ) { std::cout << X[i] << " "; }
  std::cout << std::endl;
  for ( int i = 0; i < 15; i++ ) { std::cout << C[i] << " "; }
  std::cout << std::endl;
  std::cout << res << " " << error << " " << output << std::endl;
}

template <typename T>
void testFilter() {
  std::array<T, 5>  X{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 15> C{r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(),
                      r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 5>  Xref{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  std::array<T, 5>  H{r<T>(), r<T>(), r<T>(), r<T>(), r<T>()};
  T                 res{r<T>()};
  T                 error{r<T>()};
  testFilter<T>( X, C, Xref, H, res, error );
}

template <typename T>
void testFilterChoosenValues() {
  std::array<T, 5>  X{-0x1.64b2402d2cdfep+11, 0x1.f0313fe8d5333p+9, -0x1.1e55c620370a5p-1, 0x1.beb28fc05b763p-4,
                     -0x1.3a2ab6p-12};
  std::array<T, 15> C{0x1.9p+8,
                      0x0p+0,
                      0x1.9p+8,
                      0x0p+0,
                      0x0p+0,
                      0x1.47ae147ae147cp-7,
                      0x0p+0,
                      0x0p+0,
                      0x0p+0,
                      0x1.47ae147ae147cp-7,
                      0x0p+0,
                      0x0p+0,
                      0x0p+0,
                      0x0p+0,
                      0x1.a36e2eb1c432dp-14};
  std::array<T, 5>  Xref{-0x1.64b2402d2cdfep+11, 0x1.f0313fe8d5333p+9, -0x1.1e55c620370a5p-1, 0x1.beb28fc05b763p-4,
                        -0x1.3a2ab6p-12};
  std::array<T, 5>  H{0x1.bed2df330cf85p-1, -0x1.cce2611f7e72p-10, -0x1.901f01aaf404dp+1, 0x1.9cb649eeb7149p-8, 0x0p+0};
  T                 res{-0x1.372b7a1f5658fp-6};
  T                 error{0x1.a36e2eb1c432dp-8};
  testFilter<T>( X, C, Xref, H, res, error );
}

template <typename T>
void testAll() {
  testSimilarity51<T>();
  testSimilarity55<T>();
  testSimilarity57<T>();
  testAverage<T>();
  testFilter<T>();
}

int main() {
  // initialize random number generator
  srand( 642 );
  // report all results with all bits, so in hexadecimal form
  std::cout << std::hexfloat << std::setprecision( 17 );
  // test well chosen values
  testSimilarity55Chosenvalues<double>();
  testFilterChoosenValues<double>();
  // launch a 100 random tests
  for ( int i = 0; i < 100; i++ ) { testAll<double>(); }
}
