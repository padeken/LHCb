/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//=================================================================================
/** @file RichRadiatorType.h
 *
 *  Header file for RICH particle ID enumeration : RichRadiatorType
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//=================================================================================

#pragma once

// STL
#include <array>
#include <cstdint>
#include <iostream>
#include <string>

// Boost
#include <boost/container/small_vector.hpp>
#include <boost/version.hpp>

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Hack to work around cling issue
#define N_RADIATOR_TYPES 3

// General namespace for RICH specific definitions documented in RichSide.h
namespace Rich {

  /// Number of RICH radiators
  inline constexpr std::uint16_t NRadiatorTypes = N_RADIATOR_TYPES;

  /** @enum Rich::RadiatorType
   *
   *  RICH radiator types
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   08/07/2004
   */
  enum RadiatorType : std::int8_t {
    InvalidRadiator = -1, ///< Unspecified radiator type
    Aerogel         = 0,  ///< Aerogel in RICH1
    Rich1Gas        = 1,  ///< Gaseous RICH1 radiator
    Rich2Gas        = 2,  ///< Gaseous RICH2 radiator
    C4F10           = 1,  ///< Gaseous RICH1 radiator (to be removed)
    CF4             = 2,  ///< Gaseous RICH2 radiator (to be removed)
    // background types
    GasQuartzWin  = 3, ///< Quartz windows to the gas radiator volumes
    HPDQuartzWin  = 4, ///< HPD Quartz windows
    Nitrogen      = 5, ///< Nitrogen volume
    AerogelFilter = 6, ///< Aerogel filter material
    CO2           = 7, ///< Carbon dioxide
    PMTQuartzWin  = 8  ///< MAPMT Quartz windows
  };

  /// Type for fixed size arrays with radiator information
  template <typename TYPE>
  using RadiatorArray = std::array<TYPE, NRadiatorTypes>;

  /// Type for container of radiator types
  // Explicitly specify boost::container::small_vector default template arguments
  // to sidestep cling error, see lhcb/LHCb#75
#if BOOST_VERSION < 107100
  using Radiators = boost::container::small_vector<RadiatorType, N_RADIATOR_TYPES, void>;
#else
  using Radiators = boost::container::small_vector<RadiatorType, N_RADIATOR_TYPES, void, void>;
#endif

  /// Access all valid radiator types
  inline Radiators radiators() noexcept { return {Rich::Aerogel, Rich::Rich1Gas, Rich::Rich2Gas}; }

  /** Text conversion for RadiatorType enumeration
   *
   *  @param radiator Radiator type enumeration
   *  @return Radiator type as an std::string
   */
  std::string text( const Rich::RadiatorType radiator );

  /// Implement textual ostream << method for Rich::RadiatorType enumeration
  inline std::ostream& operator<<( std::ostream& s, const Rich::RadiatorType radiator ) {
    return s << Rich::text( radiator );
  }

  /// Print a vector of Radiator IDs
  inline std::ostream& operator<<( std::ostream& str, const Radiators& rads ) {
    return GaudiUtils::details::ostream_joiner( str << '[', rads, ", " ) << ']';
  }

} // namespace Rich
