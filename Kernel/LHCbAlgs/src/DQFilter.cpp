/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * DQFilter.h
 *
 *  Created on: Jan 31, 2012
 *      Author: marcocle
 */

#include "GaudiAlg/FilterPredicate.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IAccept.h"

/** @class DQFilter
 *  Small algorithm to filter events according to the Data Quality flags stored
 *  in the conditions database on a run-by-run basis.
 *
 *  When initialized, the algorithm register itself as user of the DQ Flags
 *  conditions
 *
 *  @author Marco Clemencic
 *  @date   Jan 31, 2012
 */
class DQFilter final : public Gaudi::Functional::FilterPredicate<bool()> {

public:
  using FilterPredicate::FilterPredicate;
  StatusCode initialize() override;
  bool       operator()() const override;

private:
  ToolHandle<IAccept>                             m_acceptTool{this, "AcceptTool", "DQAcceptTool",
                                   "IAccept Tool to filter the events (default: DQAcceptTool)."};
  mutable Gaudi::Accumulators::AveragingCounter<> m_efficiency{this, "efficiency"};
};

StatusCode DQFilter::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&]() -> StatusCode {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "DQFilter/" << name() << " initialized:" << endmsg << "  filtering on execute" << endmsg << "  using "
              << m_acceptTool.name() << endmsg;
    }
    return StatusCode::SUCCESS;
  } );
}

bool DQFilter::operator()() const {
  const bool accepted = m_acceptTool->accept();
  if ( msgLevel() <= MSG::VERBOSE ) {
    verbose() << "Filter event: " << ( ( accepted ) ? "good" : "bad" ) << " event" << endmsg;
  }
  m_efficiency += accepted;
  return accepted;
}

DECLARE_COMPONENT( DQFilter )
