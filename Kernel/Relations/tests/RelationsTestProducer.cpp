/***********************************************************************************\
* (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/Producer.h"
#include "Relations/Relation1D.h"

namespace LHCb::Relations::Test {

  using BaseClass_t   = Gaudi::Functional::Traits::BaseClass_t<Gaudi::Algorithm>;
  using RelationTable = LHCb::Relation1D<int, int>;

  struct SquaresProducer final : Gaudi::Functional::Producer<RelationTable(), BaseClass_t> {
    SquaresProducer( const std::string& name, ISvcLocator* svcLoc )
        : Producer{name, svcLoc, KeyValue( "OutputLocation", "/Event/MySquares" )} {}

    RelationTable operator()() const override {
      RelationTable table;
      for ( int i = 0; i < 10; ++i ) table.relate( i, i * i ).ignore();
      return table;
    }
  };

  DECLARE_COMPONENT( SquaresProducer )

  struct SquaresConsumer final : Gaudi::Functional::Consumer<void( const RelationTable& ), BaseClass_t> {
    SquaresConsumer( const std::string& name, ISvcLocator* svcLoc )
        : Consumer{name, svcLoc, KeyValue( "InputLocation", "/Event/MySquares" )} {}

    void operator()( RelationTable const& table ) const override {
      for ( const auto& i : table.relations() ) { always() << i << endmsg; }
    }
  };

  DECLARE_COMPONENT( SquaresConsumer )

} // namespace LHCb::Relations::Test
