/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AllocationTrackerHelpers.h"

#include "Kernel/AllocationTracker.h"
#include "Kernel/IAllocationTracker.h"
#include "Kernel/STLExtensions.h"

#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/ThreadLocalContext.h"

#include <boost/container/static_vector.hpp>
#include <boost/regex.hpp>

#include <scoped_allocator>

namespace {
  struct stack_frame {
    stack_frame( void const* address ) : m_address{address} {}
    void const*      address() const { return m_address; }
    std::string_view library() const {
      auto pos = m_library.rfind( '/' );
      if ( pos == std::string::npos ) {
        return m_library;
      } else {
        return std::string_view{m_library}.substr( pos + 1 );
      }
    }
    std::string_view libraryPath() const { return m_library; }
    std::string&     libraryStorage() { return m_library; }
    std::string_view function() const { return m_function; }
    std::string&     functionStorage() { return m_function; }
    friend bool operator<( stack_frame const& lhs, stack_frame const& rhs ) { return lhs.m_function < rhs.m_function; }
    friend bool operator==( stack_frame const& lhs, stack_frame const& rhs ) {
      return lhs.m_function == rhs.m_function;
    }

  private:
    void const* m_address{nullptr};
    std::string m_library, m_function;
  };

  struct stacktrace {
    using allocator_type = AllocationTracker::allocator_with_ptrs<void*>;
    using span_type      = LHCb::span<void* const>;
    stacktrace()         = delete;
    stacktrace( allocator_type const& alloc ) : m_dynamic_addresses{alloc} {
      // First try and use static storage
      m_static_addresses.resize( s_stack_depth_guess );
      m_static_addresses.resize( System::backTrace( m_static_addresses.data(), m_static_addresses.size() ) );
      if ( UNLIKELY( m_static_addresses.size() == s_stack_depth_guess ) ) {
        // Didn't have enough space in the static buffer
        m_static_addresses.clear();
        m_dynamic_addresses.resize( 2 * s_stack_depth_guess );
        while ( true ) {
          std::size_t stackSize = System::backTrace( m_dynamic_addresses.data(), m_dynamic_addresses.size() );
          if ( stackSize < m_dynamic_addresses.size() ) {
            // Great, we had enough space
            m_dynamic_addresses.resize( stackSize );
            break;
          }
          // Still not enough space
          m_dynamic_addresses.resize( 2 * m_dynamic_addresses.size() );
        }
        std::reverse( m_dynamic_addresses.begin(), m_dynamic_addresses.end() );
      } else {
        std::reverse( m_static_addresses.begin(), m_static_addresses.end() );
      }
    }
    stacktrace( stacktrace const& )     = delete;
    stacktrace( stacktrace&& ) noexcept = default;
    stacktrace& operator=( stacktrace const& ) = delete;
    stacktrace& operator=( stacktrace&& ) noexcept = default;
    stacktrace( stacktrace const& other, allocator_type const& alloc )
        : m_static_addresses{other.m_static_addresses}
        , m_dynamic_addresses{other.m_dynamic_addresses, alloc} {} // why do I need this?
    stacktrace( stacktrace&& other, allocator_type const& alloc ) noexcept
        : m_static_addresses{std::move( other.m_static_addresses )}
        , m_dynamic_addresses{std::move( other.m_dynamic_addresses ), alloc} {}
    friend bool operator<( stacktrace const& lhs, stacktrace const& rhs ) {
      auto const lhs_addrs = lhs.addresses();
      auto const rhs_addrs = rhs.addresses();
      return std::lexicographical_compare( lhs_addrs.begin(), lhs_addrs.end(), rhs_addrs.begin(), rhs_addrs.end() );
    }
    [[nodiscard]] span_type addresses() const {
      return m_dynamic_addresses.empty() ? span_type{m_static_addresses} : span_type{m_dynamic_addresses};
    }
    [[nodiscard]] std::size_t              size() const { return this->addresses().size(); }
    [[nodiscard]] std::vector<stack_frame> as_vector() const {
      // For an out parameter of System::getStackLevel that we don't use
      void* addr{nullptr};
      // Get a view to the active address storage
      auto const addrs = this->addresses();
      // Output container
      std::vector<stack_frame> ret;
      ret.reserve( addrs.size() );
      for ( auto stack_addr : addrs ) {
        ret.emplace_back( stack_addr );
        System::getStackLevel( stack_addr, addr, ret.back().functionStorage(), ret.back().libraryStorage() );
      }
      return ret;
    }

  private:
    constexpr static auto s_stack_depth_guess = 25;
    // Using boost::container::small_vector would be neater, but I couldn't
    // quickly make it work using a custom, stateful upstream allocator...
    boost::container::static_vector<void*, s_stack_depth_guess> m_static_addresses;
    std::vector<void*, allocator_type>                          m_dynamic_addresses;
  };

  using allocation_incident_info = std::map<stacktrace, AllocationTracker::allocation_count, std::less<stacktrace>,
                                            std::scoped_allocator_adaptor<AllocationTracker::allocator_with_ptrs<
                                                std::pair<stacktrace const, AllocationTracker::allocation_count>>>>;

  // Static storage for allocation information that can be acccessed from the
  // free-standing callback function.
  std::vector<std::optional<allocation_incident_info>> s_allocations;

  std::size_t indexFromSlot( std::size_t slot ) { return ( slot == EventContext::INVALID_CONTEXT_ID ) ? 0 : slot + 1; }

  std::optional<allocation_incident_info> extractAllocationInformation( std::size_t slot ) {
    std::optional<allocation_incident_info> ret{std::nullopt};
    auto                                    index = indexFromSlot( slot );
    if ( index >= s_allocations.size() ) { return ret; }
    auto& allocations_opt = s_allocations[index];
    ret                   = std::move( allocations_opt );
    allocations_opt.reset();
    return ret;
  }

  struct merged_allocation_info {
    std::optional<allocation_incident_info> main_info, worker_info;
  };

  merged_allocation_info extractAllocationInformation() {
    merged_allocation_info ret;
    ret.main_info = extractAllocationInformation( EventContext::INVALID_CONTEXT_ID );
    for ( auto i = 0ul; i < s_allocations.size() - 1; ++i ) {
      auto worker_info = extractAllocationInformation( i );
      if ( worker_info ) {
        if ( ret.worker_info ) {
          // merge other_worker_info into ret.worker_info
          for ( const auto& element : *worker_info ) {
            auto [iter, inserted] = ret.worker_info->emplace( element );
            if ( !inserted ) { iter->second += element.second; }
          }
        } else {
          ret.worker_info = std::move( worker_info );
        }
      }
    }
    return ret;
  }

  // If the AllocationTracker library was added to LD_PRELOAD then this function will be called on every dynamic
  // allocation.
  void allocationCallback( AllocationTracker::Function, std::size_t size, uint64_t ticks,
                           AllocationTracker::malloc_t malloc_ptr, AllocationTracker::free_t free_ptr ) {
    auto& allocations_opt = s_allocations[indexFromSlot( Gaudi::Hive::currentContext().slot() )];
    if ( !allocations_opt ) {
      // need to set up the storage structure using the pointers we were given
      allocations_opt.emplace( allocation_incident_info::allocator_type::outer_allocator_type{malloc_ptr, free_ptr} );
    }
    auto [iter, inserted] = allocations_opt->emplace( std::piecewise_construct, std::tuple{}, std::tuple{} );
    iter->second.add_allocation( size, ticks );
  }
} // namespace

struct StacktraceAllocationTracker : public extends<Service, IAllocationTracker> {
  using extends::extends;

  /** Setup before threads are spawned and tracking is enabled.
   */
  void reserveSlots( std::size_t num_slots ) override {
    if ( AllocationTracker::getCallback() ) {
      throw GaudiException{"reserveSlots was called while a callback was active. This is unsafe!",
                           "StacktraceAllocationTracker", StatusCode::FAILURE};
    }
    // + 1 so we can record stats from invalid slot IDs
    s_allocations.resize( num_slots + 1 );
  }

  /** Being recording dynamic allocation statistics.
   */
  void beginTracking() override {
    // The callback calls System::backTrace on every allocation. If this has not been used yet then this can trigger
    // libgcc to be dynamically loaded, which itself allocates. Just making a dummy call here before installing the
    // callback avoids the problem.
    System::backTrace( nullptr, 0 );
    // Install our callback to start tracking allocations
    AllocationTracker::setCallback( allocationCallback );
  }

  /** Stop recording dynamic allocation statistics.
   */
  void endTracking() override {
    // Remove our callback, stop tracking allocations
    AllocationTracker::setCallback( nullptr );
  }

  /** Increment the internal count of the number of events that have been
   *  processed while allocation tracking was enabled. This allows the results
   *  to be normalised more helpfully.
   */
  void incrementEventCount( std::size_t num_events, uint64_t num_ticks ) override {
    m_total_ticks += num_ticks;
    m_num_events += num_events;
  }

  StatusCode finalize() final {
    // print out the dynamic allocation tracking information that we stored
    auto  alloc_info_pair = extractAllocationInformation();
    auto& alloc_info      = alloc_info_pair.worker_info;
    if ( alloc_info ) {
      info() << "Dynamic allocation information:" << endmsg;
      uint64_t                                                                total_allocation_ticks{0};
      std::size_t                                                             max_stacktrace_depth{0};
      std::map<std::vector<stack_frame>, AllocationTracker::allocation_count> filtered_info;
      for ( auto const& [stacktrace, allocation_stats] : *alloc_info ) {
        std::vector<stack_frame> key;
        key.reserve( stacktrace.size() );
        max_stacktrace_depth = std::max( max_stacktrace_depth, stacktrace.size() );
        total_allocation_ticks += allocation_stats.num_ticks();
        for ( auto& frame : stacktrace.as_vector() ) { // converts addresses to strings
          // Don't print details of our own internals
          if ( frame.library() == "libAllocationTrackerPreload.so" ) { break; }
          // Don't print these rather uninformative entries
          if ( frame.function() == "local" ) { continue; }
          // Don't print duplicate entries in the tree (presumably the
          // addresses are different, but we don't show those so the duplicates
          // are just unhelpful
          if ( !key.empty() && key.back().library() == frame.library() and key.back().function() == frame.function() ) {
            continue;
          }
          key.emplace_back( std::move( frame ) );
        }
        if ( key.empty() ) { key.emplace_back( "[empty]" ); }
        // see if the bottom of this call stack is earmarked for special treatment
        if ( std::binary_search( m_dynamicAllocationSpecialFunctions.begin(), m_dynamicAllocationSpecialFunctions.end(),
                                 key.back().function() ) ) {
          // If so, delete the whole stack above the function and prefix it
          // with a *
          key.erase( key.begin(), std::prev( key.end() ) );
          key.back().functionStorage().insert( 0, 1, '*' );
        }
        // see if the bottom of the stack matches a pattern for special treatment
        for ( auto const& pattern : m_dynamicAllocationSpecialFunctionCompiledPatterns ) {
          if ( boost::regex_match( std::string{key.back().function()}, pattern ) ) {
            // If so, delete the whole stack above the function and show the
            // pattern itself, prefixed with a *
            key.erase( key.begin(), std::prev( key.end() ) );
            key.back().functionStorage() = '*' + pattern.str();
            // The library isn't checked as part of operator== for key, so
            // after we do these manipulations it will just have the value
            // for whatever the first key to match was.
            key.back().libraryStorage().clear();
          }
        }
        // Store the entry in a sorted, filtered list
        auto [iter, inserted] = filtered_info.emplace(
            std::piecewise_construct, std::forward_as_tuple( std::move( key ) ), std::tuple{allocation_stats} );
        if ( !inserted ) {
          // If we were already tracking allocations that come under this key,
          // just add together the counts
          iter->second += allocation_stats;
        }
      }
      // Loop through the filtered set of frames and print out the tree
      std::vector<stack_frame> const* prev_frames{nullptr};
      for ( auto const& [frames, allocation_stats] : filtered_info ) {
        // find the index of the first frame that differs between 'frames' and '*prev_frames'
        std::size_t index =
            prev_frames
                ? std::distance(
                      frames.begin(),
                      std::mismatch( frames.begin(), frames.end(), prev_frames->begin(), prev_frames->end() ).first )
                : 0;
        for ( auto i = index; i < frames.size(); ++i ) {
          auto const& frame = frames[i];
          info() << std::string( i, '.' ) << frame.function() << " [";
          if ( !frame.library().empty() ) { info() << frame.library(); }
          if ( i == frames.size() - 1 ) {
            info() << ", " << float( allocation_stats.num_bytes() ) / allocation_stats.num_allocations()
                   << " bytes/alloc, ~" << float( allocation_stats.num_allocations() ) / m_num_events
                   << " allocs/evt, ~" << 100.f * float( allocation_stats.num_ticks() ) / total_allocation_ticks
                   << "% alloc time";
          }
          info() << ']' << endmsg;
        }
        prev_frames = &frames;
      }
      info() << "Deepest stacktrace had " << max_stacktrace_depth << " frames" << endmsg;
    }
    return extends::finalize();
  }

private:
  Gaudi::Property<std::vector<std::string>> m_dynamicAllocationSpecialFunctions{
      this,
      "SpecialDynamicAllocatingFunctions",
      {"LinkManager::newInstance()", "UpdateManagerSvc::reserve(Gaudi::Time const&) const",
       "FixTESPathDetails::fullTESLocation[abi:cxx11](std::basic_string_view<char, std::char_traits<char> >, "
       "std::basic_string_view<char, std::char_traits<char> >)"},
      [this]( auto& ) {
        std::sort( m_dynamicAllocationSpecialFunctions.begin(), m_dynamicAllocationSpecialFunctions.end() );
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
  std::vector<boost::regex>                 m_dynamicAllocationSpecialFunctionCompiledPatterns;
  Gaudi::Property<std::vector<std::string>> m_dynamicAllocationSpecialFunctionPatterns{
      this,
      "SpecialDynamicAllocatingFunctionPatterns",
      {R"(DataObjectHandle<AnyDataWrapper<(.*?)\s*> >::put\(\1&&\) const)",
       R"((.*?)\*\s+Gaudi::Functional::details::put<\1, \1, void>\(DataObjectHandle<\1> const&, \1&&\))"},
      [this]( auto& ) {
        m_dynamicAllocationSpecialFunctionCompiledPatterns.clear();
        m_dynamicAllocationSpecialFunctionCompiledPatterns.reserve( m_dynamicAllocationSpecialFunctionPatterns.size() );
        for ( auto const& pattern : m_dynamicAllocationSpecialFunctionPatterns ) {
          m_dynamicAllocationSpecialFunctionCompiledPatterns.emplace_back( pattern );
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
  uint64_t    m_total_ticks{0};
  std::size_t m_num_events{0};
};

DECLARE_COMPONENT( StacktraceAllocationTracker )