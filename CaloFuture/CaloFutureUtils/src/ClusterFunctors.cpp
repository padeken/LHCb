/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloException.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"

// ============================================================================
/** @file ClusterFunctors.cpp
 *
 *  Implementation of non-inline method from ClusterFunctors namespace
 *  @see ClusterFunctors
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 04/07/2001
 */
// ============================================================================

namespace LHCb::Calo::Functor::Cluster {
  // ============================================================================
  /**  Calculate the "energy" of the cluster as a sum of
   *   energies of its digits, weighted with energy fractions
   *   @param   cl  pointer to cluster
   *   @return      "energy" of cluster
   */
  // ============================================================================
  double energy( const LHCb::CaloCluster* cl ) {
    if ( !cl || cl->entries().empty() ) { return 0; }
    return clusterEnergy( cl->entries().begin(), cl->entries().end() );
  }

  // ===========================================================================
  /**    useful function to determine, if clusters have
   *     at least one common cell.
   *
   *     For invalid arguments return "false"
   *
   *     @param   cl1   pointer to first  cluster
   *     @param   cl2   pointer to second cluster
   *     @return "true" if clusters have at least 1 common cell
   */
  // ===========================================================================
  bool overlapped( const LHCb::CaloCluster* cl1, const LHCb::CaloCluster* cl2 ) {
    if ( !cl1 || !cl2 || cl1->entries().empty() || cl2->entries().empty() ) { return false; }
    auto p = commonDigit( cl1->entries().begin(), cl1->entries().end(), cl2->entries().begin(), cl2->entries().end() );
    return cl1->entries().end() != p.first;
  }

  // ===========================================================================
  /**  Calculate the "energy", X and Y position
   *   of the cluster as a sum of
   *   energies/x/y of its digits,
   *   weighted with energy fractions
   *   @param   cl  pointer to cluster object
   *   @param   de  pointer to DeCalorimeter object
   *   @param   e   energy
   *   @param   x   x-position
   *   @param   y   y-position
   *   @return    status code
   */
  // ===========================================================================
  StatusCode calculateEXY( const LHCb::CaloCluster* cl, const DeCalorimeter* de, double& e, double& x, double& y ) {
    e = 0;
    x = 0;
    y = 0;
    if ( !cl || cl->entries().empty() ) { return StatusCode::FAILURE; }
    return calculateClusterEXY( cl->entries().begin(), cl->entries().end(), de, e, x, y );
  }

  StatusCode calculateEXY( LHCb::Event::Calo::Clusters::const_reference cl, const DeCalorimeter* de, double& e,
                           double& x, double& y ) {
    auto r = CaloDataFunctor::calculateClusterEXY( cl.entries(), de );
    if ( !r ) {
      e = 0;
      x = 0;
      y = 0;
      return StatusCode::FAILURE;
    } else {
      e = r->Etot;
      x = r->x;
      y = r->y;
      return StatusCode::SUCCESS;
    }
  }

  // ===========================================================================
  /** throw the exception
   *  @param message exception message
   *  @return status code (fictive)
   */
  // ===========================================================================
  void throwException( const std::string& message ) { throw CaloException( " ClusterFunctors::" + message ); }
} // namespace LHCb::Calo::Functor::Cluster
