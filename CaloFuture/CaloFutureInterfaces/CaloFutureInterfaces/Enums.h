/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Kernel/meta_enum.h>

#include <stdexcept>

namespace LHCb::Calo::Enum {
  template <typename T>
  constexpr int count();

  inline constexpr double Default = -1.e+06; // default value

  meta_enum_class( DataType, int, Unknown = -1,
                   HypoE = 0,     // 0 hypo energy
                   HypoEt,        // 1 hypo Et
                   HypoM,         // 2 hypo Mass
                   ClusterE = 6,  // 6 cluster energy  ( = E9 for 3x3 cluster evaluation)
                   ToHcalE  = 8,  // 8 Hcal deposit in front (using local setting of CaloFutureHypo2CaloFuture)
                   E1       = 11, // cluster seed energy
                   E9,            // 3x3 area cluster
                   E19,           // E1/E9
                   E1Hypo,        // E1/HypoE
                   E4,            // max (2x2) energy within 3x3
                   E49,           // E4/E9
                   CellID,        // cluster-seed id
                   Hcal2Ecal,     // ToHcalE/ClusterE
                   ClusterMatch,  // chi2(2D)
                   ElectronMatch, // 20 chi2(e)
                   BremMatch,     // chi2(brem)
                   NeutralID,     // neutral pid
                   Spread,        // cluster spread
                   TrajectoryL, isPhoton = 37, isPhotonEcl, isPhotonFr2, isPhotonFr2r4, isPhotonAsym, isPhotonKappa,
                   isPhotonEseed, isPhotonE2, isNotH = 52, isNotE, ClusterCode, ClusterFrac, Saturation, ClusterAsX,
                   ClusterAsY, isPhotonXGB )

      template <>
      inline constexpr int count<DataType>() {
    return 61;
  }

  inline int toTypeMask( DataType dt ) { // 0x1 : neutral ; 0x2 : charged ; 0x3 : both
    switch ( dt ) {
    case DataType::HypoE:
    case DataType::HypoEt:
    case DataType::HypoM:

    case DataType::ClusterE:
    case DataType::ToHcalE:

    case DataType::E1:
    case DataType::E9:
    case DataType::E19:
    case DataType::E1Hypo:
    case DataType::E4:
    case DataType::E49:
    case DataType::CellID:
    case DataType::Hcal2Ecal:
    case DataType::ClusterMatch:
      return 0x3;

    case DataType::ElectronMatch:
    case DataType::BremMatch:
      return 0x2;
    case DataType::NeutralID:
      return 0x1;
    case DataType::Spread:
      return 0x3;
    case DataType::TrajectoryL:
      return 0x2;

    case DataType::isPhoton:
    case DataType::isPhotonEcl:
    case DataType::isPhotonFr2:

    case DataType::isPhotonFr2r4:
    case DataType::isPhotonAsym:
    case DataType::isPhotonKappa:
    case DataType::isPhotonEseed:
    case DataType::isPhotonE2:
      return 0x1;
    case DataType::isNotH:
    case DataType::isNotE:
      return 0x1;

    case DataType::ClusterCode:
    case DataType::ClusterFrac:
      return 0x3;
    case DataType::Saturation:
    case DataType::ClusterAsX:
    case DataType::ClusterAsY:

    case DataType::isPhotonXGB:
      return 0x1;
    default:
      break;
    }
    throw std::runtime_error( "Unknown DataType" );
  }

  enum class MatchType { ClusterMatch = 0, ElectronMatch, BremMatch };
  inline const char* toString( MatchType mt ) {
    switch ( mt ) {
    case MatchType::ClusterMatch:
      return "ClusterMatch";
    case MatchType::ElectronMatch:
      return "ElectronMatch";
    case MatchType::BremMatch:
      return "BremMatch";
    }
    throw std::runtime_error( "Unknown MatchType" );
  }

  enum class ClusterType {
    Main = 0,
    SplitOrMain,
  };
  inline const char* toString( ClusterType ct ) {
    switch ( ct ) {
    case ClusterType::Main:
      return "Main";
    case ClusterType::SplitOrMain:
      return "SplitOrMain";
    }
    throw std::runtime_error( "Unknown ClusterType" );
  }
} // namespace LHCb::Calo::Enum
