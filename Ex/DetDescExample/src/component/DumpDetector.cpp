/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "DetDesc/LVolume.h"
#include "DetDesc/PVolume.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Transformer.h"

/**
 *  Algorithm to dump information from the Calo
 */
class DumpCalo
    : public Gaudi::Functional::Transformer<int( const DeCalorimeter& ), LHCb::DetDesc::usesConditions<DeCalorimeter>> {
public:
  DumpCalo( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name, pSvcLocator, KeyValue{"DetectorLocation", "/dd/Structure/LHCb/DownstreamRegion/Ecal"},
                    KeyValue{"OutputLocation", "/Event/ErrorCount"}} {}

  void processLVolume( const ILVolume* lvol ) const {

    info() << "LVOL: " << lvol->name() << endmsg;
    for ( auto pvol : lvol->pvolumes() ) {
      info() << "PVOL: " << pvol->name() << endmsg;
      processLVolume( pvol->lvolume() );
    }
  }

  int operator()( const DeCalorimeter& calo ) const override {

    auto geometry = calo.geometry();
    auto lvol     = geometry->lvolume();
    processLVolume( lvol );

    return 0;
  }
};

DECLARE_COMPONENT( DumpCalo )
