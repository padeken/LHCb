/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ZEROMQHELPER_H
#define ZEROMQHELPER_H 1
#include <deque>
#include <exception>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "IZeroMQSvc.h"
#include "functions.h"

template <class T>
class ZeroMQHelper {

public:
  static std::pair<T, bool> receive( const IZeroMQSvc* svc, zmq::socket_t& socket ) {
    bool more = false;
    auto t    = svc->receive<T>( socket, &more );
    return make_pair( std::move( t ), std::move( more ) );
  }

  static T decode( const IZeroMQSvc* svc, zmq::socket_t& msg ) { return svc->decode<T>( msg ); }

  static std::pair<std::unique_ptr<T>, bool> receiveROOT( const IZeroMQSvc* svc, zmq::socket_t& socket ) {
    bool more = false;
    auto t    = svc->receive<T>( socket, &more );
    return make_pair( std::move( t ), std::move( more ) );
  }

  static std::unique_ptr<T> decodeROOT( const IZeroMQSvc* svc, zmq::socket_t& msg ) { return svc->decode<T>( msg ); }

  static bool send( const IZeroMQSvc* svc, zmq::socket_t& socket, const T& item, int flags = 0 ) {
    return svc->send( socket, item, flags );
  }

  static T encode( const IZeroMQSvc* svc, const T& item ) { return svc->encode( item ); }
};

#endif // ZEROMQHELPER_H
