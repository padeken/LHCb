#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Check the functor cache sources prepared in the `loki_functors_cache` macro.
#
# This script should be called after sources are generated with `gaudirun.py`
# in an initially empty directory. It checks that all produced cpp files are
# among the expected output files, which are passed as command line arguments.
# (The expected output files are statically generated based on the FACTORIES
# argument and determine what gets compiled and then linked in the functor
# cache library.) If that's not the case, the unknown cpp outputs are listed
# and a non-zero exit code is returned.


set -euo pipefail

# Check if some unknown sources were produced
cmd=(find -maxdepth 1 -name "*.cpp")
for known in "$@"; do
    cmd+=(! -name "$known")
done

if ("${cmd[@]}" | >&2 grep '.*') ; then
    >&2 echo "error: the functor sources above are produced by an unknown factory."
    >&2 echo "       please add it to the FACTORIES argument of loki_functors_cache()"
    exit 1
fi

# TODO warn if some factories didn't produce any output (i.e. all cpp files are empty)
